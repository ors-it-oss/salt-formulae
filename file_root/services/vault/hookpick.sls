{%- set version = salt['pillar.get']('hookpick:version', '0.2.1') %}
{%- set url = 'https://github.com/jaxxstorm/hookpick/releases/download/v' ~ version ~ '/hookpick_' ~ version %}
{%- set target = '/usr/local/bin/hookpick' %}

hookpick-install:
  archive.extracted:
  - name: /usr/local/bin/
  - source: {{ url }}_linux_amd64.tar.gz
  - source_hash: {{ url }}_checksums.txt
  - source_hash_update: True
  - enforce_toplevel: False
  file.managed:
  - name: {{ target }}
  - mode: 755
  - replace: False
