{%- from 'vault/lib.jinja' import vault %}
{%- for backend, data in vault.auth.items() %}
vault-auth-backend-{{ backend }}:
  vaultx.auth_backend:
  - name: {{ backend }}
{%- endfor %}
