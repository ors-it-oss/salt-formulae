{%- for ca_name, ca_data in salt['pillar.get']('vault:ca', {}).items() %}
vault-ca-{{ ca_name }}:
  vaultx.ca:
  - name: {{ ca_name }}
  - pubfile: /srv/salt/file_roots/pub/{{ ca_name }}.pem
{%-   for k, v in ca_data.items() %}
  - {{ k }}: {{ v }}
{%-   endfor %}
{%- endfor %}
