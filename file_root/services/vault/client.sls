{%- from 'vault/lib.jinja' import vault %}
include:
- .install

{%- if vault.tls in vault.ca %}
vault-ca:
  file.managed:
  - name: /etc/vault/ca.pem
  - source: salt://pub/{{ vault.tls }}.pem
  - makedirs: True
{%- endif %}
