{%- from 'shared.jinja' import root %}
{%- from 'vault/lib.jinja' import vault %}
{%- set url = 'https://releases.hashicorp.com/vault/' ~ vault.version ~ '/vault_' ~ vault.version %}
{%- set target = root|path_join('/usr/local/bin/') %}
{%- set conf = root|path_join('/etc/vault') %}
{%- if salt.service.status('vault') -%}
vault-stopped:
  service.dead:
  - name: vault
  - prereq:
    - vault-install
{%- endif %}

vault-install:
  pip.installed:
  - name: hvac
  - reload_modules: True
  archive.extracted:
  - name: {{ target }}
  - source: {{ url }}_linux_amd64.zip
  - source_hash: {{ url }}_SHA256SUMS
  - source_hash_update: True
  - enforce_toplevel: False
  - overwrite: True
  - unless: vault version | grep -qo "v{{ vault.version }} "
  file.managed:
  - name: {{ target }}/vault
  - replace: False
  - mode: 755

vault-cap-mlock:
  cmd.run:
  - name: setcap cap_ipc_lock=+ep /usr/local/bin/vault
  - onchanges:
    - archive: vault-install
