# CA pubfile
path "pki/{{ca}}/ca_chain" {
  capabilities = ["read"]
}
path "pki/{{ca}}/ca" {
  capabilities = ["read"]
}
path "pki/{{ca}}/ca/pem" {
  capabilities = ["read"]
}
path "pki/{{ca}}/crl" {
  capabilities = ["read"]
}
path "pki/{{ca}}/crl/pem" {
  capabilities = ["read"]
}

path "pki/{{ca}}/cert/*" {
  capabilities = ["read"]
}

path "pki/{{ca}}/issue/*" {
  capabilities = ["create", "read", "update"]
}
path "pki/{{ca}}/sign/*" {
  capabilities = ["create", "read", "update"]
}

path "pki/{{ca}}/revoke" {
  capabilities = ["create", "read", "update"]
}
