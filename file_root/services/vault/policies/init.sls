{%- from 'vault/lib.jinja' import vault %}

{%- for policy, pdata in vault.policies.items() %}
{%-   set rules = pdata.pop('rules', None) %}
{%-   set source = pdata.pop('source', None) %}
{%-   if not rules and not source %}
{%-     set source = 'vault/policies/' ~ policy ~ '.hcl' %}
{%-   endif %}
vault-policy-{{ policy }}:
  vaultx.policy:
  - name: {{ policy }}
{%- if rules %}
  - rules: {{ rules }}
{%- elif source %}
  - source: {{ source }}
{%- endif %}
{%-   for k,v in pdata.items() %}
  - {{ k }}: {{ v }}
{%-   endfor %}
{%- endfor %}
