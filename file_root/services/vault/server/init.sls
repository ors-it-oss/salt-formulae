{%- from 'shared.jinja' import root, list_if, pki_vault %}
{%- from 'vault/lib.jinja' import vault %}
{##- TODO: keep default policy up-to-date
      https://www.vaultproject.io/docs/concepts/policies.html
      - update
      - run dev server
      - export default
      - overwrite in prod

      https://www.vaultproject.io/guides/operations/production.html
#}
{%- set conf = root|path_join('/etc/vault') %}
{%- set bootstrap_url = 'http://127.0.0.1:8200' %}

{%- macro config(listen, prefix=None, watch=True) -%}
{%-   if prefix %}
{%-     set idph = '-' + prefix %}
{%-   else %}
{%-     set idph = '' %}
{%-   endif %}
{%-   load_yaml as config %}
ui: True
storage:
  {{ vault.storage }}:
{%-     if vault.storage == 'file' %}
    path: /var/vault
{%-     elif storage == 'consul' %}
    url: {{ pillar.consul.server }}
{%-     endif %}
api_addr: {{ vault.url }}
listener:
{%-     for addr in listen %}
- tcp:
{%-       if addr.startswith('http:') %}
    address: '{{ addr[7:] }}'
    tls_disable: 1
{%-       else %}
    address: '{{ addr[8:] }}'
    tls_key_file: /etc/vault/cert.key
    tls_cert_file: /etc/vault/cert.pem
    tls_prefer_server_cipher_suites: True
    {# TODO: This set breaks it!?   tls_cipher_suites: TLS_RSA_WITH_AES_256_CBC_SHA,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305 #}
{%-       endif %}
{%-     endfor -%}
{%-   endload %}
{%-   if 'config' in vault %}
{%-     set config = salt['pillar.get']('vault:config', config, merge=True) %}
{%-   endif %}

vault{{ idph }}-conf:
  file.serialize:
  - name: {{ conf }}/config.json
  - formatter: json
  - dataset: {{ config }}
  - require:
    - file: vault-conf-dir
{%-   if watch %}
  - watch_in:
    - service: vault
{%-   endif %}
{%- endmacro -%}

include:
- .deploy
{{ list_if(vault.tls != 'acme', '..ca') }}
- ..policies

{%- set bootstrap = vault.url.startswith('https') and vault.tls != 'acme' and not salt.file.file_exists(conf|path_join('cert.key')) %}
{%- if bootstrap %}
{%-   set url = bootstrap_url %}
{{ config(prefix='bootstrap', listen=[bootstrap_url]) }}

extend:
{%-   for ca in vault.ca.keys() %}
  vault-ca-{{ ca }}:
    vaultx:
    - url: {{ bootstrap_url }}
    - require:
      - vaultx: vault-unseal
{%-   endfor %}
{%-   for policy in vault.policies.keys() %}
  vault-policy-{{ policy }}:
    vaultx:
    - url: {{ bootstrap_url }}
    - require:
      - vaultx: vault-unseal
{%-   endfor %}
{%- else %}
{%-   set url = vault.url %}
{{ config(listen=vault.listen) }}
{%- endif %}

vault-init:
  vaultx.operator_init:
  - name: '{{ url }}'
  - shares: {{ vault.bootstrap.shares }}
  - threshold: {{ vault.bootstrap.threshold }}
  - files:
      /root/.vault-token: token_raw
{%- for key_id in range(0, vault.bootstrap.threshold) %}
      /etc/salt/pki/master/vault_key{{ key_id }}.gpgm: key
{%- endfor %}
      /etc/vault/hookpick.yml:
        shares: 2
        template: jinja
        contents: |
          gpg: True
          datacenters:
          - name: vault
            hosts:
            - name: {{ vault.url.split(':')[1][2:] }}
              port: {{ vault.url.split(':')[-1] }}
            keys:
            {{ '{%- for key in vault_keys %}' }}
            {{ '- key: {{ key }}' }}
            {{ '{%- endfor %}' }}
  - require:
    - service: vault

vault-unseal:
  vaultx.unsealed:
  - name: {{ url }}
  - keys:
{%- for key_id in range(0, vault.bootstrap.threshold) %}
    - /etc/salt/pki/master/vault_key{{ key_id }}.gpgm
{%- endfor %}
  - require:
    - vaultx: vault-init

{{ salt.tmpltools.pki(ca='vault', name=salt.pillar.get('vault:fqdn', 'vault.' ~ grains.domain), user='vault', files={
      conf|path_join('ca.pem'): 'chain',
      conf|path_join('cert.pem'): ['pub','chain'],
      conf|path_join('cert.key'): 'key'
}) }}

  - url: {{ url }}
{%- if vault.tls != 'acme' %}
  - require:
    - vaultx: vault-ca-{{ vault.tls }}
{%- endif %}
{%- if bootstrap %}
  - require_in:
    - file: vault-conf
{%- endif %}

{%- if bootstrap %}
{{ config(listen=vault.listen, watch=False) }}
vault-bootstrap-restart:
  module.wait:
  - service.restart:
    - name: vault
  - watch:
    - file: vault-conf
{%- endif %}
