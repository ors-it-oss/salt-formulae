{%- from 'shared.jinja' import root %}
{%- from 'vault/lib.jinja' import vault %}
{%- set conf = root|path_join('/etc/vault') %}
include:
- ..install
- ..hookpick

vault:
  group.present:
  - name: vault
  - system: True
  user.present:
  - name: vault
  - fullname: HashiCorp Vault
  - shell: /bin/false
  - home: {{ conf }}
  - gid_from_name: True
  - createhome: False
  - system: True
{%- if salt.group.info('consul')  %}
  - groups:
    - consul
{%- endif %}
  file.managed:
  - name: /etc/systemd/system/vault.service
  - source: salt://{{ slspath }}/systemd_unit
  - template: jinja
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: vault
  service.running:
  - enable: True
  - watch:
    - file: vault
    - file: vault-env
{#    - archive: vault-install#}

vault-env:
  file.managed:
  - name: /etc/sysconfig/vault
  - contents: |
      GOMAXPROCS=2
      VAULT_LOG_LEVEL={{ vault.loglevel }}

      # For hookpick
      GNUPGHOME=/etc/salt/pki/gpg
      VAULT_CACERT=/etc/vault/ca.pem
  - mode: 644

vault-conf-dir:
  file.directory:
  - name: {{ conf }}
  - user: root
  - group: vault
  - mode: 750

{%- if 'file' in vault.storage %}
vault-store-dir:
  file.directory:
  - name: {{ root|path_join('/var/vault') }}
  - user: vault
  - group: vault
  - mode: 700
  - require_in:
    - service: vault
{%- endif %}
