{%- set policy = 'fail2ban_nf' %}
{%- set path = '/etc/fail2ban/' ~ policy %}
fail2ban-selinux:
  file.managed:
  - name: {{ path }}.te
  - source: salt://{{ slspath }}/selinux.te
  - template: jinja
  cmd.run:
  - name: |
      rm -f {{ path }}.{mod,pp}
      checkmodule -M -m -o {{path}}.mod {{path}}.te
      semodule_package -m {{path}}.mod -o {{path}}.pp
      semodule -vi {{path}}.pp
  - onchanges:
    - file: fail2ban-selinux
