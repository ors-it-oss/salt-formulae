{%- from 'shared.jinja' import salt_header %}
{%- if grains.get('selinux', False) %}
include:
- .selinux
{%- endif %}

fail2ban:
  pkg.installed: []
  service.running:
  - enable: true
  - require:
    - pkg: fail2ban
  file.recurse:
  - name: /etc/fail2ban/fail2ban.d/
  - source: salt://{{ slspath }}/fail2ban.d/
  - require:
    - pkg: fail2ban
  - watch_in:
    - service: fail2ban
  - clean: True

fail2ban-jails:
  file.recurse:
  - name: /etc/fail2ban/jail.d/
  - source: salt://{{ slspath }}/jail.d/
  - template: jinja
  - require:
    - pkg: fail2ban
  - watch_in:
    - service: fail2ban
  - clean: True

fail2ban-nftables-script:
  file.managed:
  - name: /etc/nftables/fail2ban.nft
  - source: salt://{{ slspath }}/nftables-init.nft
  - template: jinja
  - makedirs: True

fail2ban-nftables-init:
  file.managed:
  - name: /etc/systemd/system/fail2ban.service.d/nftables.conf
  - contents: |
      {{ salt_header()|indent(6) }}
      [Service]
      # FirewallD might be nft'ing too & bork up horribly. Apparently.
      ExecStartPre=/bin/sleep 5
      ExecStartPre=/usr/sbin/nft -e -f /etc/nftables/fail2ban.nft
      ExecStopPost=/usr/sbin/nft delete table inet fail2ban
  - makedirs: True
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: fail2ban-nftables-init
  - watch_in:
    - service: fail2ban
