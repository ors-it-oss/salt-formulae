{% from 'redis/server/map.jinja' import redis with context %}
{%- set init = grains.get('init', False) %}

redis:
  pkg.installed:
  - refresh: True
  - pkgs: {{ redis.pkgs }}
  pip.installed:
  - name: redis
  - upgrade: True
  service.running:
  - name: {{ redis.service }}
  - enable: True
  - watch:
    - pkg: redis
    - file: redis-config

redis-config:
  file.managed:
  - name: {{ redis.config }}
  - source: salt://{{ slspath }}/etc/{{ salt['pillar.get']('redis:config', 'redis') }}.conf.jinja
  - template: jinja
  - user: root
  - group: root
  - mode: 0644
  - require:
    - pkg: redis

{#TODO(?) tunes like this & transparant hugepages#}
{#vm.overcommit_memory:#}
{#  sysctl.present:#}
{#     - value: 1#}

{%- if init == 'systemd' %}
redis-service-notify:
  file.managed:
  - name: /etc/systemd/system/redis.service.d/notify.conf
  - makedirs: True
  - contents: |
      [Service]
      Type=notify

redis-service-reload:
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: redis-service-notify
  - watch_in:
    - service: redis
{%- endif %}
