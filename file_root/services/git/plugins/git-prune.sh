#!/bin/sh
#set -x
age="$1"

[ -n "$age" ] || { 'Age must be given'; exit 1; }

branch=$(git branch --no-color | cut -c3-)
head=$(git rev-parse $branch)
#echo "Current branch: $branch $head"
commit=$(git rev-parse "$branch@{$age ago}" 2>&1)

#There was more in there than a well-formed commit id.
#Presumably a warning about no commit found matching the criteria
if echo "$commit" | grep -qv '^[a-z0-9]\+$'; then
   exit 0
fi

echo "Removing everything more than $age commits ago..."

git checkout --orphan pruned $commit
git commit -C $commit
git rebase --onto pruned $commit $branch
git branch -d pruned
