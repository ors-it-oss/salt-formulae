git-prune:
  file.managed:
    - name: {{ salt.cmd.std_out('git --exec-path') }}/git-prune
    - source: salt://{{ slspath }}/git-prune.sh
    - mode: 755