git-subsplit:
  file.managed:
    - name: {{ salt.cmd.run_stdout('git --exec-path') }}/git-subsplit
    - source: https://raw.githubusercontent.com/dflydev/git-subsplit/master/git-subsplit.sh
    - skip_verify: True
    - mode: 755
