#!/bin/sh
#set -x
age="1 year"

GIT_AUTHOR_NAME="ETCkeeper janitor"
GIT_AUTHOR_EMAIL="etckeeper@`hostname -s`"
GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

branch=$(git branch --no-color | cut -c3-)
head=$(git rev-parse $branch)
#echo "Current branch: $branch $head"
commit=$(git rev-parse "$branch@{$age ago}" 2>&1)

#There was more in there than a well-formed commit id.
#Presumably a warning about no commit found matching the criteria
if echo "$commit" | grep -qv '^[a-z0-9]\+$'; then
   exit 0
fi

echo "Removing everything more than $age commits ago..."

git checkout --orphan pruned $commit
git commit -C $commit
git rebase --onto pruned $commit $branch
git branch -d pruned

