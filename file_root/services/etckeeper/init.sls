{%- from 'shared.jinja' import list_if %}
{%- set rhel = salt.tmpltools.rhel() %}
{%- if grains.os_family == 'Debian' %}
{%-   set pkg_high = 'apt' %}
{%-   set pkg_low = 'dpkg' %}
{%- elif grains.os_family == 'RedHat' %}
{%-   set pkg_low = 'rpm' %}
{%-   if rhel and rhel < 7 %}
{%-     set pkg_high = 'yum' %}
{%-   else %}
{%-     set pkg_high = 'dnf' %}
{%-   endif %}
{%- endif %}

{%- if rhel %}
include:
- repo.epel
{%- endif %}

etckeeper-nobzr:
  pkg.removed:
  - pkgs:
    - bzr
  file.absent:
  - name: /etc/.bzr

etckeeper-pkgs:
  pkg.installed:
  - pkgs:
    - git
    - etckeeper
{{ list_if(pkg_high == 'dnf', 'etckeeper-dnf', indent=4) }}
  - require:
    - pkg: etckeeper-nobzr
    - file: etckeeper-nobzr
{%- if rhel %}
    - sls: repo.epel
{%- endif %}

etckeeper-conf:
  file.managed:
  - name: /etc/etckeeper/etckeeper.conf
  - source: salt://{{ slspath }}/etckeeper.conf
  - template: jinja
  - context:
      pkg_high: {{ pkg_high }}
      pkg_low: {{ pkg_low }}
  - user: root
  - group: root
  - mode: 644

etckeeper-pruner:
  file.managed:
  - name: /etc/etckeeper/commit.d/80git-prune
  - source: salt://{{ slspath }}/git-prune.sh
  - user: root
  - group: root
  - mode: 750

etckeeper-gcollector:
  file.managed:
  - name: /etc/etckeeper/commit.d/90git-gc
  - source: salt://{{ slspath }}/git-gc.sh
  - user: root
  - group: root
  - mode: 750

etckeeper-deploy:
  cmd.run:
  - name: etckeeper init && etckeeper commit "initial commit"
  - creates:
    - /etc/.git
  - require:
    - pkg: etckeeper-pkgs

etckeeper-commit:
  cmd.run:
  - name: etckeeper commit "SaltStack $(date)"
  - env:
    - GIT_AUTHOR_NAME: 'SaltStack Minion'
    - GIT_AUTHOR_EMAIL: salt@{{ opts.id }}
  - order: last
  - require:
    - cmd: etckeeper-deploy
  - unless: test -d /etc/.git
