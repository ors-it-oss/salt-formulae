{%- from 'map.jinja' import golang_arch %}
{%- from 'kube/lib.jinja' import version, dir_etc with context %}
{##
k8s-server-install:
  archive.extracted:
  - user: root
  - name: /srv
  - makedirs: False
  - enforce_toplevel: False
  - skip_verify: True
  - source: https://dl.k8s.io/v{{ version }}/kubernetes-server-linux-{{ golang_arch }}.tar.gz
  - options: --exclude='*.docker_tag' --exclude='*.tar' --exclude='*-src.tar.gz'
#}

kube:
  group.present:
  - name: kube
  - system: True
  user.present:
  - name: kube
  - fullname: Kubernetes
  - shell: /bin/false
  - home: {{ dir_etc }}
  - gid_from_name: True
  - createhome: False
  - system: True

kube-conf-dir:
  file.directory:
  - name: {{ dir_etc }}
  - user: root
  - group: kube
  - mode: 750

kube-var-dir:
  file.directory:
  - name: /var/run/kubernetes
  - user: kube
  - group: kube
  - mode: 755

kube-log-dir:
  file.directory:
  - name: /var/log/kubernetes
  - user: kube
  - group: kube
  - mode: 770

kube-tmp:
  file.managed:
  - name: /usr/lib/tmpfiles.d/kubernetes.conf
  - contents: |
      d /var/run/kubernetes 0755 kube kube -
