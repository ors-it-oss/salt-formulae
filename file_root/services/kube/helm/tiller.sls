{%- from 'lib.jinja' import globals %}
{%- set path = '/etc/kubernetes/pki/' %}
{%- set ns = salt.pillar.get('kube:tiller:namespace', 'kube-system') %}
{%- set sa = salt.pillar.get('kube:tiller:serviceaccount', 'tiller') %}
{%- set history = salt.pillar.get('kube:tiller:history', 3) %}
{%- set cn = 'system:serviceaccount:' ~ ns ~ ':' ~ sa %}

{%- set cert_file = path|path_join(sa ~ '.pem') %}
{%- set ca_file = globals.cert_dir|path_join('kube-system-ca.pem') %}
{%- set client_ca_file = globals.cert_dir|path_join('kube-client-ca.pem') %}
{%- set key_file = path|path_join('tiller.key') %}

tiller-pki-dir:
  file.directory:
  - name: {{ path }}
  - mode: 750
  - makedirs: True

{{ salt.tmpltools.pki(
      name=cn,
      ca='kube-system',
      user='root',
      role='kube',
      files={
        ca_file: 'ca',
        cert_file: ['pub', 'chain'],
        key_file: 'key'
    })
}}

tiller-install:
  cmd.run:
  - name: helm init --history-max {{ history }} --node-selectors kubernetes.io/role=master --tiller-namespace {{ ns }} --service-account tiller --tiller-tls --tiller-tls-cert {{ cert_file }} --tiller-tls-key {{ key_file }} --tiller-tls-verify --tls-ca-cert {{ client_ca_file }} --upgrade
#  - onchanges:
#    - vaultx: pki-vault-kube-system-{{ cn }}
