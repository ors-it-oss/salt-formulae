{%- from 'map.jinja' import golang_arch %}
{%- set version = salt['pillar.get']('kube:helm:version', '2.12.2') %}
include:
- kube.ctl

helm-install:
  archive.extracted:
  - user: root
  - name: /usr/local/bin
  - makedirs: False
  - enforce_toplevel: False
  - source: https://kubernetes-helm.storage.googleapis.com/helm-v{{ version }}-linux-{{ golang_arch }}.tar.gz
  - source_hash: https://kubernetes-helm.storage.googleapis.com/helm-v{{ version }}-linux-{{ golang_arch }}.tar.gz.sha256
  - options: --strip-components=1
