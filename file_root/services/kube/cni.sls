{%- from 'map.jinja' import golang_arch %}
{%- set version = salt['pillar.get']('kube:cni:version', '0.7.5') %}
cni-conf.d:
  file.directory:
  - user: root
  - group: root
  - name: /etc/cni/net.d
  - mode: 755
  - makedirs: True

{#- These are all available CNI network plugins.#}
cni-binaries:
  file.managed:
  - name: /etc/cni/release
  - contents: {{ version }}
  archive.extracted:
  - user: root
  - name: /opt/cni/bin
  - makedirs: True
  - source: https://github.com/containernetworking/plugins/releases/download/v{{ version }}/cni-plugins-{{ golang_arch }}-v{{ version }}.tgz
  - source_hash: https://github.com/containernetworking/plugins/releases/download/v{{ version }}/cni-plugins-{{ golang_arch }}-v{{ version }}.tgz.sha256
{#-    - clean: True  {# TODO: Doesn't work because the tar paths are stored as './mycni-plugin' #}
  - overwrite: True
  - onchanges:
    - file: cni-binaries

cni-conf-loopback:
  file.serialize:
  - name: /etc/cni/net.d/ZZ-loopback.conf
  - user: root
  - formatter: json
  - dataset:
      cniVersion: 0.3.1
      type: loopback

cni-conf-cilium-portmap:
  file.serialize:
  - name: /etc/cni/net.d/00-chain.conflist
  - user: root
  - formatter: json
  - dataset:
      cniVersion: 0.3.1
      name: chain
      plugins:
      - type: cilium-cni
{#{%- if salt.file.file_exists('/var/run/cilium/cilium.sock') %}#}
{#{%- else %}#}
{#      - type: loopback#}
{#{%- endif %}#}
      - type: portmap
        capabilities:
          portMappings: True

bpf-deps:
  pkg.installed:
  - pkgs:
    - bpftool
{#    - bpftrace#}
