{%- set path = '/srv/metrics' %}
{%- set cert_file = path|path_join('metrics-server.pem') %}
{%- set ca_file = path|path_join('kube-system-ca.pem') %}
{%- set key_file = path|path_join('metrics-server.key') %}

kube-metrics-pki-dir:
  file.directory:
  - name: {{ path }}
  - mode: 750
  - makedirs: True

{{ salt.tmpltools.pki(
    name='system:metrics-server',
    ca='kube-system',
    user='root',
    role='kube',
    files={
      ca_file: 'ca',
      cert_file: ['pub', 'chain'],
      key_file: 'key'
  })
}}

kube-metrics-secret:
  cmd.run:
  - name: kubectl -n kube-system create secret generic metrics-server --from-file={{ ca_file }} --from-file={{ cert_file }} --from-file={{ key_file }}
  - onchanges:
    - vaultx: pki-vault-kube-system-system:metrics-server
