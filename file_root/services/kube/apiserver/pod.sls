{%- from 'lib.jinja' import globals -%}
{%- from 'kube/lib.jinja' import kube_comp, dir_etc, dir_cert, dir_private, kube_stid with context %}

{{ kube_stid }}-staticpod:
  file.managed:
  - name: {{ dir_etc|path_join('staticpods.d', kube_comp ~ '.yml') }}
  - source: salt://kube/tpl/staticpod.j2
  - makedirs: True
  - user: kube
  - group: kube
  - template: jinja
  - context:
      ports:
      - 6443
      volumes:
      #- [/etc/kubernetes, rw, Directory]
      - [{{dir_etc|path_join('audit-policy.yml')}}, ro, File]
      - [{{ globals.cert_dir }}, ro, Directory]
      - [/var/log/kubernetes, rw, DirectoryOrCreate]
      - [{{ dir_cert }}, ro, Directory]
      - [{{ dir_private }}, ro, Directory]
