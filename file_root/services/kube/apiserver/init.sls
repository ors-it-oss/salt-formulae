{%- from 'shared.jinja' import root %}
{%- from 'map.jinja' import paths %}
{%- from 'lib.jinja' import globals with context %}
{%- from 'kube/lib.jinja' import kube_comp, kube_bin, dir_etc, dir_cert, dir_private, kube_stid, kube_config, ip_sans with context %}
{{ kube_stid }}-encryption:
  file.managed:
  - name: {{ root|path_join(dir_etc, 'encryption-config.yml') }}
  - source: salt://{{ slspath }}/tpl/encryption-config.yml
  - mode: 640
  - user: root
  - group: kube
  - dir_mode: 750
  - template: jinja
  - context: {{ pillar.kube[kube_comp].encryption }}
  - makedirs: True

{{ kube_stid }}-audit-policy:
  file.managed:
  - name: {{ root|path_join(dir_etc, 'audit-policy.yml') }}
  - source: salt://{{ slspath }}/tpl/audit-policy.yml
  - user: root
  - group: kube
  - mode: 640
  - dir_mode: 750
  - template: jinja
  - makedirs: True
  - watch_in:
    - service: {{ kube_stid }}-service

{%- set pkey = 'kube:' ~ kube_comp ~ ':service-account:pub' %}
{%- if salt['pillar.get'](pkey, False) %}
{{ kube_stid }}-service-account-pub:
  file.managed:
  - name: {{ root|path_join(globals.cert_dir, 'kube-service-account.pub') }}
  - contents_pillar: {{ pkey }}
  - mode: 640
  - user: root
  - group: kube
  - watch_in:
    - service: {{ kube_stid }}-service
{%- endif %}

{#  {{ kube_config(local_only=True) }} #}
{%- set triggers = {'service.reload': kube_bin} %}

{%- if 'client-ca' in pillar.kube.apiserver %}
{{  salt.tmpltools.pki(
      ca=pillar.kube.apiserver['client-ca'],
      user='kube',
      group='kube',
      schedule_triggers=triggers,
      files={
        root|path_join(globals.cert_dir, 'kube-client-ca-bundle.crt'): 'ca',
    })
}}
  - watch_in:
    - service: {{ kube_stid }}-service
{%- endif %}

{{  salt.tmpltools.pki(
      name=salt['pillar.get']('kube:etcd:user', 'kube'),
      ca='etcd-client',
      user='kube',
      alt_names=pillar.dns.fqdn,
      ip_sans=ip_sans,
      schedule_triggers=triggers,
      files={
        root|path_join(globals.cert_dir, 'etcd-client-ca.pem'): 'ca',
        root|path_join(dir_cert, 'etcd-client.pem'): 'pub',
        root|path_join(dir_private, 'etcd-client-key.pem'): 'key'
    })
}}
  - watch_in:
    - service: {{ kube_stid }}-service

{%- set master = pillar.kube.master[8:].split(':')[0] %}
{{  salt.tmpltools.pki(
      name='system:kubelet-api-admin',
      ca='kube-system',
      alt_names=[pillar.dns.fqdn, master],
      ip_sans=ip_sans,
      user='kube',
      role='kube',
      schedule_triggers=triggers,
      files={
        root|path_join(globals.cert_dir, 'kube-system-ca.pem'): 'ca',
        root|path_join(dir_cert, 'kube-system.pem'): 'pub',
        root|path_join(dir_cert, 'kube-system-chain.pem'): ['pub', 'ca'],
        root|path_join(dir_private, 'kube-system-key.pem'): 'key'
    })
}}
  - watch_in:
    - service: {{ kube_stid }}-service

{{  salt.tmpltools.pki(
      name='aggregrator',
      ca='kube-system',
      alt_names=[pillar.dns.fqdn, master],
      schedule_triggers=triggers,
      ip_sans=ip_sans,
      user='kube',
      role='kube',
      files={
        root|path_join(globals.cert_dir, 'kube-system-ca.pem'): 'ca',
        root|path_join(dir_cert, 'aggregrator.pem'): 'pub',
        root|path_join(dir_cert, 'aggregrator-chain.pem'): ['pub', 'ca'],
        root|path_join(dir_private, 'aggregrator-key.pem'): 'key'
    })
}}
  - watch_in:
    - service: {{ kube_stid }}-service

{%- include 'kube/tpl/state.j2' %}
