{%- from 'shared.jinja' import root, uri %}
{%- from 'lib.jinja' import globals %}
{%- from 'kube/lib.jinja' import kube_connection, kube_comp, kube_bin, kube_stid with context %}

{%- set pkey = 'kube:' ~ kube_comp ~ ':service-account:key' %}
{%- if salt['pillar.get'](pkey, False) %}
{{ kube_stid }}-service-account-key:
  file.managed:
  - name: {{ root|path_join(globals.private_dir, 'kube-service-account.key') }}
  - contents_pillar: {{ pkey }}
  - mode: 640
  - user: root
  - group: kube
{%- endif %}

kube-controller-manager-libexec-dir:
  file.directory:
  - name: /usr/libexec/kubernetes
  - mode: 750
  - user: root
  - group: kube
  - makedirs: True

{{ kube_connection(server=salt['pillar.get']('kube:master', uri('127.0.0.1', 6443)), service=kube_bin) }}

{%- include 'kube/tpl/state.j2' %}
