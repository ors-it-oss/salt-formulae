{%- from 'shared.jinja' import uri %}
{%- from 'kube/lib.jinja' import kube_connection, kube_bin with context %}

{{ kube_connection(server=salt['pillar.get']('kube:server', uri('127.0.0.1', 6443)), service=kube_bin) }}

{%- include 'kube/tpl/state.j2' %}
