{%- from 'shared.jinja' import root, uri %}
{%- from 'lib.jinja' import globals %}
{%- from 'kube/lib.jinja' import kube_connection, kube_bin, kube_base, kube_stid, dir_etc with context %}
{%- from 'kube/proxy/flags.j2' import config with context %}

{{ kube_connection(ca_role='proxy', service=kube_bin) }}

{{ kube_stid }}-config:
  file.serialize:
  - name: {{ root|path_join(dir_etc, kube_base ~ '-config.yml') }}
  - formatter: yaml
  - dataset: {{ config }}
  - mode: 640
  - user: root
  - group: kube
  - watch_in:
    - service: {{ kube_stid }}-service

{%- include 'kube/tpl/state.j2' %}
