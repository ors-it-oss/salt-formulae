{%- from 'kube/lib.jinja' import bin_url, kube_install, kube_bin, kube_stid with context %}

{{ kube_install(group='root') }}

{{ kube_stid }}-completions:
  pkg.installed:
  - name: bash-completion
  cmd.run:
    - name: kubectl completion bash >/etc/bash_completion.d/kubectl
    - onchanges:
      - file: {{ kube_stid }}-install

{%- if salt['pillar.get']('kubernetes.api_url', False) %}
{{ kube_stid }}-kubeconfig:
  file.managed:
  - name: /etc/salt/salt.kubeconfig
  - mode: 640
  - user: root
  - source: salt://kube/mf/kubeconfig.yml
  - makedirs: True
  - template: jinja
  - context:
      cert:
        pub:  {{ pillar['kubernetes.client-certificate-file'] }}
        key: {{ pillar['kubernetes.client-key-file'] }}
        ca: {{ pillar['kubernetes.certificate-authority-file'] }}
      cluster: salt
      server: {{ pillar['kubernetes.api_url'] }}
      user: {{ pillar['kubernetes.user'] }}
{%- endif %}
