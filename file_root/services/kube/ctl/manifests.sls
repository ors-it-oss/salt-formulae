{%- from 'kube/lib.jinja' import dir_etc %}
{%- set kube_mf = dir_etc|path_join('manifests.d') %}
{%- set mf_override = pillar.get('mf', False) %}
{%- if mf_override %}
{%-   set mf_file = kube_mf|path_join(mf_override ~ '.yml') %}
kube-manifests-{{ mf_override }}:
  file.recurse:
  - name: {{ kube_mf }}
  - template: jinja
  - source: salt://kube/mf/
  - include_pat: E@.*{{ mf_override }}.*\.ya?ml$
  - exclude_pat: E@^(static.*|[.~]+.*|te?m?pl?.*)|\.(swp|tmp|bak)$
  cmd.run:
  - name: kubectl apply --all --force --validate=true -f {{ mf_file }}
  - env:
      KUBECONFIG: /etc/salt/salt.kubeconfig
  - onchanges:
    - file: kube-manifests-{{ mf_override }}
{%- else %}
kube-manifests:
  file.recurse:
  - name: {{ kube_mf }}
  - template: jinja
  - source: salt://kube/mf/
  - include_pat: E@\.ya?ml$
  - exclude_pat: E@^(static.*|[.~]+.*|te?m?pl?.*)|\.(swp|tmp|bak)$
  - clean: True
  cmd.run:
  - name: kubectl apply --all --force --recursive --validate=true -f {{ kube_mf }}
  - env:
      KUBECONFIG: /etc/salt/salt.kubeconfig
  - onchanges:
    - file: kube-manifests
{%- endif %}
