{%- set description = 'Kubelet Server' %}
{%- include 'kube/tpl/deploy.j2' %}

kubernetes-accounting:
  file.managed:
  - name: /etc/systemd/system.conf.d/kubernetes-accounting.conf
  - contents: |
      [Manager]
      DefaultCPUAccounting=yes
      DefaultMemoryAccounting=yes
  - makedirs: True

kube-let-run-dir:
  file.directory:
  - name: /var/lib/kubelet
  - mode: 750
  - user: root
  - group: kube
  - makedirs: True
