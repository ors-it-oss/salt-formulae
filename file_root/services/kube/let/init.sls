{%- from 'shared.jinja' import root -%}
{%- from 'shared.jinja' import list_if, uri -%}
{%- from 'kube/lib.jinja' import kube_connection, dir_etc, kube_base, kube_bin, kube_stid with context -%}
{%- from 'kube/let/flags.j2' import standalone, options, config with context -%}
include:
- kube.cni

kube-staticpods:
  file.directory:
  - name: {{ options.get('pod-manifest-path', config['staticPodPath']) }}
  - mode: 750
  - user: root
  - makedirs: True

kube-resolv:
  file.managed:
  - name: {{ root|path_join(dir_etc, 'resolv.conf') }}
  - group: kube
  - contents: |
{%- for nameserver in grains.dns.ip4_nameservers %}
      nameserver {{ nameserver }}
{%- endfor %}
      search {{ pillar.dns.domain }}
  - watch_in:
    - service: {{ kube_stid }}-service

{{ kube_stid }}-config:
  file.serialize:
  - name: {{ root|path_join(dir_etc, kube_base ~ '-config.yml') }}
  - formatter: yaml
  - dataset: {{ config }}
  - mode: 640
  - user: root
  - group: kube
  - watch_in:
    - service: {{ kube_stid }}-service

{%- if not standalone and salt['pillar.get']('kube:cluster', False) %}
{{ kube_connection(cn='system:node:' ~ options.get('hostname-override', grains.nodename), ca_role='kubelet', ca_function='kubelet', service=kube_bin) }}
{%- endif %}

{%- include 'kube/tpl/state.j2' %}
