{%- from 'kube/lib.jinja' import dir_etc %}
{%- set manifests_dir = dir_etc|path_join('staticpods.d') %}
{%- for srv_name, srv_data in salt['pillar.get']('kube:let:pods', {}).items() %}
{%-   set mf = srv_data.pop('mf') %}
{%-   set srv_data = salt.slsutil.merge(pillar, srv_data) %}
kube-staticpod-{{ srv_name }}:
  file.managed:
  - name: {{ manifests_dir|path_join(srv_name) }}.yml
  - source: salt://kube/mf/{{ mf }}.yml
  - template: jinja
  - context: {{ srv_data }}
  - concurrent: True
{%- endfor %}
