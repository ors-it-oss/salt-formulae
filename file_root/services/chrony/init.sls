{%- from 'chrony/map.jinja' import chrony with context %}
{%- from 'ntp/pools.jinja' import public with context %}
{%- set ntp = pillar.get('ntp', {}) %}

chrony:
  pkg.installed:
  - name: {{ chrony.package }}
  file.managed:
  - name: {{ chrony.config }}
  - source: salt://{{ slspath }}/chrony_conf.jinja
  - template: jinja
  - context:
      masters: False {# TODO #}
      private: {{ ntp.get('servers', False) }}
      public: {{ public }}
      peers: {{ ntp.get('peers', False) }}
  - user: root
  - mode: 644
  - watch_in:
    - service: chrony
  service.running:
  - enable: True
  - name: {{ chrony.service }}
  - require:
    - pkg: {{ chrony.package }}
