{%- set home = '/var/lib/rancid' %}
{%- set remote = salt['pillar.get']('rancid:git:remote', False) %}
{%- set ssh = salt['pillar.get']('rancid:ssh', False) %}
rancid-pkgs:
  pkg.installed:
  - pkgs:
    - rancid
    - git
    - diffstat
    - dma {# delivers 'sendmail' binary #}

rancid-user:
  user.present:
  - name: rancid
  - fullname: RANCID user
  - shell: /bin/bash
  - home: {{ home }}
  - gid_from_name: True
  - system: True

rancid-dir:
  file.directory:
  - name: /var/lib/rancid
  - dir_mode: 750
  - file_mode: 640
  - user: rancid
  - group: rancid

rancid-log:
  file.directory:
  - name: /var/log/rancid
  - dir_mode: 750
  - file_mode: 640
  - user: rancid
  - group: root

rancid-log-rotate:
  cron.present:
  - identifier: logrotate
  - name: /usr/bin/find /var/log/rancid -mtime +{{ salt['pillar.get']('rancid:retention:logs', 30) }} -exec rm {} \;
  - user: rancid
  - dayweek: 6
  - require:
    - user: rancid

rancid-run:
  cron.present:
  - identifier: run
  - name: /usr/bin/rancid-run
  - user: rancid
{%- for k,v in salt['pillar.get']('rancid:schedule', {}).items() %}
  - {{ k }}: {{ v }}
{%- endfor %}
  - require:
    - user: rancid

{%- if ssh %}
rancid-ssh-known_hosts:
  file.managed:
  - name: {{ home }}/.ssh/known_hosts
  - dir_mode: 700
  - mode: 640
  - makedirs: True
  - user: rancid
  - group: rancid
  - replace: False
  ssh_known_hosts.present:
  - name: {{ remote.server }}
  - user: rancid
  - hash_known_hosts: True
  - key: {{ remote.key }}
  - enc: {{ remote.enc }}
  - fingerprint_hash_type: sha256

rancid-ssh-key:
  file.managed:
  - name: /var/lib/rancid/.ssh/id_{{ pillar.rancid.ssh.type }}
  - contents_pillar: rancid:ssh:key
  - user: rancid
  - group: rancid
  - makedirs: True
  - dir_mode: 700
  - mode: 600
{%- endif %}

{#---------------- Eye-watering evil stuff ----------------#}
{%- if remote %}
{##-
  RANCiD tries to git init --bare the repo ssh:// etc leading to all kinds of pointless crazy
  We should patch this upstream
#}
rancid-hotfix-bare:
  file.comment:
  - name: /usr/lib/rancid/bin/rancid-cvs
  - backup: False
  - regex: '.*git.*init.*bare.*'
  - require:
    - pkg: rancid-pkgs

{##-
  RANCiD has no idea of git branches. We introduce one combined w/the added CVSBRANCH env var
  In essence this just adds a git checkout after the clone
#}
{%-   if 'branch' in remote %}
rancid-hotfix-branch:
  file.replace:
  - name: /usr/lib/rancid/bin/rancid-cvs
  - backup: False
  - show_changes: True
  - pattern: '^(\s+)git clone(.*)\n(?!.*git checkout)'
  - repl: |
      \1git clone\2\n\1[ -n "$CVSBRANCH" ] && (git checkout -B "$CVSBRANCH" -t origin/$CVSBRANCH || (git checkout -B "$CVSBRANCH" && git push -u))\n
  - flags:
    - MULTILINE
  - require:
    - pkg: rancid-pkgs
{%-   endif %}

{##-
  If you're pushing from multiple RANCiDs this comes in very handy
  However, if they're hitting the branch simultaneously, this still won't work
#}
rancid-hotfix-name:
  file.replace:
  - name: /usr/lib/rancid/bin/rancid-cvs
  - backup: False
  - show_changes: True
  - pattern: '^(\s+git config.*user.name)\s+"*(.+)"*'
  - repl: '\1 "RANCiD {{ grains.host }}"'
  - require:
    - pkg: rancid-pkgs
{%- endif %}
