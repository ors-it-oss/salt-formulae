{%- set remote = salt['pillar.get']('rancid:git:remote', False) %}
include:
- .deploy

rancid-conf:
  file.managed:
  - name: /etc/rancid/rancid.conf
  - template: jinja
  - source: salt://{{ slspath }}/rancid.conf.jinja
  - context:
      groups: {{ salt['pillar.get']('rancid:groups', {}).keys() }}
      par_count: {{ salt['pillar.get']('rancid:par_count', grains.num_cpus * 3) }}
      remote: {{ remote }}

rancid-initdb:
  cmd.run:
  - name: /usr/lib/rancid/bin/rancid-cvs
  - runas: rancid
  - onchanges:
    - file: rancid-conf

rancid-clogin:
  file.managed:
  - name: /var/lib/rancid/.cloginrc {#- man cloginrc #}
  - source: salt://{{ slspath }}/cloginrc.jinja
  - template: jinja
  - context:
      user: admin
      password: {{ pillar.rancid.clogin }}
  - user: rancid
  - group: rancid
  - mode: 640

{%- for group, members in salt['pillar.get']('rancid:groups', {}).items() %}
rancid-{{ group }}-groupdb:
  file.managed:
  - name: /var/lib/rancid/{{ group }}/router.db
  - template: jinja
  - source: salt://{{ slspath }}/router.db.jinja
  - makedirs: True
  - user: rancid
  - group: rancid
  - dir_mode: 750
  - mode: 640
  - context:
      {#- TODO: Sort, but it's like [one, {two: {tea:pot}}, three] #}
      members: {{ members }}
  - defaults:
      domain: {{ salt['pillar.get']('rancid:domain', None) }}
      mfg: {{ salt['pillar.get']('rancid:default_mfg', None) }}
      state: up
  - require:
    - cmd: rancid-initdb

{%-   if remote %}
rancid-{{ group }}-autopull:
  file.managed:
  - name: /var/lib/rancid/{{ group }}/.git/hooks/pre-push
  - mode: 755
  - source: salt://git/hooks/auto-pull-rebase.sh
  - require:
    - cmd: rancid-initdb
{%-   endif %}
{%- endfor %}
