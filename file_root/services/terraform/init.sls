{# https://github.com/saltstack/salt/issues/39720 #}
{%- set version = salt['pillar.get']('terraform:version', '0.11.7') %}
{%- set url = 'https://releases.hashicorp.com/terraform/' ~ version ~ '/terraform_' ~ version %}
{%- set target = '/usr/local/bin/terraform' %}

terraform-install:
  archive.extracted:
  - name: /usr/local/bin/
  - source: {{ url }}_linux_amd64.zip
  - source_hash: {{ url }}_SHA256SUMS
  - enforce_toplevel: False
