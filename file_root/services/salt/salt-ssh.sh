#!/bin/bash
# * Set up a temp directory with some stuff in it
# * Preconfigure Salt SSH to run standalone from that temp space
# * Create a roster with minion_ids ($1) and IP's ($2)
# * Run Salt-SSH to Highstate (or $@) the roster

set -x
cd $(dirname $0)

SALT_DIR=${SALT_DIR:-/srv/salt}
SALT_TMP=$(mktemp -d /tmp/sssh-${$}-XXX)
#SALT_CMD=${3:-state.highstate}

######## CONFIG ########
cat >$SALT_TMP/master <<CONF
# Master paths
cachedir: $SALT_TMP/cache
log_file: $SALT_TMP/log
pidfile: $SALT_TMP/salt-master.pid
pki_dir: $SALT_TMP
sock_dir: $SALT_TMP

# Git stuff
git_pillar_provider: gitpython    # TODO: https://github.com/saltstack/salt/issues/39892
git_pillar_ssl_verify: True
gitfs_global_lock: False
gitfs_provider: gitpython
gitfs_ssl_verify: True

fileserver_backend:
#  - git
  - roots

# The meat
file_roots:
  base:
    - ${SALT_DIR}/file_root/
pillar_roots:
  base:
    - ${SALT_DIR}/pillar_root/

# https://github.com/saltstack/salt/issues/40007
# https://github.com/saltstack/salt/pull/40775
#ext_pillar:
#  - git:
#    - ssh://git@git.profects.com/papsdam/salt-papsdam.git:
#      - root: pillar
#gitfs_remotes:
#  - ssh://git@git.profects.com/papsdam/salt-papsdam.git:
#    - root: file_root

# Use this as a workaround! (https://github.com/saltstack/salt/issues/9878)
# O wait, it's probably broken on gitfs! (https://github.com/saltstack/salt/issues/19564)
#extra_filerefs:
# - lib.jinja

# SSH options
ssh_log_file: $SALT_TMP/log
# Only on Nitrogen+
ssh_options:
  - ForwardAgent=True
  - HashKnownHosts=True
  - UserKnownHostsFile=$SALT_TMP/known_hosts
ssh_priv: agent-forwarding
ssh_timeout: 10
ssh_use_home_key: False

#Really (https://github.com/saltstack/salt/issues/39892)
ssh_minion_opts:
  failhard: True
  git_pillar_provider: gitpython

# Behaviour config
con_cache: True
enable_gpu_grains: False
grains_cache: True
minion_data_cache: True
pillar_cache: True
pillar_opts: False
pillar_safe_render_error: True
pillar_merge_lists: True
state_aggregate: True
state_output: mixed_id
state_verbose: True
yaml_utf8: True
CONF

mkdir -pm 700 $SALT_TMP/pki/master
mkdir -pm 750 $SALT_TMP/cache


######## ROSTER ########
roster_tmp=$SALT_TMP/roster

# Parse $1 & $2 into arrays
IFS=', ' read -r -a HOSTS <<< "$1"
IFS=', ' read -r -a IPS <<< "$2"
shift; shift

# Walk 'em & generate the inventory entries
for idx in "${!HOSTS[@]}"; do
    fqdn=${HOSTS[idx]}
    ip=${IPS[idx]}

    # Obsolete after Nitrogen release
    ssh-keygen -R $ip >/dev/null 2>&1

    # https://github.com/saltstack/salt/issues/39718
    try=0
    while [ $try -lt 60 ]; do
        timeout 5 bash -c "cat </dev/null >/dev/tcp/$ip/22"
        tcpxit=$?
        if [ $tcpxit -gt 0 ]; then
            try=`expr $try + 1`
            # 'Connection refused' drops immediately
            [ $tcpxit -lt 100 ] && sleep 5
            continue
        fi

        cat <<ROSTER
${fqdn}:
  host: $ip
ROSTER
        break

done; done > $roster_tmp


######## LAUNCH SALT-SSH ########
#salt-run -l trace fileserve.update
salt-ssh -l debug -v \
    --ignore-host-keys \
    --config-dir $SALT_TMP \
    '*' "${@:-state.highstate}"
saltxit=$?

rm -rf $SALT_TMP
exit $saltxit
