#!/bin/sh
set -x
# SystemD will invariably destroy all service children
# we escape that horrible fate by becoming a task(?)
if [ -f /sys/fs/cgroup/systemd/tasks ]; then
    echo $$ >/sys/fs/cgroup/systemd/tasks
    # Snuff out deprecated upstart job while you're at it
    rm /etc/init/salt-minion.conf
fi

salt-call state.sls salt.minion
service salt-minion stop
pkill salt-minion
pkill -9 salt-minion
rm /etc/salt/pki/minion/minion_master.pub
rm /etc/salt/minion.d/master.conf
echo "master: $1" > /etc/salt/minion.d/99-master-address.conf
salt-call test.ping
service salt-minion start
