pips:
  pip.installed:
  - names:
    - cerberus
    - saltpylint
    - salttesting
    - modernize
    - pep8
{#      - pyOpenSSL#}
