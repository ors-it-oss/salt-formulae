{%- for proxy_minion in salt['pillar.get']('salt:proxies') %}
salt-proxy-{{ proxy_minion }}:
  salt_proxy.configure_proxy:
  - proxyname: {{ proxy_minion }}
  - start: True
{%- endfor %}
