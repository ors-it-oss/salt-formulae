salt-repo-refresh:
  schedule.present:
  - minutes: 10
  - function: state.sls
  - job_args:
    - salt.repos.refresh
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: True
  - splay: 60
