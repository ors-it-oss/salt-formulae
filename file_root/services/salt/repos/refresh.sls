{%- from 'salt/map.jinja' import release,salt_ssh with context %}
{#   TODO: Disabled SendEnv LC_* in ssh_config because Gitea b0rks on that... #}
{%- if not salt.file.directory_exists('/srv/salt/repo') %}
repo-dir:
  file.directory:
  - name: /srv/salt/repo
  - makedirs: True
  - dir_mode: 700
  - file_mode: 600
{%- endif %}

{%- set servers = {} %}
{%- for repo, repo_data in salt['pillar.get']('salt:repo', {}).items() %}
{%-   set server = repo_data.url.split('@')[1].split(':')[0].split('/')[0] %}
{%-   do servers.update({repo_data.url.split('@')[1].split(':')[0].split('/')[0]: 'ireallywishihadaset'}) %}
{%-   if salt_ssh %}
{%-     set branch = salt.cmd.run("grep -hR gitfs_base /etc/salt/master.d/|awk -F': ' '{print $2}'") or release %}
{%-   else %}
{%-     set branch = salt['config.get']('gitfs_base', release) %}
{%-   endif %}
repo-{{ repo }}-latest:
  git.latest:
  - target: /srv/salt/repo/{{ repo }}
  - name: {{ repo_data.url }}
  - force_reset: True
  - rev: {{ repo_data.get('branch', branch) }}
  - identity: {{ repo_data.get('identity', '/etc/salt/pki/master/ssh/salt-ssh.rsa') }}
{%- endfor %}
