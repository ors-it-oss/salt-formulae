#!/bin/sh
#set -x
service salt-minion stop
pkill salt-minion
pkill -9 salt-minion

service salt-minion start
