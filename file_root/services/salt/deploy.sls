{#-
  keep this syntax as backwards compatible as possible because of old minions
  That means, amongst others,
  * NO objectification (salt.sys.list_functions won't work, salt['sys.list_functions'] will)
  * NO default for filter_by's
#}
{#- TODO: PIP installs? #}
{%- from 'shared.jinja' import proxy with context %}
{%- from 'salt/map.jinja' import salt_ssh, dir_log with context %}

{#- Distro's that are out of support #}
{%- set osfinger = grains.get('osfinger', grains['os'] + '-' + grains['osrelease'].split('.')[0]) %}
{%- if osfinger in ('Ubuntu-10.04', 'Debian-7', 'Debian-6') %}
{%-   set release = 'stable latest' %}
{%- else %}
{#- Distro's that should be up-to-date #}
{%-   set release = salt['pillar.get']('salt:release', salt['pillar.get']('salt:master:version', 'NOWWHAT')) %}
{%-   set source = salt['pillar.get']('salt:source', 'stable') %}
{%-   if grains['osarch'] in ('i386', 'i686') %}
{#-     repo.saltstack.com has no 32 bit packages (Feb '16) -#}
{%-     set source = 'git' %}
{%-   elif ':' in source %}
{%-     set repo = source  %}
{%-     set source = 'git' %}
{%-   else %}
{%-     set repo = '' %}
{%-   endif %}
{%-   if ' ' not in release|string %}
{%-     set release = source  ~ ' ' ~ release %}
{%-   endif %}
{%-   if repo|length > 0 %}
{%-     set release = '-g ' ~ repo ~ ' ' ~ release %}
{%-   endif %}
{%- endif %}
{%- if salt['pillar.get']('salt:profile', False) %}
{%-   set profile = '-p ' ~ salt['pillar.get']('salt:profile') %}
{%- else %}
{%-   set profile = '' %}
{%- endif %}
{%- set args = '-D ' ~ profile ~ ' ' ~ release %}
{#{{ proxy('/usr/local/sbin/salt-deploy', 'cmd') }}#}
{##- https://docs.saltstack.com/en/latest/faq.html#what-is-the-best-way-to-restart-a-salt-daemon-using-salt
     note by the way the Boron-new cmd.run_bg #}
salt-deploy-script:
{%- if not salt_ssh %}
{#- a regular salt desperately attempting to bootstrap itself #}
  file.managed:
  - name: /usr/local/sbin/salt-deploy
  - source: salt://salt/salt-deploy.sh
  - user: root
  - group: sys
  - mode: 750
  cmd.run:
  - name: |
      exec 0>&- ; exec 1>&- ; exec 2>&- # close stdin, stdout and stderr
      nohup /bin/sh -c '/usr/local/sbin/salt-deploy {{args}} >{{ dir_log }}/salt-deploy.log 2>&1'
  - order: last
{%-   if grains['saltversioninfo'][0] == 0 %}
  - python_shell: True
{%-   endif %}
{%- else %}
{#- Salt-SSH #}
  cmd.script:
  - source: salt://salt/salt-deploy.sh
  - args: {{ args }}
{%- endif %}
