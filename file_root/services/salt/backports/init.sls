{%- from 'salt/map.jinja' import current with context %}
{##
{%- if current < 2015.8 %}
{%-   set current = 'lithium' %}
{%- elif current == 2015.8 %}
{%-   set current = 'beryllium' %}
{%- else %}
{%-   set current = False %}
{%- endif %}
{%- if current %}
include:
  - .{{ current }}
{%- endif %}
#}
