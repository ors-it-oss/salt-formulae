{%- from 'salt/map.jinja' import dir_etc, minion_svc, minion_deps %}
include:
- repo.salt
- salt.backports

salt-minion-dependencies:
{%- if minion_deps %}
  pkg.latest:
  - pkgs: {{ minion_deps }}
  - watch_in:
    - service: salt-minion-svc
  pip.installed:
  - names:
    - pyOpenSSL
    - pytoml
{##- Needs compiler & hasnt been updated since 2012. Only one dockermod func uses it afaict.
{%-   if grains.os == 'Fedora' %}
    - timelib
{%-   endif %}
#}
{%- endif %}

salt-minion-config-base:
  file.recurse:
  - name: {{ dir_etc }}/minion.d
  - user: root
{#-    - group: sys#}
  - file_mode: 640
  - template: jinja
  - source: salt://{{ slspath }}/etc

{%- set init = grains.get('init', False) %}
{%- if init == 'upstart' %}
salt-minion-svc-override:
  file.managed:
  - name: /etc/init/salt-minion.override
  - source: salt://salt/minion/conf/upstart.override
  - watch_in:
    - service: {{ minion_svc }}
{%- elif init == 'systemd' %}
salt-minion-svc-override:
  file.managed:
  - name: /etc/systemd/system/salt-minion.service.d/overrides.conf
  - makedirs: True
  - source: salt://salt/minion/conf/systemd

salt-minion-svc-reload:
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: salt-minion-svc-override
  - watch_in:
    - service: {{ minion_svc }}
{%- endif %}

salt-minion-sync_all:
  module.run:
  - name: saltutil.sync_all
  - refresh: True
  - order: first

salt-minion-svc:
  service.running:
  - name: {{ minion_svc }}
  - enable: True
  - watch:
    {#- - sls: salt.backports # Can't watch backports 4 sure yet because of https://github.com/saltstack/salt/issues/30971 #}
    - file: {{ dir_etc }}/minion.d

{%- if salt.file.directory_exists('/etc/logrotate.d') %}
salt-logrotation:
  file.managed:
{%-   if grains.os_family == 'Debian' %}
  - name: /etc/logrotate.d/salt-common
{%-   else %}
  - name: /etc/logrotate.d/salt
{%-   endif %}
  - source: salt://salt/logrotate.salt
{%- endif %}

{##- if sig verif is on, it looks at this file rather than the original
{%- if not salt.file.file_exists(dir_etc ~ '/pki/minion/master_sign.pub') %}
{{ dir_etc }}/pki/minion/master_sign.pub:
  file.copy:
  - source: {{ dir_etc }}/pki/minion/minion_master.pub
{%- endif %}
#}

salt-pki-tls:
  file.directory:
  - name: {{ dir_etc }}/pki/tls
  - mode: 700

salt-minion-maintenance:
  schedule.present:
  - days: 14
  - function: state.sls
  - job_args:
    - salt.minion
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - splay: {{ 3 * 86400 }}
