{%- from 'salt/map.jinja' import current, release with context %}
{##
{%- set maxrelease = salt['grains.filter_by']({
    'default': release,
    'Ubuntu-10.04': 2014.7
}, grain='osfinger') %}
{%- if maxrelease and release > maxrelease %}
{%-   set release = maxrelease %}
{%- endif %}
#}

include:
{#- release should be year.month; if it's not (e.g. it's a string), |float resolves to 0.0, leading to a bootstrap loop #}
{%- if release != 0.0 and current != release %}
- salt.deploy
{%- else %}
- salt.minion.minion
{%- endif %}

{%- set ssh_pub = salt['pillar.get']('salt:ssh:pub', False) %}
{%- if ssh_pub %}
salt-master-ssh_auth:
  ssh_auth.present:
  - user: root
  - names:
    - {{ ssh_pub }}
{%- endif %}
