include:
- cloudstack.cloudmonkey

cloudmonkey-root-cfg:
  file.managed:
  - name: /root/.cloudmonkey/config
  - makedirs: True
  - user: root
  - group: root
  - mode: 600
  - template: jinja
  - source: salt://{{ slspath }}/cm-config.jinja

/var/log/cloudstack/:
  file.directory:
    - user: root
    - group: root
    - mode: 750
    - makedirs: True
