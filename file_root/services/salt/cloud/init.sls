libcloud-pkg-absent:
  pkg.removed:
  - pkgs:
    - python-libcloud

libcloud-pip:
  pip.installed:
  - name: apache-libcloud
  - require:
    - pkg: libcloud-pkg-absent

{#- The Salt Masters don't deal with pillar data, so we need the profiles here anyway #}
{%- for data in ('providers', 'profiles') %}
{%-   set pillar_key = 'cloud:' ~ data  %}
{%-   if salt['pillar.get'](pillar_key, False) %}
salt-cloud-{{ data }}-pillar:
  file.serialize:
  - name: /etc/salt/cloud.{{ data }}.d/from-pillar.conf
  - formatter: yaml
  - user: root
  - group: root
  - mode: 640
  - makedirs: True
  - dataset_pillar: {{ pillar_key }}
{%-   endif %}
{%- endfor %}

salt-cloud-cache:
  file.directory:
  - name: /var/cache/salt/cloud
  - mode: 750
