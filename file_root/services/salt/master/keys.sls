{#include:#}
{#- ..master#}

{%- set pkid = '/etc/salt/pki/master' %}
{%- if salt['pillar.get']('salt:master:config:master_sign_pubkey', False) %}
{%-   set kname = 'master_sign' %}
{%- else %}
{%-   set kname = 'master' %}
{%- endif %}

{%- if salt['pillar.get']('salt:master:key', False) %}
salt-master-key:
  file.managed:
  - name: {{ pkid }}/{{ kname }}.pem
  - user: root
  - group: root
  - mode: 600
  - contents_pillar: salt:master:key
  - makedirs: True
{#  - watch_in:#}
{#    - service: salt-master#}

{%-   if not salt['pillar.get']('salt:master:pub', False) %}
salt-master-pub:
  cmd.run:
  - name: openssl rsa -in {{ kname }}.pem -pubout -out {{ kname }}.pub
  - cwd: {{ pkid }}
  - onchanges:
    - file: salt-master-key
{%-   endif %}
{%- endif %}
{%- if salt['pillar.get']('salt:master:pub', False) %}
salt-master-pub:
  file.managed:
  - name: {{ pkid }}/{{ kname }}.pub
  - user: root
  - group: root
  - mode: 644
  - contents_pillar: salt:master:pub
  - makedirs: True
{%- endif %}

{%- if salt['pillar.get']('salt:master:ssh:key', False) %}
salt-ssh-key:
  file.managed:
  - name: {{ pkid }}/ssh/salt-ssh.rsa
  - user: root
  - group: root
  - mode: 600
  - contents_pillar: salt:master:ssh:key
  - makedirs: True
{#  - watch_in:#}
{#    - service: salt-master#}

{%-   if not salt['pillar.get']('salt:master:ssh:pub', False) %}
salt-ssh-pub:
  cmd.run:
  - name: echo "`ssh-keygen -y -f salt-ssh.rsa` Salt Master" >salt-ssh.rsa.pub
  - cwd: {{ pkid }}/ssh
  - onchanges:
    - file: salt-ssh-key
{%-   endif %}
{%- endif %}
{%- if salt['pillar.get']('salt:master:ssh:pub', False) %}
salt-ssh-pub:
  file.managed:
  - name: {{ pkid }}/ssh/salt-ssh.rsa.pub
  - user: root
  - group: root
  - mode: 644
  - contents_pillar: salt:master:ssh:pub
  - makedirs: True
{%- endif %}

{%- set vault = salt['pillar.get']('vault:url', False) %}
{%- if vault %}
salt-vault-token:
  vaultx.token:
  - name: salt
  - url: {{ vault }}
  - orphan: True
  - policies:
    - provisioner
  - files:
      /etc/salt/pki/vault-token: token
      /etc/salt/pki/vault-accessor: accessor
  - require:
    - file: salt-master-etc
{%- endif %}
