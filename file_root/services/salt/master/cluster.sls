{%- set dirs = salt.cp.list_master_dirs() %}
{%- set clupath = pillar.salt.cluster.name ~ '/salt/master' %}
{%- set rosterpath = clupath ~ 'roster' %}

salt-master-etc-{{ pillar.salt.cluster.name }}:
  file.recurse:
  - name: /etc/salt/master.d/
  - source: salt://{{ clupath }}/etc/
  - use:
    - file: salt-master-etc

salt-master-ext-{{ pillar.salt.cluster.name }}:
  file.recurse:
  - name: /srv/salt/ext/
  - source: salt://{{ clupath }}/ext/
  - use:
    - file: salt-master-ext

{# ---------------- ROSTER ---------------- #}
{%- if salt.cp.list_master(prefix=rosterpath)|length > 0 %}
salt-master-oddball-roster:
  file.managed:
  - name: /etc/salt/roster
  - source: salt://{{ rosterpath }}
  - template: jinja
{%- endif %}
