#!/bin/sh
# In the ghettoooo
# CentOS 7! Xenial roughly working
# https://libgit2.github.com/docs/guides/build-and-link/
set -x
libgit=0.26.0
pygit=0.26.3

bdir=$(mktemp -d /tmp/lpgit2-${pygit}.XXXX)
cd $bdir

# Deps
if [ -x "$(which apt)" ]; then
    mirror=http://nl.archive.ubuntu.com/ubuntu/
    curl -fLsSv -o libssh2.deb ${mirror}/pool/universe/libs/libssh2/libssh2-1_1.8.0-1_amd64.deb
    curl -fLsSv -o libssh2-dev.deb ${mirror}/pool/universe/libs/libssh2/libssh2-1-dev_1.8.0-1_amd64.deb
    curl -fLsSv -o libgcrypt20.deb ${mirror}/pool/main/libg/libgcrypt20/libgcrypt20_1.7.8-2ubuntu1_amd64.deb
    curl -fLsSv -o libgcrypt20-dev.deb ${mirror}/pool/main/libg/libgcrypt20/libgcrypt20-dev_1.7.8-2ubuntu1_amd64.deb
    apt -y install libcurl-dev # TODO: Nags about 'which one' (DaFuq???)
    apt -y install make cmake gcc python2.7-dev libffi-dev ./*.deb
else
    yum -y install make cmake gcc python2-devel libffi-devel curl-devel libssh2 libssh2-devel
    yum -y erase python-pygit2 libgit2
fi

# Cleanup
rm -rf /usr/local/lib/libgit2* /lib64/libgit2* /tmp/pygit2-* /root/libgit2*

# Get from repo
mkdir libgit2
cd libgit2
curl -fLsSv -v https://github.com/libgit2/libgit2/archive/v${libgit}.tar.gz | tar xzv --strip-components=1 -f -

# Compile & link C libs
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_CLAR=OFF -DTHREADSAFE=ON .
make
make install

ln -s /usr/local/lib/libgit2.so.$(echo $libgit | cut -d. -f2) /lib64/
ldconfig

# Install PIP
echo y | pip uninstall pygit2
env LIBGIT2=/usr/local pip install pygit2===${pygit}

# Cleanup
rm -rf $bdir
