{%- from 'salt/map.jinja' import release with context %}
#####      File Server settings      #####
##########################################
# Salt runs a lightweight file server written in zeromq to deliver files to
# minions. This file server is built into the master daemon and does not
# require a dedicated port.

# The hash_type is the hash to use when discovering the hash of a file on
# the master server. The default is md5 but sha1, sha224, sha256, sha384
# and sha512 are also supported.
#
# WARNING: While md5 is supported, do not use it due to the high chance
# of possible collisions and thus security breach.
#
# Prior to changing this value, the master should be stopped and all Salt
# caches should be cleared.
#hash_type: md5
hash_type: sha256

# When using multiple environments, each with their own top file, the
# default behaviour is an unordered merge. To prevent top files from
# being merged together and instead to only use the top file from the
# requested environment, set this value to 'same'.
#top_file_merging_strategy: merge
top_file_merging_strategy: same

# File Server Backend
#
# Salt supports a modular fileserver backend system, this system allows
# the salt master to link directly to third party systems to gather and
# manage the files available to minions. Multiple backends can be
# configured and will be searched for the requested file in the order in which
# they are defined here. The default setting only enables the standard backend
# "roots" which uses the "file_roots" option.
#fileserver_backend:
#  - roots
#
# To use multiple backends list them in the order they are searched:
#fileserver_backend:
#  - git
#  - roots
fileserver_backend:
- roots
- git

# Git File Server Backend Configuration
#
# Optional parameter used to specify the provider to be used for gitfs. Must
# be one of the following: pygit2, gitpython, or dulwich. If unset, then each
# will be tried in that same order, and the first one with a compatible
# version installed will be the provider that is used.
#gitfs_provider: pygit2
gitfs_provider: pygit2

# Along with gitfs_privkey (and optionally gitfs_passphrase), is used to
# authenticate to SSH remotes. This parameter (or its per-remote counterpart)
# is required for SSH remotes.
#gitfs_pubkey: ''
gitfs_pubkey: /etc/salt/pki/master/ssh/salt-ssh.rsa.pub

# Along with gitfs_pubkey (and optionally gitfs_passphrase), is used to
# authenticate to SSH remotes. This parameter (or its per-remote counterpart)
# is required for SSH remotes.
#gitfs_privkey: ''
gitfs_privkey: /etc/salt/pki/master/ssh/salt-ssh.rsa

# The gitfs_ssl_verify option specifies whether to ignore ssl certificate
# errors when contacting the gitfs backend. You might want to set this to
# false if you're using a git backend that uses a self-signed certificate but
# keep in mind that setting this flag to anything other than the default of True
# is a security concern, you may want to try using the ssh transport.
#gitfs_ssl_verify: True
gitfs_ssl_verify: True

gitfs_base: {{ release }}

#####         Pillar settings        #####
##########################################
# The ext_pillar_first option allows for external pillar sources to populate
# before file system pillar. This allows for targeting file system pillar from
# ext_pillar.
#ext_pillar_first: False

# The pillar_opts option adds the master configuration file data to a dict in
# the pillar called "master". This is used to set simple configurations in the
# master config file that can then be used on minions.
#pillar_opts: False
pillar_opts: False

# The pillar_safe_render_error option prevents the master from passing pillar
# render errors to the minion. This is set on by default because the error could
# contain templating data which would give that minion information it shouldn't
# have, like a password! When set true the error message will only show:
#   Rendering SLS 'my.sls' failed. Please see master log for details.
#pillar_safe_render_error: True
pillar_safe_render_error: True

# Recursively merge lists by aggregating them instead of replacing them.
#pillar_merge_lists: False
pillar_merge_lists: True

# Git External Pillar (git_pillar) Configuration Options
#
# Specify the provider to be used for git_pillar. Must be either pygit2 or
# gitpython. If unset, then both will be tried in that same order, and the
# first one with a compatible version installed will be the provider that
# is used.
#git_pillar_provider: pygit2
git_pillar_provider: pygit2

# If the desired branch matches this value, and the environment is omitted
# from the git_pillar configuration, then the environment for that git_pillar
# remote will be base.
#git_pillar_base: master
git_pillar_base: {{ release }}

# If the branch is omitted from a git_pillar remote, then this branch will
# be used instead
#git_pillar_branch: master
git_pillar_branch: {{ release }}

# Specifies whether or not to ignore SSL certificate errors when contacting
# the remote repository.
#git_pillar_ssl_verify: False
git_pillar_ssl_verify: True

# When set to False, if there is an update/checkout lock for a git_pillar
# remote and the pid written to it is not running on the master, the lock
# file will be automatically cleared and a new lock will be obtained.
#git_pillar_global_lock: True
git_pillar_global_lock: False

# Along with git_pillar_privkey (and optionally git_pillar_passphrase),
# is used to authenticate to SSH remotes.
#git_pillar_pubkey: ''
git_pillar_pubkey: /etc/salt/pki/master/ssh/salt-ssh.rsa.pub

# Along with git_pillar_pubkey (and optionally git_pillar_passphrase),
# is used to authenticate to SSH remotes.
#git_pillar_privkey: ''
git_pillar_privkey: /etc/salt/pki/master/ssh/salt-ssh.rsa

gpg_keydir: /etc/salt/pki/gpg

# The default renderer used for decryption, if one is not specified for a given
# pillar key in decrypt_pillar.
#decrypt_pillar_default: gpg

# List of renderers which are permitted to be used for pillar decryption.
#decrypt_pillar_renderers:
#  - gpg


# Set this option to True to force the pillarenv to be the same as the effective
# saltenv when running states. If pillarenv is specified this option will be
# ignored.
#pillarenv_from_saltenv: False

# Set this option to 'True' to force a 'KeyError' to be raised whenever an
# attempt to retrieve a named value from pillar fails. When this option is set
# to 'False', the failed attempt returns an empty string. Default is 'False'.
#pillar_raise_on_missing: False

{%- set vault = salt['pillar.get']('vault', False) %}
{%- if vault and 'url' in vault %}
vault:
  url: {{ vault.url }}
{%-   set token = vault.get('token', salt.cp.get_file_str('/etc/salt/pki/vault-token')|trim) %}
{%-   if token %}
  auth:
    method: token
    token: {{ token }}
{%-   endif %}
  verify: /etc/vault/ca.pem
  # TODO this *really* needs to come from pillar,
  # -but- adding {pillar} might/will introduce a doomloop; see
  # https://github.com/saltstack/salt/pull/49343
  # https://github.com/saltstack/salt/issues/49671
  policies:
{%-   for policy in vault.policies.keys() if policy.startswith('pki-') %}
  - {{ policy }}
{%-   endfor %}
{%- endif %}
