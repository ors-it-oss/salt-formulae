salt-master-maintenance:
  schedule.present:
  - days: 30
  - function: state.sls
  - job_args:
    - salt.master
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - splay: {{ 7 * 86400 }}

{#- Force a refresh once a day because sometimes it just doesn't fly #}
salt-master-fileroots:
  schedule.present:
  - days: 1
  - function: saltutil.runner
  - job_args:
    - fileserver.update
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - splay: {{ 21600 }}

{#- TODO: Of course not all Salt masters will have git repo's #}
salt-master-pillar:
  schedule.present:
  - days: 1
  - function: saltutil.runner
  - job_args:
    - git_pillar.update
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - splay: {{ 21600 }}
