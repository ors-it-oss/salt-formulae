{%- from 'shared.jinja' import proxy, list_if, list_if_pillar with context %}
{%- from 'salt/map.jinja' import master_deps %}
{%- from 'salt/master/etc/tune.conf' import max_open_files with context %}
{{ proxy('salt-master') }}

include:
- .syndic
{{ list_if_pillar('salt:cluster', '.cluster') }}
{{ list_if_pillar('cloud', '..cloud') }}
{{ list_if_pillar('salt:repos', '..repos') }}
{{ list_if(salt['pillar.get']('salt:master:cache', False) == 'redis', 'redis.server') }}

{%- set init = grains.get('init', False) %}
salt-master-ulimits:
{%- if init == 'upstart' %}
  file.replace:
  - name: /etc/init/salt-master.conf
  - pattern: ^limit nofile .*
  - repl: limit nofile {{ max_open_files }} {{ max_open_files }}
  - watch_in:
    - service: salt-master
{%- elif init == 'systemd' %}
  file.managed:
  - name: /etc/systemd/system/salt-master.service.d/limits.conf
  - makedirs: True
  - contents: |
      [Service]
      LimitNOFILE={{ max_open_files }}

salt-master-svc-reload:
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: salt-master-ulimits
  - watch_in:
    - service: salt-master
{%- endif %}

salt-master:
  pkg.installed:
  - pkgs: {{ master_deps }}
  pip.installed:
  - names:
    - cerberus
    - pyOpenSSL
  service.running:
  - enable: True
  - watch:
    - file: salt-master-etc
    - file: salt-master-ext

