{%- from 'shared.jinja' import proxy, list_if, list_if_pillar with context %}
{%- from 'salt/map.jinja' import master_deps %}
{%- from 'salt/master/etc/tune.conf' import max_open_files with context %}
{{ proxy('salt-master') }}

include:
- .keys
{{ list_if_pillar('salt:cluster', '.cluster') }}
{{ list_if_pillar('cloud', '..cloud') }}
{{ list_if_pillar('salt:repos', '..repos') }}
{{ list_if(salt['pillar.get']('salt:master:cache', False) == 'redis', 'redis.server') }}

salt-master-etc:
  file.recurse:
  - name: /etc/salt/master.d
  - user: root
  - group: root
  - file_mode: 640
  - template: jinja
  - include_pat: '*.conf'
  - source: salt://{{ slspath }}/etc

{%- if salt['pillar.get']('salt:ssh:roster', False) %}
salt-ssh-roster:
  file.serialize:
  - name: /etc/salt/roster
  - formatter: yaml
  - dataset_pillar: salt:ssh:roster
{%- endif %}
