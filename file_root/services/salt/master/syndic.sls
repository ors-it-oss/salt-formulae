salt-syndic:
{%- if salt['pillar.get']('salt:master:config:syndic_master', False) %}
  service.running: []
{%- else %}
  service.disabled: []
{%- endif %}
