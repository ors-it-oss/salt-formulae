#!/bin/sh -
# Wrapper around SaltStack's bootstrap script
# Besides being smart & handy, additional important stuff done:
#   - preseed keys when required
#   - run a sync_all before starting up
#   - various cleanups & workarounds etc.

################################ DEFAULTS / SYSSETTINGS ################################
if [ -z "$BOOTSTRAP" ]; then
    #BOOTSTRAP="https://bootstrap.saltstack.com"
    BOOTSTRAP="https://raw.githubusercontent.com/saltstack/salt-bootstrap/develop/bootstrap-salt.sh"
    #BOOTSTRAP="/usr/local/sbin/bootstrap-salt.sh"
fi

if [ -z "$GITREPO_URL" ]; then
    GITREPO_URL="https://github.com/saltstack/salt.git"
    #GITREPO_URL="https://github.com/The-Loeki/salt.git"
fi

usage(){
    cat <<USAGE
-f errors
-D for debug
-p for profile (default=auto, minion, master, supermaster)
-g with different Git repo URL
-i minion ID (default: hostname -f)
-k for dir containing preseeded keypairs (master/minion.pem/pub)
-A SaltMaster DNS (as opposed to 'salt')
-H HTTP(S)/FTP Proxy server
USAGE
}

conf_get(){
    salt-call --local --out=newline_values_only config.get $1 | tail -1
}

conf_dir(){
    dirname $(conf_get $1)
}


################################ PARSE ARGS & PREFLIGHT CHECKS ################################
while getopts ":h?Dfp:A:k:i:g:H:" opt; do
    case "${opt}" in
     [h?]) usage; exit 0 ;;
        f) ignore_errors="True" ;;
        D) set -x; debug="True" ;;
        i) minion_id="$OPTARG" ;;
        k) keydir=$OPTARG; [ -d "$keydir" ] || { echo "Keydir $keydir not found"; exit 1; } ;;
        p) profile="$OPTARG" ;;
        A) saltmaster="$OPTARG" ;;
        g) gitrepo="$OPTARG" ;;
        H) export HTTP_PROXY="$OPTARG"
           export HTTPS_PROXY="$HTTP_PROXY"
           export FTP_PROXY="$HTTP_PROXY"
           export http_proxy="$HTTP_PROXY"
           export https_proxy="$HTTPS_PROXY"
           export ftp_proxy="$FTP_PROXY" ;;
        *) echo "BZZZZZT WRONG" ; usage; exit 1 ;;
esac; done; shift $((OPTIND-1))

[ "$profile" = "supermaster" -a -z "$saltmaster" ] && saltmaster=localhost
[ -n "$gitrepo" ] || gitrepo="$GITREPO_URL"

release=$*
if [ -x "$(which salt-call 2>/dev/null)" ]; then
    dir_etc="$(conf_dir conf_file)"
    saltmaster="${saltmaster:-$(conf_get master)}"
fi
if [ $(uname -s) = "FreeBSD" ]; then
    dir_etc="${dir_etc:=/usr/local/etc/salt}"
    svc_sep=_
    # pkgng has 0 capability of pegging releases
    if echo "$release" | grep -qv "git"; then
        release=$(echo "$release" | cut -d" " -f1)
    fi
fi
dir_etc="${dir_etc:=/etc/salt}"
svc_sep="${svc_sep:=-}"

ping6 -n -c 3 "${saltmaster:-salt}" >/dev/null 2>&1
[ $? -eq 0 ] || echo "WARNING: Salt Master ${saltmaster:-salt} not found! Soldiering on..."

if [ -z "$minion_id" ]; then
  if [ -s "${dir_etc}/minion_id" ]; then
    minion_id=$(cat ${dir_etc}/minion_id)
  else
    minion_id=$(hostname -f)
    if [ -z "$minion_id" ]; then
      minion_id=$(hostname)
fi; fi; fi

if [ "${profile:=auto}" = "auto" ]; then
    profile="minion"
    # It's a bit sorry this, but when installing through git:
    # how to reliably detect what is supposed to happen?
    # pkgs could utilize the pkg cache
    # maybe look at symlinks to these executables?
    if ps -ef | grep -q salt[-_]syndic; then
        profile="master"
    elif ps -ef | grep -q salt[-_]master; then
        profile="supermaster"
fi; fi


################################ PREP MINION DIR ################################
# Make sure necessary dirs exist beforehand
if [ ! -d "${dir_etc}/minion.d/" ]; then
    mkdir -m 750 -p ${dir_etc}/minion.d
fi

# Provide keys when they're not already there
# salt-call will bork horribly if this doesn't exist
mkdir -p -m 700 ${dir_etc}/pki/minion 2>/dev/null
if [ -n "$keydir" ]; then
    for keypair in master minion; do
        kpdir=${dir_etc}/pki/${keypair}
        kppem=$kpdir/${keypair}.pem
        kppub=$kpdir/${keypair}.pub

        if [ ! -f "${keydir}/${keypair}.pem" -o ! -f "${keydir}/${keypair}.pub" ]; then
            continue
        # never overwrite existing keys (or is that the other way around?)
        elif [ -f "$kppem" ]; then
            echo "$kppem already exists, skipping!" >&2
            continue
        fi

        mkdir -m 700 $kpdir
        cp "${keydir}/${keypair}.pem" $kppem
        chmod 600 $kppem
        cp "${keydir}/${keypair}.pub" $kppub
        chmod 644 $kppub
done; fi

# Enable PKI based authentication
#if [ ! -f ${dir_etc}/pki/minion/master_sign.pub -a -f ${dir_etc}/pki/minion/minion_master.pub ]; then
#  cat > "${dir_etc}/minion.d/99-master-connect.conf" <<CONN
#verify_master_pubkey_sign: True
#CONN
#  cp -a ${dir_etc}/pki/minion/minion_master.pub ${dir_etc}/pki/minion/master_sign.pub
#fi


################################ CLEANUP POSSIBLY CRIPPLING CRUFT ################################
# SystemD will invariably destroy all service children
# we escape that horrible fate by becoming a task(?)
if [ -f /sys/fs/cgroup/systemd/tasks ]; then
    echo $$ >/sys/fs/cgroup/systemd/tasks
fi

# When Salt's already there as a package and we're switching to git
if [ -x "$(which salt-call 2>/dev/null)" ] && $(echo "$release" | grep -q "git"); then
    salt-call --local pkg.remove salt-minion,salt-master,salt-common,salt-syndic,salt
fi

# Clean old repos
rm -f /etc/yum.repos.d/salt*.repo /etc/apt/sources.list.d/salt*.list 2>/dev/null

# Clean the py dir out beforehand
rm -rf /usr/lib/python*/{dist,site}-packages/salt*

# Clean the config dirs out
find ${dir_etc}/minion.d/ -name '*.conf' -a -not \( -regex '.+/[A-Z]*[0-9]+-.+' -or -name 'base*' -or -name '_*' \) -exec rm -f {} \;
find ${dir_etc}/master.d/ -name '*.conf' -a -not \( -regex '.+/[A-Z]*[0-9]+-.+' -or -name 'base*' -or -name '_*' \) -exec rm -f {} \;

################################ INSTALL BY BOOTSTRAP ################################
case "$profile" in
   minion) continue ;;
   master) BSOPTS="$BSOPTS -M -L -S -p gnupg2" ;;
   supermaster) BSOPTS="$BSOPTS -M -L -p gnupg2" ;;
   *) echo "Unknown profile $profile; choose minion, master or supermaster" ; exit 1 ;;
esac
BSOPTS="$BSOPTS -d -n -X -P ${http_proxy:+-H $http_proxy} ${minion_id:+-i $minion_id}"
[ -x "$(which python3)" ] && BSOPTS="$BSOPTS -x python3"

[ "$debug" = "True" ] && BSOPTS="$BSOPTS -D"
[ -n "$saltmaster" ] && BSOPTS="$BSOPTS -A $saltmaster"
[ -n "$gitrepo" ] && BSOPTS="$BSOPTS -g $gitrepo"

[ "${debug:-False}" = "True" ] && echo "Bootstrapping with $BSOPTS $release (which were derived from $profile)..."

if [ -s "$BOOTSTRAP" ]; then
  bootstrap_script="$BOOTSTRAP"
else
  bootstrap_script=/tmp/salt-bootstrap.sh
  if [ -x "$(which wget)" ]; then
      wget -T 60 -L $BOOTSTRAP --no-check-certificate -O $bootstrap_script
  elif [ -x "$(which curl)" ]; then
      curl -fLsSv -k -m 60 ${http_proxy:+ -x $http_proxy} -L $BOOTSTRAP  -o $bootstrap_script
  elif [ -x "$(which fetch)" ]; then
      fetch -T 60 -a -o $bootstrap_script $BOOTSTRAP
  else
      echo "NO DOWNLOADER FOUND!!!"; exit 1
  fi
  res=$?
  [ $res -eq 0 ] || { echo "Unable to download salt-bootstrap!"; exit $res; }
fi

cat $bootstrap_script | sh -s -- $BSOPTS $release
res=$?

[ -x "$(which salt-call 2>/dev/null)" ] && log_dir=$(conf_dir log_file)
mv /tmp/bootstrap-salt.log ${log_dir:-/var/log/salt}/

[ $res -eq 0 -o -n "$ignore_errors" ] || { echo "salt-bootstrap failed, and so shall salt-deploy!"; exit $res; }

################################ OUR SECRET SALTY SAUCE ################################
service salt${svc_sep}minion stop
pkill salt${svc_sep}minion
pkill -9 salt${svc_sep}minion

# In case this came from a salt-call; state will technically remain running
pkill salt-call
pkill -9 salt-call

# Clear the cache
cacheDir=$(conf_dir cachedir)
[ -n "$cacheDir" ] && rm -rf ${cacheDir}/*

# sync_all before going live
if [ "$profile" != "supermaster" ]; then
    if [ -x "$(which timeout)" ]; then
        timeout 5m salt-call ${debug:+-l debug} saltutil.sync_all
    else
        salt-call ${debug:+-l debug} saltutil.sync_all
fi; fi

service salt${svc_sep}minion start

case "$profile" in
    master)
        service salt${svc_sep}master stop
        # Sometimes it just won't die
        pkill salt${svc_sep}master
        salt-call ${debug:+-l debug} state.sls salt.master queue=True
        service salt${svc_sep}syndic restart
        service salt${svc_sep}master start
        salt --async -b 4 '*' saltutil.sync_all ;;
    supermaster)
        service salt${svc_sep}master stop
        # Sometimes it just won't die
        pkill salt${svc_sep}master
        service salt${svc_sep}master start
        salt-call ${debug:+-l debug} state.sls salt.master queue=True
        salt --async -b 4 '*' saltutil.sync_all ;;
esac
