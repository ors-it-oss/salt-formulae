{%- set config = salt['pillar.get']('dehydrated:config', {}) %}
{%- set scriptdir = config.get('scriptdir', '/opt/dehydrated') %}
{%- set basedir = config.get('basedir', '/etc/dehydrated') %}

Extract dehydrated files:
   archive.extracted:
      - name: {{ scriptdir }}
      - source: https://github.com/lukas2511/dehydrated/archive/master.tar.gz
      - type: tgz
      - user: root
      - group: root
      - if_missing: {{ scriptdir }}
      - skip_verify: True
      - options: --strip-components=1
      - enforce_toplevel: False

{%- set hook_script = salt.file.join(basedir, 'hook.sh') %}
{%- if not salt.file.file_exists(hook_script) or config.get('hook') %}
Copy hook.sh script, but only if it does not exist:
  file.managed:
    - name: {{ hook_script }}
    - source: salt://{{ config.get('hook', slspath ~ '/files/hook.sh') }}
    - user: root
    - group: root
    - mode: 700
{%- endif %}

{%- set domains = salt.file.join(basedir, 'domains.txt') %}
{%- if not salt.file.file_exists(domains) or config.get('domains_txt') %}
Copy domains.txt file, but only if it does not exist:
  file.managed:
    - name: {{ domains }}
    - source: salt://{{ config.get('domains_txt', slspath ~ '/files/domains.txt') }}
    - user: root
    - group: root
    - mode: 600
{%- endif %}

Copy dehydrated configuration:
  file.managed:
    - name: {{ basedir }}/config
    - mode: 400
    - makedirs: True
    - user: root
    - group: root
    - contents: |
        # Config file for dehydrated
        BASEDIR="{{ basedir }}"
        DOMAINS_TXT="${BASEDIR}/domains.txt"
        CERTDIR="{{ config.get('certdir', '${BASEDIR}/certs') }}"
        ACCOUNTDIR="{{ config.get('accountdir', '${BASEDIR}/accounts') }}"
        WELLKNOWN="{{ config.get('wellknown', '/var/www/dehydrated') }}"
        KEYSIZE="{{ config.get('keysize', '4096') }}"
        HOOK="${BASEDIR}/hook.sh"

Configure dehydrated-renew cronjob:
  cron.present:
    - name: /opt/dehydrated/dehydrated -c
    - identifier: dehydrated-renew
    - special: '@daily'

Create dehydrated symlink in /usr/local/sbin:
  file.symlink:
    - name: /usr/local/sbin/dehydrated
    - target: {{ salt.file.join(scriptdir, 'dehydrated') }}
    - backupname: /usr/local/sbin/dehydrated.saltbackup
    - user: root
    - group: root
    - mode: 755

Register Lets Encrypt account:
  cmd.run:
    - name: dehydrated --register --accept-terms
    - creates: {{ config.get('accountdir', salt.file.join(basedir, 'accounts')) }}
    - requires:
      - file: Create dehydrated symlink in /usr/local/sbin
