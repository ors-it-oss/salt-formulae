{%- from 'ceph/lib.jinja' import ceph_conf with context %}
include:
  - ceph.common

{%- for cluster, context in salt['pillar.get']('ceph:cluster', {'ceph': pillar.ceph}).items() %}
{{ ceph_conf('client', context=context, cluster=cluster) }}
{%- endfor %}
