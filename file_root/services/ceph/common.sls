{%- from 'shared.jinja' import salt_header -%}
{%- from 'ceph/lib.jinja' import ceph_conf, ceph_keyring with context %}
include:
  - repo.ceph

ceph_pkgs:
  pkg.installed:
    - pkgs:
      - ceph
    - require:
      - sls: repo.ceph

{%- for cluster, context in salt['pillar.get']('ceph:cluster', {'ceph': pillar.ceph}).items() %}
{%-   if cluster == 'ceph' %}
{%-     set prefix = 'ceph' %}
{%-   else %}
{%-     set prefix = 'ceph-' ~ cluster %}
{%-   endif %}
{%-   set config = '/etc/ceph/' ~ cluster ~ '.conf' %}
{{ prefix }}-config:
  file.managed:
    - name: {{ config }}
    - create: True
    - replace: False

{{ ceph_conf('global', context=context, cluster=cluster, config=config) }}

{{ prefix }}-conf-header:
  file.replace:
    - name: {{ config }}
    - pattern: '.*\n######## BEGIN SaltStack \[global\] block ########\n'
    - repl: |
        {{ salt_header(msgs='It will wipe everything above the [global] block')|indent(8) }}
        ######## BEGIN SaltStack [global] block ########
    - flags:
      - MULTILINE
      - DOTALL
    - require:
        - file: {{ prefix }}-conf-global

{%-   for user, key in context.get('key', {}).items() %}
{{      ceph_keyring(user, key, cluster=cluster) }}
{%-   endfor %}
{%- endfor %}
