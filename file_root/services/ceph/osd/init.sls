{%- from 'ceph/lib.jinja' import ceph_conf, ceph_keyring with context %}
include:
  - ceph.common

ceph-osd-disable-weekly-trim:
  file.absent:
    - name: /etc/cron.weekly/fstrim

ceph-osd-pkgs:
  pkg.installed:
    - pkgs:
      - btrfs-tools
      - xfsprogs

{{ ceph_conf('osd') }}

{% if salt['pillar.get']('ceph:osd:bootstrap', False) %}
{{ ceph_keyring('client.bootstrap-osd', pillar.ceph.osd.bootstrap, keyring='/var/lib/ceph/bootstrap-osd/ceph.keyring') }}
{%- endif %}
