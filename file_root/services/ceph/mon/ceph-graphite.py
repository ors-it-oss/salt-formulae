#!/usr/bin/env python
'''
This script fetches information from a Ceph cluster using 'ceph -s' with a JSON format
output.

It sends the information to a Graphite server where it can be used for graphing.

Written to be used at PCextreme B.V. and probably doesn't cover everything you want to
graph.
'''

import time
import socket
import subprocess
import json

graphite_host = 'graphite.pcextreme.nl'
graphite_port = 2005

prefix = 'ceph.zone01.ams02'

def collect_metric(name, value, timestamp):
    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)
    sock.connect((graphite_host, graphite_port, 0, 0))
    sock.send("%s %0.3f %d\n" % (name, value, timestamp))
    sock.close()

def execute(command):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=False)
    (result, err) = p.communicate()
    return result.strip()

status = json.loads(execute(["ceph", "-s", "--format", "json-pretty"]))

stats = {
          "pgmap.data_bytes": status['pgmap']['data_bytes'],
          "pgmap.bytes_used": status['pgmap']['bytes_used'],
          "pgmap.bytes_avail": status['pgmap']['bytes_avail'],
          "pgmap.bytes_total": status['pgmap']['bytes_total'],
          "pgmap.read_bytes_sec": status['pgmap']['read_bytes_sec'],
          "pgmap.write_bytes_sec": status['pgmap']['write_bytes_sec'],
          "pgmap.op_per_sec": status['pgmap']['op_per_sec'],
          "pgmap.degraded_objects": 0,
          "pgmap.degraded_ratio": 0.0,
          "pgmap.recovering_objects_per_sec": 0,
          "pgmap.recovering_bytes_per_sec": 0,
          "osdmap.num_osds": int(status['osdmap']['osdmap']['num_osds']),
          "osdmap.num_up_osds": int(status['osdmap']['osdmap']['num_up_osds']),
          "osdmap.num_in_osds": int(status['osdmap']['osdmap']['num_in_osds'])
        }

if 'degraded_objects' in status['pgmap']:
    stats['pgmap.degraded_objects'] = status['pgmap']['degraded_objects']

if 'degrated_ratio' in status['pgmap']:
    stats['pgmap.degraded_ratio'] = float(status['pgmap']['degrated_ratio'])

if 'recovering_objects_per_sec' in status['pgmap']:
    stats['pgmap.recovering_objects_per_sec'] = status['pgmap']['recovering_objects_per_sec']

if 'recovering_bytes_per_sec' in status['pgmap']:
    stats['pgmap.recovering_bytes_per_sec'] = status['pgmap']['recovering_bytes_per_sec']

now = int(time.time())

for key in stats.keys():
    print "%s:%0.3f" % (key, stats[key])
    collect_metric(prefix + "." + key, stats[key], now)
