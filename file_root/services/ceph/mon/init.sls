{%- from 'ceph/lib.jinja' import ceph_conf with context %}
include:
  - ceph.common

{{ ceph_conf('mon') }}
