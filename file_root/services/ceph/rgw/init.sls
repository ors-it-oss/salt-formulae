{%- from 'ceph/lib.jinja' import ceph_conf, ceph_keyring with context %}
include:
  - ceph.common

ceph-radosgw-pkgs:
  pkg.installed:
    - pkgs:
      - radosgw
      - radosgw-agent

{{ ceph_keyring('radosgw.' ~ grains.host, pillar.ceph.rgw.key) }}

{{ ceph_conf('rgw', context={'rgw': pillar.ceph.rgw}) }}
