# -*- coding: utf-8 -*-
'''
WSGI app for Ceph REST API
http://docs.ceph.com/docs/hammer/man/8/ceph-rest-api/
'''

# Import python libs
from __future__ import absolute_import
# import sys
# import logging
import ceph_rest_api


argmap = {
    'CEPH_BASEURL':   '--restapi_base_url',
    'CEPH_LOG_LEVEL': '--restapi_log_level',
    'CEPH_LOG_FILE':  '--restapi_log_file',
}


def application(params, start_response):
    '''
    Thin wrapper around ceph_rest_api to make this file stateless
    and depend on environment variables for config so all config can be done @VHost level
    '''
    kwargs = {
        'clientid': params.get('CEPH_CLIENTID', ceph_rest_api.DEFAULT_ID),
        'conf': params.get('CEPH_CONF', '/etc/ceph/ceph.conf'),
        'cluster': params.get('CEPH_CLUSTER', 'ceph'),
        'args': []
    }
    kwargs['clientname'] = params.get('CEPH_CLIENTNAME', 'client.' + kwargs['clientid'])

    for param, arg in argmap.items():
        if param in params:
            kwargs['args'].extend([arg, params[param]])

    return ceph_rest_api.generate_app(**kwargs)(params, start_response)
