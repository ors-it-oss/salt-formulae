{%- from 'ceph/lib.jinja' import ceph_conf, ceph_key with context %}
include:
  - ceph.client
  - .httpd

/var/log/ceph_api:
  file.directory:
    - user: www-data
    - group: adm
    - makedirs: True
    - dir_mode: 750
    - file_mode: 640
    - recurse:
      - user
      - group
      - mode

/etc/logrotate.d/ceph_api:
  file.managed:
    - source: salt://{{ slspath }}/logrotate.conf

{#----------------- CEPH CONF & AUTH ---------------- #}
{# WARNING: http://tracker.ceph.com/issues/12403 #}
{# aka you *need* mon 'allow r' osd 'allow *' for 'ro' :/ #}
{%- for endpoint, data in pillar.ceph.api.endpoint.items() %}
{%-   set client_id = data.get('id', 'restapi_' ~ endpoint) %}
{%-   set client = data.get('client', 'client.' ~ client_id) %}
{%-   load_yaml as context %}
  ceph_endpoint: {{ endpoint }}
  ceph_id: {{ client_id }}
  ceph_client: {{ client }}
{%-   endload %}
{{ ceph_conf(client, source='salt://' ~ slspath ~ '/restapi.jinja', context=context) }}
{{ ceph_key(client, data.key, user='www-data') }}
{%- endfor %}
