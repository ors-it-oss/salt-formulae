{%- from 'shared.jinja' import pki with context %}
{%- set svc_name = 'api.' ~ grains.domain %}
{#----------------- APACHE ---------------- #}
wsgi-pkgs:
  pkg.installed:
    - pkgs:
      - apache2
      - libapache2-mod-wsgi

http_server:
  service.running:
    - name: apache2
    - enable: True
    - require:
      - pkg: wsgi-pkgs
    - watch:
      - file: pki-{{ svc_name }}-key

/etc/apache2/sites-enabled/000-default.conf:
  file.absent:
    - watch_in:
      - service: http_server

{%- for mod in ('headers', 'env', 'ssl', 'wsgi', 'auth_basic', 'ldap', 'authnz_ldap') %}
{{ mod }}-module:
  apache_module.enable:
    - name: {{ mod }}
    - watch_in:
      - service: http_server
{%- endfor %}


{#----------------- CERT ---------------- #}
{{ salt.tmpltools.pki(pillar='ceph:api:cert', name=svc_name, user='www-data', dhparams=True) }}


{#----------------- SITECONF ---------------- #}
{%- set vhost_conf = '300-ceph_api.conf' %}
wsgi_vhost:
  file.managed:
    - name: /etc/apache2/sites-available/{{ vhost_conf }}
    - template: jinja
    - source: salt://{{ slspath }}/vhost.conf.jinja
    - watch_in:
      - service: http_server

wsgi_vhost_enable:
  file.symlink:
    - name: /etc/apache2/sites-enabled/{{ vhost_conf }}
    - target: ../sites-available/{{ vhost_conf }}
    - require:
      - file: wsgi_vhost
    - watch_in:
      - service: http_server


{#----------------- CONTENT ---------------- #}
/var/www/html/index.html:
  file.managed:
    - source: salt://{{ slspath }}/index.html.jinja
    - template: jinja

/var/www/html/ceph.jpg:
  file.managed:
    - source: salt://{{ slspath }}/ceph.jpg

/var/www/ceph_api.wsgi:
  file.managed:
    - source: salt://{{ slspath }}/ceph_api.wsgi
    - group: www-data
    - mode: 640
    - template: jinja
