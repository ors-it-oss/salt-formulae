gitlab-config:
  file.managed:
  - name: /etc/gitlab/gitlab.rb
  - source: salt://{{ slspath }}/gitlab.rb.j2
  - template: jinja
  - mode: 600

gitlab-reconfigure:
  cmd.run:
  - name: gitlab-ctl reconfigure
  - onchanges:
    - file: gitlab-config
  - use_vt: True
