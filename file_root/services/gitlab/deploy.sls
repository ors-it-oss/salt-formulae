{# TEMPLATE: https://gitlab.com/gitlab-org/omnibus-gitlab/raw/master/files/gitlab-config-template/gitlab.rb.template #}
{%- if 'gitlab' not in (salt.pkg.list_repos().keys()|join(',')) %}
{%-   if grains.os_family == 'Debian' %}
{%-     set reposcript = 'script.deb.sh' %}
{%-   elif grains.os_family == 'RedHat' %}
{%-     set reposcript = 'script.rpm.sh' %}
{%-   endif %}
gitlab-ce-repo:
  cmd.run:
  - name: 'curl -fLsSv https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/{{ reposcript }} | bash'
  - require_in: gitlab-ce-pkg
{%- endif %}

gitlab-ce-pkg:
  pkg.latest:
  - name: gitlab-ce
  - refresh: True
