{#---------------- INJECT KEYS & KNOWN HOSTS ETC. ----------------#}
{%- from 'shared.jinja' import ssh_key with context %}
{%- set gitlab = pillar.gitlab.runner.coordinator %}
{%- set run_path = '/var/tmp/gitlab-runner' %}
{#{%- set sock_path = run_path ~ '/ssh_agent.sock' %}#}
{%- set ssh_path = '/etc/gitlab-runner/ssh' %}
{%- set pub_path = '/etc/gitlab-runner/pub' %}

glrun-ssh-keys:
  file.directory:
  - name: {{ ssh_path }}
  - user: root
  - group: root
  - mode: 700
  - makedirs: True

glrun-ssh-pub:
  file.directory:
  - name: {{ pub_path }}
  - mode: 755
  - makedirs: True

glrun-vardir:
  file.directory:
  - name: {{ run_path }}
  - dir_mode: 755
  - makedirs: True
  - user: gitlab-runner
  - group: root
  - recurse: False

{##-
  TODO: ssh.recv_known_host should support multiple keys
  TODO: ssh_known_hosts.present doesn't support multiple keys in known_hosts (https://github.com/saltstack/salt/issues/31117)
  TODO: Remove once Docker betters it's DNS life
  Docker started shipping their own 'embedded' DNS.
  Naturally as a resolver it sux.
  While it does use the hosts' resolvers as forwarders, it doesn't do TCP, let alone DNSSEC
  This makes it impossible to use SSHFP records.

  https://github.com/docker/docker/issues/19474
#}
glrun-ssh-known_hosts:
  file.managed:
  - name: {{ pub_path }}/known_hosts
  - user: root
  - group: root
  - mode: 644
  - contents: |
{%- for ssh_key in ['rsa', 'ed25519'] %}
{%-   set key_data = salt.ssh.recv_known_host(gitlab, ssh_key, hash_known_hosts=False) %}
      {{ key_data.hostname }} {{ key_data.enc }} {{ key_data.key }}
{%- endfor %}

{%- for key_id, key_data in pillar.gitlab.runner.ssh.items() %}
{{ ssh_key(
  key_data.pub,
  key_data.key,
  format=key_data.enc,
  name=key_id,
  user='root',
  group='root',
  key_path=ssh_path, pub_path=pub_path)
}}
{%- endfor %}

{#- Allow ssh agent to sudo-start without a TTY #}
glrun-sshbridge-sudoers:
  file.managed:
  - name: /etc/sudoers.d/gl-ssh-agent
  - source: salt://{{ slspath }}/conf/sudoers

{#- Register custom SSH Agent services #}
{%- for exec in ('shell', 'docker') %}
{%-   if exec != 'docker' %}
{%-     set glr_user = 'gitlab-runner' %}
{%-   else %}
{%-     set userns = salt['pillar.get']('docker:config:userns-remap', False) %}
{%-     if not userns %}
{%-       set glr_user = 'gitlab-runner' %}
{%-     else %}
{## Actually there's very little reason to assume more than one user in this ns-mapping thingy
{%-       if userns == 'default' %}
{%-         do set userns = 'dockremap' %}
{%-       endif %}
#}
{%-       set glr_user = salt.cp.get_file_str('/etc/subuid').split(':')[1]  %}
{%-     endif %}
{%-   endif %}

glrun-{{ exec }}-sshagent-svc:
  file.managed:
  - name: /etc/systemd/system/gitlab-sshagent-{{ exec }}.service
  - template: jinja
  - source: salt://{{ slspath }}/conf/ssh-agent.service.jinja
  - context:
      user: {{ glr_user }}
      executor: {{ exec }}
      run_path: {{ run_path }}/{{ exec }}
      key_path: {{ ssh_path }}
      pub_path: {{ pub_path }}
  service.running:
  - name: gitlab-sshagent-{{ exec }}
  - enable: True

glrun-{{ exec }}-sshagent-svc-reload:
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: glrun-{{ exec }}-sshagent-svc
  - watch_in:
    - service: glrun-{{ exec }}-sshagent-svc
  - require_in:
    - service: glrun-{{ exec }}-sshagent-svc

{%- endfor %}
