{%- from 'shared.jinja' import arg_if_pillar with context %}
{%- from tpldir ~ '/map.jinja' import glrun_map with context %}
include:
- .register

{%- if not glrun_map.repo in salt.pkg.list_repos() %}
glrun-repo:
  cmd.run:
  - name: curl -fLsSv https://packages.gitlab.com/install/repositories/runner/gitlab-runner/{{ glrun_map.script }} | bash
  - require_in: glrun-pkg
{%- endif %}

glrun-pkg:
  pkg.installed:
  - name: gitlab-runner
  - refresh: True

glrun-concurrency:
  file.replace:
  - name: /etc/gitlab-runner/config.toml
  - pattern: ^concurrent = .*
  - repl: concurrent = {{ grains.num_cpus * 4 }}
  - require:
    - pkg: glrun-pkg

{%- if salt.group.info('docker')  %}
glrun-docker:
  group.present:
  - name: docker
  - addusers:
    - gitlab-runner

glrun-docker-builder:
  file.managed:
  - name: /usr/local/bin/docker_build
  - mode: 755
  - contents: |
      #!/bin/sh
      exec docker build --compress --pull --squash --network local $*
{%- endif %}
