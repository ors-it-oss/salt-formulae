{%- from 'shared.jinja' import list_if %}
{%- set deploy_key = pillar.gitlab.runner.get('ssh', False) %}
include:
- gitlab.runner
{{ list_if(deploy_key, '.deploy_keys') }}

{# TODO: If docker(-(ssh|machine)) in executors, include docker state #}
{#---------------- Generic arguments ----------------#}
{%- set base_name = grains.nodename.split('.')[0] %}
{%- set url = 'https://' ~ pillar.gitlab.runner.coordinator ~ '/ci' %}
{%- set run_dir = '/tmp/gitlab-runner/'  %}

{%- if 'token' in pillar.gitlab.runner %}
{%-   set token = '--token ' ~ pillar.gitlab.runner.token %}
{%- else %}
{%-   set token = '--registration-token ' ~ pillar.gitlab.runner.registration %}
{%- endif %}

{%- set base_tags = [] %}
{%- if 'vmx' in grains.cpu_flags or 'svm' in grains.cpu_flags %}
{%-   set hwvirt = True %}
{%- else %}
{%-   set hwvirt = False %}
{%- endif %}

{%- set base_args = ['--non-interactive', '--url ' ~ url] %}

{%- set base_env = [] %}
{%- if deploy_key %}
{#- GIT_SSH_COMMAND requires git >= 2.3 #}
{%-   do base_env.extend([
        'SSH_AUTH_SOCK=' ~ run_dir ~ 'ssh_agent.sock',
        'GIT_SSH_COMMAND=ssh -o VerifyHostKeyDNS=yes -o GlobalKnownHostsFile=' ~ run_dir ~ 'known_hosts',
      ]) %}
{%- endif %}

{#---------------- Executor loop ----------------#}
{%- for executor, data in pillar.gitlab.runner.get('executor', {'shell': {}}).items() %}
{%-   set name = base_name ~ '-' ~ executor %}

{%-   set args = [] %}
{%-   do args.extend(base_args) %}

{%-   set env = [] %}
{%-   do env.extend(base_env) %}

{%-   set tags = [executor] %}
{%-   do tags.extend(base_tags) %}
{%-   if executor == 'shell' %}
{%-     do tags.extend([grains.os_family, grains.osfinger]) %}
{%-     if hwvirt %}
{%-       do tags.append('hwvirt') %}
{%-     endif %}
{%-     if salt.cmd.which('docker') %}
{%-       do tags.append('docker-engine') %}
{%-     endif %}
{%-   elif executor == 'docker' %}
{%-     do args.extend(('--docker-image ' ~ data.get('docker-image', 'alpine'), '--docker-pull-policy always')) %}
{%-     if 'docker-privileged' in data.keys() %}
{%-       do tags.append('privileged') %}
{%-     endif %}
{%-     if deploy_key %}
{%-       do args.append('--docker-volumes "' ~ run_dir ~':/var/run/gitlab-runner:ro"') %}
{%-     endif %}
{%-   endif %}

{%-   do args.extend([token, '--name ' ~ name,
        '--tag-list "{0}"'.format(tags|join(',')),
        '--executor ' ~ executor]) %}
{%-   for var in env %}
{%-     do args.append('--env "{0}"'.format(var)) %}
{%-   endfor %}
{%-   for key, val in data.items() %}
{%-     do args.append('--' ~ key ~ ' ' ~ val) %}
{%-   endfor %}

glrun-register-{{ executor }}:
  cmd.run:
    - name: gitlab-runner --debug register {{ args|join(' ') }}
    - unless: gitlab-runner list 2>&1 | grep -q "{{ name }}"
    - require:
      - pkg: glrun-pkg
{%- endfor %}
