# https://gitlab.com/gitlab-org/gitlab-runner/blob/master/config.toml.example
{#
WIP
GL RUnner needs all of this in it's register cli args as well, hence the horror that is .register.
We can now serialize toml, but it doesn't do us any good for the time being :(
#}
{%- set name = opts.id.split('.')[0] %}
{%- set run_dir = '/var/tmp/gitlab-runner' %}
{%- set pillar_data = salt['pillar.get']('gitlab:runner') %}

{%- load_yaml as config %}
concurrent: {{ grains.num_cpus * 4 }}
runners:
- name: hanzo-shell
  executor: shell
- name: hanzo-docker
  executor: docker
  runners.docker:
    tls_verify: False
  runners.cache: {}
{%- load_yaml %}

gitlab-runner-config:
  file.serialize:
  - name: /etc/gitlab-runner/config.toml
  - formatter: toml
  - merge_if_exist: True
