{%- set rhel = salt.tmpltools.rhel() %}

{%- if rhel %}
include:
  - repo.epel
{%- endif %}

duply:
  pkg.installed: []

duply-dir:
  file.directory:
    - name: /etc/duply/{{ grains.host }}
    - dir_mode: 700
    - makedirs: True

duply-conf:
  file.managed:
    - name: /etc/duply/{{ grains.host }}/conf
    - source: salt://{{ slspath }}/conf.jinja
    - template: jinja

duply-gpg-key:
  file.managed:
    - name: /etc/duply/{{ grains.host }}/gpgkey.{{ pillar.duplicity.gpg.id }}.sec.asc
    - contents_pillar: duplicity:gpg:key
    - mode: 600

duply-gpg-pub:
  file.managed:
    - name: /etc/duply/{{ grains.host }}/gpgkey.{{ pillar.duplicity.gpg.id }}.pub.asc
    - contents_pillar: duplicity:gpg:pub
    - mode: 644
