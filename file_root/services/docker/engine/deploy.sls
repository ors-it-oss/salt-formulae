{%- from 'shared.jinja' import list_if %}
{%- from 'docker/engine/map.jinja' import docker with context %}
include:
- repo.docker
- docker.engine

docker-pkgs:
  pkg.installed:
  - refresh: True
  - pkgs:
    - docker-ce
    {{ list_if(salt.grains.get('selinux:enabled', True), 'container-selinux') }}
  - require:
    - sls: repo.docker
  - watch_in:
    - service: docker-svc

{%- if grains.init == 'systemd' %}
docker-daemon-reload:
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: docker-systemd-read-env
  - watch_in:
    - service: docker-svc

{#- Override SystemD unit file to take environment files in sane locations #}
docker-systemd-read-env:
  file.managed:
  - name: /etc/systemd/system/docker.service.d/read-env.conf
  - makedirs: True
  - contents: |
      [Service]
      EnvironmentFile=-{{ docker.prefix_file }}
      EnvironmentFile={{ docker.env_file }}
      ExecStart=
      ExecStart=/usr/bin/dockerd $DOCKER_OPTS $DELEGATED_PREFIX

{%-   if grains.systemd.version|int >= 226 %}
docker-systemd-moar-tasks:
  file.managed:
  - name:  /etc/systemd/system/docker.service.d/tasksmax.conf
  - makedirs: True
  - contents: |
      [Service]
      # Uncomment TasksMax if your systemd version supports it.
      # Only systemd 226 and above support this version.
      TasksMax=1048576
  - onchanges_in:
    - cmd: docker-daemon-reload
{%-   endif %}
{%- endif %}
