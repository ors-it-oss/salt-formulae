{%- from 'docker/engine/map.jinja' import docker with context %}
docker-dhcpv6-hook:
  file.managed:
  - name: {{ docker.hook_file }}
  - source: salt://{{ slspath }}/bin/dhcpv6-docker-hook.sh.jinja
  - template: jinja
  - makedirs: True
  - mode: 755

