{%- set auth_data = salt['pillar.get']('docker:login', False) %}
{%- if auth_data %}
{%-   for user, auths in auth_data.items() %}
docker-login-{{ user }}:
  file.serialize:
  - name: {{ salt.user.info(user)['home'] }}/.docker/config.json
  - formatter: json
  - makedirs: True
  - user: {{ user }}
  - mode: 0600
  - dataset:
      auths:
{%-     for svc, creds in auths.items() %}
        {{ svc }}:
          auth: {{ salt.hashutil.base64_b64encode(creds.user ~ ':' ~ creds.pass) }}
{%-     endfor %}
{%-   endfor %}
{%- endif %}
