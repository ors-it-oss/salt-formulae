{# https://github.com/saltstack/salt/pull/46176 #}
docker-system-prune:
  cmd.run:
  - name: docker system prune -a -f
