docker-stop:
  service.dead:
  - name: docker

docker-umount:
  cmd.run:
  - name: umount /var/lib/docker/containers/*/mounts/* || exit 0

# jeej Fluorine
#  - success_retcodes:
#    - 32
#    - 128

docker-rm:
  pkg.removed:
  - pkgs:
    - containerd
    - docker-ce
    - docker-ce-cli

docker-pip:
  pip.removed:
  - name: docker
  - reload_modules: True

/etc/yum.repos.d/docker-ce.repo:
  file.absent: []

/etc/docker:
  file.absent: []

/var/lib/docker:
  file.absent: []

/var/lib/docker-engine:
  file.absent: []

/var/lib/docker-shim:
  file.absent: []
