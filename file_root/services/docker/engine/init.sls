{%- from 'shared.jinja' import list_if %}
{%- from 'docker/engine/map.jinja' import docker with context %}
{# TODO: The SystemD reload stuff needs to go. Salt needs to handle this (https://github.com/saltstack/salt/issues/34927) #}
include:
- .net
- .login

{%-   if not salt.file.file_exists(docker.env_file) %}
{#- Install a default #}
docker-default-env:
  file.managed:
  - name: {{ docker.env_file }}
  - template: jinja
  - source: salt://{{ slspath }}/conf/docker_env.jinja
  - watch_in:
    - service: docker-svc
{%- endif %}

{%- if salt['pillar.get']('docker:config', False) %}
{#- https://github.com/docker/docker/issues/20151 #}
docker-config:
  file.serialize:
  - name: /etc/docker/daemon.json
  - formatter: json
  - makedirs: True
  - dataset_pillar: docker:config
  - watch_in:
    - service: docker-svc
{%- endif %}

docker-svc:
  service.running:
  - name: docker
  - enable: True
  - require:
    - pkg: docker-pkgs

{#- Needed for integrations; these two bite each other, one needs to go before the other can be added #}
docker-pip-depr:
  pip.removed:
  - name: docker-py

docker-pip:
  pip.installed:
  - name: docker
  - reload_modules: True
  - require:
    - pip: docker-pip-depr
