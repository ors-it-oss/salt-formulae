{## Local builds #}
{%- set builds = salt['pillar.get']('docker:builds', False) %}

docker-autobuilds:
  file.recurse:
  - name: /etc/docker/build.d/
  - source: salt://docker/builds/

{%- if builds %}
