docker-compose:
  pip.installed:
{%- if salt['pillar.get']('docker:compose:version', False) %}
  - name: docker-compose == {{ pillar.docker.compose.version }}
{%- else %}
  - name: docker-compose
{%- endif %}
  - upgrade: True
