* https://www.openssh.com/releasenotes.html
* https://cvsweb.openbsd.org/cgi-bin/cvsweb/src/usr.bin/ssh/sshd_config

## Hardening details etc.
* http://chneukirchen.org/blog/archive/2014/02/the-road-to-openssh-bliss-ed25519-and-the-identitypersist-patch.html
* http://blog.cr.yp.to/20140323-ecdsa.html
* https://stribika.github.io/2015/01/04/secure-secure-shell.html
* http://safecurves.cr.yp.to/
* https://projectbullrun.org/dual-ec/vulnerability.html
* http://ed25519.cr.yp.to/
* http://kacper.blog.redpill-linpro.com/archives/702
* https://github.com/BetterCrypto/Applied-Crypto-Hardening/blob/master/src/configuration/SSH/OpenSSH/6.0/sshd_config
