{% from 'openssh/map.jinja' import openssh with context %}

openssh-server:
  pkg.latest:
  - name: {{ openssh.pkg }}
  service.running:
  - name: {{ openssh.svc }}
  - watch:
    - pkg: {{ openssh.pkg }}
    - file: /etc/ssh/sshd_config
    - ssh_moduli: /etc/ssh/moduli

{%- if grains.init == 'upstart' %}
openssh-svc-overrides:
  file.managed:
  - name: /etc/init/{{ openssh.svc }}.override
  - source: salt://{{ slspath }}/upstart.override
  - mode: 644
  - user: root
  - group: root
{%- endif %}

openssh-strong-moduli:
  ssh_moduli.harden:
  - name: /etc/ssh/moduli
  - min_strength: 2000
  - watch:
    - pkg: {{ openssh.pkg }}

openssh-server-cfg:
  file.managed:
  - name: /etc/ssh/sshd_config
  - source: salt://{{ slspath }}/sshd_config.jinja
  - template: jinja
  - mode: 644
  - user: root
  - group: root
  - context:
    sftpserver: {{ openssh.sftpserver }}
    harden: {{ salt['pillar.get']('openssh:server:harden', True) }}
