{%- from 'map.jinja' import golang_arch -%}
{%- import_yaml 'containerd/defaults.yml' as defaults %}
{%- set containerd = salt.pillar.get('containerd', default=defaults, merge=True) %}
{%- set url = 'https://storage.googleapis.com/cri-containerd-release/cri-containerd-' ~ containerd.version ~ '.linux-' ~ golang_arch ~ '.tar.gz' %}
include:
- containerd

containerd-nopkgs:
  pkg.removed:
  - pkgs:
    - containerd
    - containerd.io
    - runc

containerd:
  pkg.installed:
  - pkgs:
    - container-selinux
    - protobuf-c
    - socat
    - libcgroup
    - libnet
    - libseccomp
  archive.extracted:
  - user: root
  - group: root
  - mode: 755
  - name: /containerd
  - options: -v --no-overwrite-dir -C / ./usr/local/*
  - source: {{ url }}
  - source_hash: {{ url }}.sha256
  - unless: test $(/usr/local/bin/containerd -v | cut -d' ' -f3) = v{{ containerd.version }}
  - watch_in:
    - service: containerd-service
  file.managed:
  - name: /etc/systemd/system/containerd.service
  - source: salt://{{ slspath }}/systemd.ini
  - template: jinja
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: containerd
  - watch_in:
    - service: containerd-service

containerd-selinux:
  cmd.run:
  - name: restorecon -RrFv /usr/local/bin /usr/local/sbin /etc/containerd {{ containerd.config.root }}
  - onchanges:
    - archive: containerd
    - file: containerd-config
    - file: containerd-config-dir
    - file: containerd-root-dir

containerd-selinux-bins:
  selinux.fcontext_policy_present:
  - name: /usr/local/(s?)bin/(runc|containerd(-.*)?)
  - sel_type: container_runtime_exec_t
  - require_in:
    - archive: containerd

containerd-config-dir:
  file.directory:
  - name: /etc/containerd
  - require_in:
    - file: containerd-config
  selinux.fcontext_policy_present:
  - name: /etc/containerd(/.*)?
  - sel_type: container_config_t

containerd-root-dir:
  file.directory:
  - name: {{ containerd.config.root }}
  - mode: 700
  - makedirs: True
  - require_in:
    - service: containerd-service
  selinux.fcontext_policy_present:
  - name: {{ containerd.config.root.rstrip('/') }}(/.*)?
  - sel_type: container_var_lib_t


{##-

/etc/docker/certs\.d(/.*)?	system_u:object_r:cert_t:s0

/etc/docker(/.*)?	system_u:object_r:container_config_t:s0

/root/\.docker	system_u:object_r:container_home_t:s0

/usr/bin/docker-novolume-plugin	--	system_u:object_r:container_auth_exec_t:s0
/usr/lib/docker/docker-novolume-plugin	--	system_u:object_r:container_auth_exec_t:s0

/usr/bin/docker.*	--	system_u:object_r:container_runtime_exec_t:s0
/usr/lib/docker/[^/]*plugin	--	system_u:object_r:container_runtime_exec_t:s0
/usr/libexec/docker/docker.*	--	system_u:object_r:container_runtime_exec_t:s0
/usr/libexec/docker/.*	--	system_u:object_r:container_runtime_exec_t:s0

/usr/lib/systemd/system/docker.*	--	system_u:object_r:container_unit_file_t:s0

/var/lib/docker/.*/config\.env	system_u:object_r:container_share_t:s0
/var/lib/docker/containers/.*/hostname	system_u:object_r:container_share_t:s0
/var/lib/docker/containers/.*/hosts	system_u:object_r:container_share_t:s0
/var/lib/docker/init(/.*)?	system_u:object_r:container_share_t:s0
/var/lib/docker/overlay2(/.*)?	system_u:object_r:container_share_t:s0
/var/lib/docker/overlay(/.*)?	system_u:object_r:container_share_t:s0

/var/lib/docker/containers/.*/.*\.log	system_u:object_r:container_log_t:s0

/var/lib/docker(/.*)?	system_u:object_r:container_var_lib_t:s0

/var/run/docker\.pid	--	system_u:object_r:container_var_run_t:s0
/var/run/docker\.sock	-s	system_u:object_r:container_var_run_t:s0
/var/run/docker(/.*)?	system_u:object_r:container_var_run_t:s0

/var/run/docker/plugins(/.*)?	system_u:object_r:container_plugin_var_run_t:s0

contexts/files/file_contexts:/usr/lib/systemd/system/containerd.*	--	system_u:object_r:container_unit_file_t:s0



[root@marty ~]# reset; grep docker /etc/selinux/targeted/contexts/files/file_contexts|sort |grep -v -e '\-latest' -e '\-current' -e '\-client'

#}
