{%- import_yaml 'containerd/defaults.yml' as defaults %}
{%- set containerd = salt.pillar.get('containerd', default=defaults, merge=True) %}

containerd-service:
  service.running:
  - name: containerd
  - enable: True
  - watch:
    - file: containerd-env
    - file: containerd-config

containerd-env:
  file.managed:
  - name: /etc/sysconfig/containerd
  - contents: |
      CONTAINERD_OPTS=
      GOMAXPROCS={{ grains.num_cpus }}
  - mode: 644

containerd-config:
  file.serialize:
  - name: /etc/containerd/config.toml
  - formatter: toml
  - dataset: {{ containerd.config }}

crictl-config:
  file.managed:
  - name: /etc/crictl.yaml
  - contents: |-
      runtime-endpoint: unix:///run/containerd/containerd.sock

# TODO: Figure this out
# * why is this 'suddenly' necessary? (Because DockerD did it by itself?)
# * Can I just set it through udev upon veth creation (don't be 'open router')
# * And/Or do I need to include cilium interfaces (not bein' helpful)
containerd-sysctl:
  sysctl.present:
  - config: /etc/sysctl.d/10-containerd.conf
  - names:
    - net.ipv4.conf.all.forwarding:
      - value: 1
    - net.ipv6.conf.all.forwarding:
      - value: 1
