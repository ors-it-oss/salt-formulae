drone-install:
  archive.extracted:
  - name: /usr/local/bin/
  - source: http://downloads.drone.io/release/linux/amd64/drone.tar.gz
  - source_hash: http://downloads.drone.io/release/linux/amd64/drone.sha256
  - enforce_toplevel: False
