{%- set path = '/srv/cilium' %}
{%- set cilium = salt['pillar.get']('cilium') %}
{%- set cert_file = path|path_join('etcd-client.crt') %}
{%- set ca_file = path|path_join('etcd-client-ca.crt') %}
{%- set key_file = path|path_join('etcd-client.key') %}
{#{%- for f in ('cilium-ds', 'cilium-rbac') %}#}
{#cilium-mf-{{ f }}:#}
{#  file.managed:#}
{#  - source: https://raw.githubusercontent.com/cilium/cilium/{{ cilium.version }}/examples/kubernetes/{{ pillar.kube.version.split('.')[0:2]|join('.') }}/{{ f }}.yaml#}
{#  - name: {{ path|path_join(f ~ '.yml') }}#}
{#  - skip_verify: True#}
{#  - makedirs: True#}
{#{%- endfor %}#}
cilium-pki-dir:
  file.directory:
  - name: {{ path }}
  - mode: 750
  - makedirs: True

{{ salt.tmpltools.pki(
      name='cilium',
      ca=pillar.etcd.ca.client,
      user='root',
      files={
        ca_file: 'ca',
        cert_file: ['pub', 'chain'],
        key_file: 'key'
    })
}}

cilium-etcd-secret:
  cmd.run:
  - name: kubectl -n kube-system create secret generic cilium-etcd --from-file={{ ca_file }} --from-file={{ cert_file }} --from-file={{ key_file }}
  - onchanges:
    - vaultx: pki-etcd-client-cilium

{#cilium-mf-overrides:#}
{#  file.recurse:#}
{#  - name: {{ path }}#}
{#  - source: salt://{{ slspath }}/mf/#}
{#  - template: jinja#}
