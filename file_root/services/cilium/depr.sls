{%- from 'shared.jinja' import root, uri %}
{%- from 'map.jinja' import paths %}

{%- set etc = root|path_join('etc', 'cilium') %}
{#{%- set var = root|path_join('var', 'cfssl') %}#}
{%- set cert_path = paths.certs|path_join('cilium') %}

{%- set args = salt['pillar.get']('cilium:args', {}) %}
{%- set config = salt['pillar.get']('cilium:config', {}) %}

{%- set kvstore = args.get('kvstore', 'etcd') %}
{%- set kvstore_opt = etc|path_join(kvstore ~ '.yml') %}
{%- if salt['pillar.get']('kube:cluster', False) %}
{%- from 'kube/lib.jinja' import kube_server with context %}
{%-   set kube_conf = etc|path_join('cilium.kubeconfig') %}
cilium-kubeconfig:
  file.managed:
  - name: {{ kube_conf }}
  - mode: 640
  - user: root
  - source: salt://kube/mf/tpl/kubeconfig.yml
  - makedirs: True
  - template: jinja
  - context:
      cert:
        pub: {{ paths.certs|path_join('kubelet', 'kube.pem') }}
        key: {{ paths.certs|path_join('kubelet', 'kube-key.pem') }}
        ca: {{ paths.certs|path_join('kubelet', 'kube-ca.pem') }}
      cluster: {{ pillar.kube.cluster }}
      server: {{ kube_server() }}
      user: cilium

{%-   do args.update({
  'k8s-kubeconfig-path': kube_conf
}) %}
{%- endif %}

cilium-kvstore-config:
  file.managed:
  - name: {{ kvstore_opt }}
  - makedirs: True
{%- if kvstore == 'etcd' %}
  - contents: |
      endpoints:
{%-   for master in pillar.etcd.endpoints.values() %}
      - {{ uri(master, 2379) }}
{%-   endfor %}
      ca-file: {{ cert_path|path_join('etcd-ca.pem') }}
      key-file: {{ cert_path|path_join('etcd-key.pem') }}
      cert-file:  {{ cert_path|path_join('etcd.pem') }}
{%- endif %}

{%- if config %}
{#- https://github.com/cilium/cilium/blob/master/daemon/config.go #}
cilium-config:
  file.serialize:
  - name: {{ etc|path_join('ciliumd.yml') }}
  - makedirs: True
  - dataset: {{ config }}
  - serializer: yaml
{%- endif %}