{%- from 'shared.jinja' import root -%}
{%- from 'lib.jinja' import globals with context -%}
{%- from 'etcd/server/flags.j2' import etc_dir, cert_dir, private_dir -%}
{%- set pki_peer = 'pki-' ~ pillar.etcd.ca.peer ~ '-' ~ pillar.etcd.name %}
{%- set pki_client = 'pki-' ~ pillar.etcd.ca.client ~ '-' ~ pillar.etcd.name %}

{#include:#}
{#- etcd.deploy#}

etcd:
{#  pkg.installed:#}
{#  - pkgs:#}
{#    - etcd#}
  file.managed:
  - name: {{ etc_dir }}/etcd.conf
  - user: etcd
  - makedirs: True
  - template: jinja
  - source: salt://{{ slspath }}/etcd.conf.j2
  service.running:
  - enable: True
  - require:
    - vaultx: {{ pki_peer }}
    - vaultx: {{ pki_client }}
  - watch:
    - file: etcd
    - vaultx: {{ pki_peer }}
    - vaultx: {{ pki_client }}

etcd-certs:
  file.directory:
  - name: {{ cert_dir }}
  - mode: 755
  - user: root
  - group: etcd
  - require_in:
    - vaultx: {{ pki_peer }}
    - vaultx: {{ pki_client }}

etcd-private:
  file.directory:
  - name: {{ private_dir }}
  - mode: 750
  - user: root
  - group: etcd
  - require_in:
    - vaultx: {{ pki_peer }}
    - vaultx: {{ pki_client }}

{{  salt.tmpltools.pki(
      name=pillar.etcd.name,
      ca=pillar.etcd.ca.peer,
      alt_names=pillar.dns.fqdn,
      ip_sans=globals.ip_sans,
      schedule_triggers={'service.reload': 'etcd'},
      user='etcd',
      files={
        root|path_join(globals.cert_dir, 'etcd-peer-ca.pem'): 'ca',
        cert_dir|path_join('etcd-peer.pem'): 'pub',
        private_dir|path_join('etcd-peer-key.pem'): 'key'
    })
}}

{{  salt.tmpltools.pki(
      name=pillar.etcd.name,
      ca=pillar.etcd.ca.client,
      alt_names=pillar.dns.fqdn,
      ip_sans=globals.ip_sans,
      schedule_triggers={'service.reload': 'etcd'},
      user='etcd',
      files={
        root|path_join(globals.cert_dir, 'etcd-client-ca.pem'): 'ca',
        cert_dir|path_join('etcd-client.pem'): ['pub', 'chain'],
        private_dir|path_join('etcd-client-key.pem'): 'key'
    })
}}
