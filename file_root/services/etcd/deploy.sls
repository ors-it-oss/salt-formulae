{%- from 'shared.jinja' import salt_header %}
{%- from 'etcd/lib.jinja' import vers, etcd_install %}
{{ etcd_install(True) }}

etcd-service-override:
  file.managed:
  - name: /etc/systemd/system/etcd.service.d/local-version.conf
  - contents: |
      {{ salt_header()|indent(6) }}
      [Service]
      ExecStart=
      ExecStart=/bin/bash -c "GOMAXPROCS=$(nproc) /usr/local/bin/etcd --name=\"${ETCD_NAME}\" --data-dir=\"${ETCD_DATA_DIR}\" --listen-client-urls=\"${ETCD_LISTEN_CLIENT_URLS}\""
      ExecReload=/bin/kill -HUP $MAINPID
  - makedirs: True
  cmd.run:
  - name: systemctl daemon-reload
  - onchanges:
    - file: etcd-service-override
