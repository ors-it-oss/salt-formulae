# -*- coding: utf-8 -*-
# vim: ft=sls

{## OLD METHOD
{% from "letsencrypt/map.jinja" import letsencrypt with context %}

letsencrypt-client-git:
  git.latest:
    - name: https://github.com/letsencrypt/letsencrypt
    - target: {{ letsencrypt.cli_install_dir }}
#}
certbot:
  pip.installed: []
