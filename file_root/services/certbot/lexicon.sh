#!/bin/sh
# Auth-hooks for Lexicon
# https://id-rsa.pub/post/certbot-auto-dns-validation-with-lexicon/
action=$1
. /etc/letsencrypt/lexicon.env

lexicon $DNS_PROVIDER --auth-api-key $DNS_APIKEY --auth-secret-key $DNS_SECRET \
    "$action" "${CERTBOT_DOMAIN}" TXT \
    --name "_acme-challenge.${CERTBOT_DOMAIN}" \
    --content "${CERTBOT_VALIDATION}" || exit 255

if [ "$action" == "create" ]; then
    sleep 10
fi