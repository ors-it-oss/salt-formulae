{%- from 'ntp/pools.jinja' import public with context %}
{%- set ntp = pillar.get('ntp', {}) %}

ntp:
  pkg.latest:
  - names:
    - ntp
    - ntpdate
    {# ntp-doc if server #}
  file.managed:
  - name: /etc/ntp.conf
  - mode: 644
  - user: root
  - group: root
  - source: salt://{{ slspath }}/ntp_conf.jinja
  - context:
      masters: False {# TODO #}
      private: {{ ntp.get('servers', False) }}
      public: {{ public }}
      peers: {{ ntp.get('peers', False) }}
  - template: jinja

{#- TODO: NTPdate before starting NTP, or @boot, or something similar #}
  service.running:
{% if grains.os_family == 'RedHat' %}
  - name: ntpd
{% elif grains.os_family == 'Debian' %}
  - name: ntp
{% endif %}
  - enable: True
  - watch:
    - file: /etc/ntp.conf
    - pkg: ntp
