# -*- coding: utf-8 -*-
'''
Operations on SSH moduli file
Contains only harden
Might one day contain generate
(if such a need will arise)
'''
from __future__ import absolute_import, print_function, unicode_literals

import salt.utils.files
import salt.exceptions
from salt._compat import StringIO

import logging
# import pprint
# pp = pprint.PrettyPrinter(indent=4)

log = logging.getLogger(__name__)


def harden(name='/etc/ssh/moduli', min_strength=None):
    '''
    Filter ou weak SSH moduli from a file
    It would actually be nicer to put this stuff in a module,
    but this simple custom state documents the code pattern for a custom state

    This state filters out moduli below the `min_strength` param from a moduli file
    (default /etc/ssh/moduli)

    moduli : /etc/ssh/moduli
        The file to adapt
    min_strength
        moduli's below this strength will be filtered out
    '''

    ret = {'name': name, 'changes': {}, 'result': False, 'comment': ''}

    try:
        min_strength = int(min_strength)
    except:
        raise salt.exceptions.SaltInvocationError(
            'Argument "min_strength" ({0}) is supposed to (become) an int.'.format(min_strength))

    if not __salt__['file.file_exists'](name):
        raise salt.exceptions.SaltInvocationError(
            'Moduli file {0} does not exist.'.format(name))

    mod_output = StringIO()
    mod_fixing = False
    mod_fixed = False

    with salt.utils.files.fopen(name, mode='r') as mod_file:
        for modulus in mod_file.readlines():
            if modulus.startswith('#') or mod_fixed:
                mod_output.write(modulus)
                continue

            mod_size = int(modulus.split()[4])
            # mod sizes are sorted, so
            # if the first one is good, they all are
            mod_weak = mod_size < min_strength
            if mod_weak:
                if not mod_fixing:
                    log.debug('{0} contains moduli that are weaker than {1}'.format(name, min_strength))
                mod_fixing = True
            elif not mod_fixing:
                comment = '{0} does not contain moduli that are weaker than {1}'.format(name, min_strength)
                ret['result'] = True
                ret['comment'] = comment
                return ret
            else:
                comment = '{0} purged of moduli weaker than {1}'.format(name, min_strength)
                ret['comment'] = comment
                # log.debug(comment)
                mod_fixed = True
                mod_output.write(modulus)

    __salt__['file.write'](name, mod_output.getvalue())

    ret['result'] = True
    return ret
