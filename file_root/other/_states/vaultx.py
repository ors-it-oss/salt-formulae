# -*- coding: utf-8 -*-
'''
Vault PowerTools
'''
from __future__ import absolute_import, print_function, unicode_literals

# Import Python libs
import itertools
import os
import OpenSSL.crypto
import re
import time

# Salt libraries
from salt._compat import ipaddress
import salt.ext.six as six

# Import 3rd-party libs
import hvac
import requests

# Logging & Debugging
import logging
# import pprint

log = logging.getLogger(__name__)
# ppr = pprint.PrettyPrinter(indent=2).pprint


def _cmd_run(cmd, stdin=None):
    return __salt__['cmd.run_all'](
        cmd,
        stdin=stdin,
        output_loglevel='quiet',
        clean_env=True,
        python_shell=True
    )


def _to_sec(ttl):
    try:
        ttl = int(ttl)
    except ValueError:
        vmod = ttl[-1]
        ttl = int(ttl[:-1])
        if vmod == 'd':
            ttl = ttl * 24 * 3600
        elif vmod == 'h':
            ttl = ttl * 3600
    return ttl


def _get_vapi(url=None, token=None):
    if not url:
        url = os.environ.get('VAULT_ADDR', False)
    elif isinstance(url, hvac.Client):
        return url

    verify = None
    if not url:
        conn = __utils__['vault.get_vault_connection']()
        url = conn['url']
        token = conn.get('token', False)
        verify = conn.get('verify', False)
    elif url.startswith('https'):
        verify = os.environ.get('VAULT_CACERT', '/etc/vault/ca.pem')

    token = os.environ.get('VAULT_TOKEN', token)
    if not token:
        token = __salt__['cp.get_file_str'](os.path.expanduser('~/.vault-token'))
        if token:
            token = token.strip()

    v_api = hvac.Client(url=url, token=token, verify=verify)
    return v_api


def _secret_file(name, spec, curr, context={}):
    if isinstance(spec, six.string_types):
        spec = [spec]

    if isinstance(spec, (list, tuple)):
        contents = []
        for i in spec:
            if i in context:
                contents.append(context[i])
            # else:
            #     contents.append(next(vault_keys))
        spec = {
            'mode': '0600',
            'contents': '\n'.join(contents)
        }
        f_ctx = None
    else:
        f_ctx = spec.pop('context', {})
        f_ctx['mode'] = f_ctx.get('mode', '0600')
        # shares = spec.pop('shares', 0)
        # if shares:
        #     f_ctx['vault_keys'] = itertools.islice(vault_keys, shares)
        f_ctx.update(context)

    f_res = __states__['file.managed'](
        name=name, show_changes=False, context=f_ctx, **spec)
    state = _state_merge(curr, f_res, 'Error saving {}'.format(name))

    return state


def _state_merge(curr, update, fail_prefix=None, last_msg=[]):
    if update['result'] is False:
        curr['result'] = False
        comm = update['comment']
        if fail_prefix:
            comm = '- {0}: {1}'.format(fail_prefix, comm)
        curr['comment'].append(comm)
        curr['comment'] = '\n'.join(curr['comment'] + last_msg)
        return False
    elif update['result'] is True:
        curr['result'] = True

    if update['changes']:
        curr['changes'][update['name']] = update['changes']

    if update['comment']:
        curr['comment'].append('- ' + update['comment'])
    return True


def _vault_merge(curr, func, msg, changes=None, **f_kwargs):
    state = {'name': func.__name__, 'changes': None, 'result': None}
    try:
        ret = func(**f_kwargs)
        log.trace('Vault {}, {}: {}'.format(
            func.__name__,
            ','.join('{}:{}'.format(k, v) for k, v in f_kwargs.items()),
            ret
        ))
    except (hvac.exceptions.VaultError, requests.exceptions.RequestException) as e:
        ret = None
        state.update({'result': False, 'comment': e})
    else:
        state.update({'result': True, 'comment': msg})
        if changes:
            state['changes'] = changes

    state = _state_merge(curr, state, fail_prefix=func.__name__)
    return state, ret


def operator_init(name, shares, threshold, files, gpg='salt'):
    '''
    when needed, call vault operator init.
    By doing it like this, we can easily prevent logging & tmpfiles of keys 'n' such

    :param name: URL
    :param shares: number of keys
    :param threshold: number of keys required to unseal
    :param gpg: list of GPG keys (one per share), use Salt's if none are given.
    :param files:
        file specs for key material
        /root/.vault-token: token_raw|token_gpg|key
        /etc/mystuff.yml:
        - shares: 2
        - template: jinja
        - contents: |
            {{ '{% for key in vault_keys %}{{ key }}{% endfor %}' }}

    :return:
    '''
    ret = {'name': 'Vault operator init', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(name)

    res, v_ret = _vault_merge(ret, vault.is_initialized, '')
    if not res:
        return ret
    elif v_ret:
        ret['comment'] = 'Vault is already initialized'
        return ret

    gpg_keys = []
    if gpg == 'salt':
        gpg_keydir = __salt__['config.get']('gpg_keydir')
        gpg_pubs = os.path.join(gpg_keydir, 'pubring.gpg')
        gpg_pubs = [gpg_pubs for _ in range(0, shares)]
    else:
        pass  # TODO
    for gpg_pub in gpg_pubs:
        res = _cmd_run('gpg --dearmor -o - {} | base64'.format(gpg_pub))
        gpg_keys.append(res['stdout'])

    res, v_ret = _vault_merge(
        ret, vault.initialize,
        'A new Vault was initialized', changes='initialized',
        secret_shares=shares, secret_threshold=threshold, pgp_keys=gpg_keys
    )
    if not res:
        return ret
    vault_keys = v_ret['keys_base64']
    token_raw = v_ret['root_token']
    del v_ret

    res = _cmd_run('gpg --with-colons --import-options show-only --import {0}'.format(gpg_pubs[0]))
    gpg_name = None
    for l in res['stdout'].splitlines():
        if l.startswith('uid:'):
            gpg_name = l.split(':')[9]
    res = _cmd_run(
        'gpg --no-default-keyring --keyring "{0}" --recipient "{1}" --trust-model always --armor --encrypt'.format(
            gpg_pubs[0], gpg_name
        ),
        stdin=token_raw
    )
    token_gpg = res['stdout']
    ret['changes']['vault_root_token'] = token_gpg

    context = {
        'token_gpg': token_gpg,
        'token_raw': token_raw
    }
    del token_gpg
    del token_raw
    vault_keys = iter(vault_keys)
    for f_path, f_data in files.items():
        f_context = {}
        if 'key' in f_data:
            context['key'] = next(vault_keys)
        else:
            try:
                context['vault_keys'] = itertools.islice(vault_keys, f_data.pop('shares'))
            except (AttributeError, KeyError):
                pass
        f_context.update(context)
        res = _secret_file(f_path, f_data, ret, f_context)
        if not res:
            return ret
    del context

    ret['changes']['vault_keys'] = list(vault_keys)
    ret['comment'] = '\n'.join(ret['comment'])
    ret['result'] = True
    return ret


def unsealed(name, keys, gpg='salt'):
    '''
    auto-unseal Vault
    :param name:
    :param keys:
    :param gpg:
    :return:
    '''

    ret = {'name': 'Vault unsealing', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(name)

    res, v_ret = _vault_merge(ret, vault.is_sealed, '')
    if not res:
        return ret
    elif not v_ret:
        return ret

    if gpg == 'salt':
        gpg_keydir = __salt__['config.get']('gpg_keydir')

    gpg_cmd = 'base64 --decode {0} | gpg --homedir={1} --decrypt'
    unseal_keys = []
    for key in keys:
        if os.path.exists(key):
            cmd = gpg_cmd.format(key, gpg_keydir)
            stdin = None
        else:
            cmd = gpg_cmd.format('', gpg_keydir)
            stdin = key
        res = _cmd_run(cmd, stdin=stdin)
        unseal_keys.append(res['stdout'])

    res, _ = _vault_merge(
        ret, vault.unseal_multi,
        'The Vault is open', changes='unsealed',
        keys=unseal_keys
    )
    del unseal_keys
    if res:
        ret['comment'] = '\n'.join(ret['comment'])
    return ret


def token(name, files, url=None, **token_details):
    '''
    :param name:
    :param url:
    :param token_details:
    :return:
    '''
    ret = {'name': 'Make sure a token exists', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url)

    token = None
    accessor = None
    for f_path, f_data in files.items():
        if isinstance(f_data, dict):
            continue
        elif isinstance(f_data, six.string_types):
            f_data = [f_data]

        t_str = __salt__['cp.get_file_str'](f_path)
        if not t_str:
            continue

        for i, l in enumerate(t_str.splitlines()):
            if f_data[i] == 'accessor':
                accessor = l.strip()
                break
            elif f_data[i] == 'token' and not token:
                token = l.strip()
        else:
            continue
        break

    if accessor or token:
        if accessor:
            token = None
        res, v_ret = _vault_merge(ret, vault.lookup_token, '', accessor=accessor, token=token)
        if not res:
            return ret
        elif v_ret:
            ret['comment'] = 'Token exists'
            return ret

    if 'display_name' not in token_details:
        token_details['display_name'] = name

    res, v_ret = _vault_merge(
        ret, vault.create_token,
        'Token created for {}'.format(name), changes='new token created',
        **token_details)
    # ppr(v_ret)
    if not v_ret:
        return ret

    token = v_ret['auth']['client_token']
    accessor = v_ret['auth']['accessor']
    context = {
        'token': token,
        'accessor': accessor
    }
    for f_path, f_data in files.items():
        res = _secret_file(f_path, f_data, ret, context)
        if not res:
            return ret

    ret['comment'] = '\n'.join(ret['comment'])
    return ret


def auth_backend(name, backend_type=None, description=None, url=None, **tunes):
    '''

    nla:
      vault.auth_backend:
      - name: approle

    bla:
        vault.auth_backend:
        - name: puppies
        - type: approle

    :param name:
    :param type:
    :param description:
    :param url:
    :param tunes:
    :return:
    '''

    ret = {'name': 'Auth backend', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url)
    backend_type = backend_type or name
    mount_point = name if name != backend_type else None

    for ttl in ('default_lease_ttl', 'max_lease_ttl'):
        if ttl in tunes:
            tunes[ttl] = _to_sec(tunes[ttl])

    if name + '/' in vault.sys.list_auth_methods():
        ret['comment'].append('- {0} auth already enabled'.format(name))
    else:
        res, v_ret = _vault_merge(
            ret, vault.sys.enable_auth_method,
            '{} auth backend enabled'.format(name), changes='{} auth enabled'.format(name),
            backend_type=backend_type,
            description=description,
            mount_point=mount_point,
        )
        if not res:
            return ret

    res, v_ret = _vault_merge(
        ret, vault.get_auth_backend_tuning, '',
        backend_type=backend_type,
        mount_point=mount_point
    )
    if not res:
        return ret
    v_data = v_ret['data']

    try:
        for tune, tune_val in tunes.items():
            assert v_data[tune] == tune_val
    except (AssertionError, TypeError, KeyError):
        res, v_ret = _vault_merge(
            ret, vault.tune_auth_backend,
            'Changed tunes of {} auth backend'.format(name), changes='{} auth tuned'.format(name),
            backend_type=backend_type,
            mount_point=mount_point,
            description=description,
            **tunes
        )
        if not res:
            return ret
    else:
        ret['comment'].append('- {0} auth already tuned correctly'.format(name))

    ret['comment'] = '\n'.join(ret['comment'])
    return ret


def policy(name, url=None, rules=None, source=None, context=None, defaults=None, template='jinja', saltenv=None):
    '''
    my_awesome_policy:
        vaultx.policy:
        - name: awesome_policy
        - rules: |
            path "pki/{{ca}}/ca" {
              capabilities = ["read"]
            }
        - source:  # Mutually exclusive w/rules
          - vault/policies/ca.hcl
        - context:
            ca: myCa

    :param name:
    :param url:
    :param rules:
    :param source:
    :param context:
    :param defaults:
    :param template:
    :param saltenv:
    :return:
    '''

    ret = {'name': 'Policy {}'.format(name), 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url)

    try:
        assert rules is not None or source is not None
    except AssertionError:
        ret['result'] = False
        ret['comment'] = 'Specify either rules or source'
        return ret

    rule_kwargs = {
        'template': template,
        'context': context or {},
        'defaults': defaults or {},
        'saltenv': saltenv,
    }
    contents = []
    if rules:
        contents = [rules]
    elif source:
        if not isinstance(source, (list, tuple)):
            source = [source]
        for source in source:
            contents.append(__salt__['cp.get_file_str']('salt://' + source))

    contents = '\n'.join(
        __salt__['file.apply_template_on_contents'](contents=content, **rule_kwargs) for content in contents
    )

    res, v_ret = _vault_merge(ret, vault.sys.read_policy, '', name=name)
    if not res:
        return ret
    elif v_ret and v_ret.get('rules', v_ret.get('data', {}).get('rules', False)) == contents:
        ret['comment'] = 'Policy {} present & up to date'.format(name)
        return ret

    res, v_ret = _vault_merge(
        ret, vault.sys.create_or_update_policy,
        'Write policy {}'.format(name), changes='Policy written',
        name=name, policy=contents
    )
    # ppr(res)
    # ppr(v_ret)
    if not res:
        return ret

    ret['comment'] = '\n'.join(ret['comment'])
    return ret


def ca(name,
       common_name,
       url=None,
       token=None,
       default_ttl='90d',
       max_ttl='3653d',
       ttl='3653d',
       key_type='ec',
       key_bits=None,
       pubfile=None,
       roles=None,
       parent=None,
       **kwargs):
    '''
    https://www.vaultproject.io/api/secret/pki/index.html#generate-root

    :param name: Name of CA
    :param url: URL of Vault project
    :param common_name: CN of CA
    :param default_ttl: default TTL of certificates
    :param ttl: TTL of CA certificate
    :param max_ttl: Maximum TTL CA can give out
    :param key_type: ec|rsa
    :param key_bits:
    :param pubfile:
    :param roles:
    :param kwargs:
    :return:
    '''
    ret = {'name': 'Vault CA', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url, token)
    path = 'pki/{}'.format(name)

    default_ttl = _to_sec(default_ttl)
    max_ttl = _to_sec(max_ttl)

    res, v_ret = _vault_merge(ret, vault.sys.list_mounted_secrets_engines, '')
    if not res:
        return ret
    elif path + '/' in v_ret:
        ret['comment'].append('- PKI engine already mounted on {}'.format(path))
    else:
        res, _ = _vault_merge(
            ret, vault.sys.enable_secrets_engine,
            'PKI backend enabled', changes='backend mounted',
            backend_type='pki',
            description=common_name,
            mount_point=path,
            config={
                'default_lease_ttl': default_ttl,
                'max_lease_ttl': max_ttl,
            }
        )
        if not res:
            return ret

    res, v_ret = _vault_merge(
        ret, vault.read, '',
        path='{}/cert/ca'.format(path)
    )
    if not res:
        return ret

    if v_ret:
        ca_pub = v_ret['data']['certificate']
        ret['comment'].append('- CA certificate already present')
    else:
        if not key_bits:
            if key_type == 'rsa':
                key_bits = 8192
            elif key_type == 'ec':
                key_bits = 521

        data = {
            'common_name': common_name,
            'key_type': key_type,
            'key_bits': key_bits,
            'ttl': _to_sec(ttl),
        }
        data.update(kwargs)

        if parent:
            ca_path = '{}/intermediate'.format(path)
        else:
            ca_path = '{}/root'.format(path)

        res, v_ret = _vault_merge(
            ret, vault.write,
            '{} bit {} CA cert generated'.format(key_bits, key_type),
            changes='generated {}{}'.format(key_type, key_bits),
            path=ca_path + '/generate/internal',
            **data
        )
        if not res:
            return ret
        # ppr(v_ret)

        if parent:
            res, v_ret = _vault_merge(
                ret, vault.write,
                'Signing CSR {}'.format(name),
                changes='signed {}{} intermediate CA'.format(key_type, key_bits),
                path='pki/{}/root/sign-intermediate'.format(parent),
                csr=v_ret['data']['csr'],
                **data
            )
            if not res:
                return ret
            # ppr(v_ret)

            cert = [v_ret['data']['certificate']]
            if 'ca_chain' in v_ret['data']:
                cert.extend(v_ret['data']['ca_chain'])
            else:
                cert.append(v_ret['data']['issuing_ca'])

            res, _ = _vault_merge(
                ret, vault.write,
                'Activated signed CA {}'.format(name),
                changes='set signed {}{} certificate for CA'.format(key_type, key_bits),
                path=ca_path + '/set-signed',
                certificate='\n'.join(cert)
            )
            if not res:
                return ret

        ca_pub = v_ret['data']['certificate']

    if pubfile:
        tres = __states__['file.managed'](
            name=pubfile,
            contents=ca_pub, show_changes=False, makedirs=True)
        state = _state_merge(ret, tres, 'Error writing CA pub')
        if not state:
            return ret

    if roles:
        for role, roledata in roles.items():
            for k in roledata.keys():
                if 'ttl' in k:
                    roledata[k] = _to_sec(roledata[k])
            r_path = '{}/roles/{}'.format(path, role)
            res, v_ret = _vault_merge(
                ret, vault.read, '',
                path=r_path
            )
            if not res:
                return ret
            try:
                current = v_ret['data']
                for k, v in roledata.items():
                    if current[k] not in (v, [v]):
                        raise ValueError
                ret['comment'].append('- Role {} is OK'.format(role))
            except (TypeError, KeyError, ValueError) as e:
                res, _ = _vault_merge(
                    ret, vault.write,
                    'Role {} written'.format(role), changes='role {} written'.format(role),
                    path=r_path,
                    **roledata
                )
                if not res:
                    return ret

    ret['comment'] = '\n'.join(ret['comment'])
    return ret


def cert(name,
         ca, role,
         files, owner='root', url=None, token=None,
         renew_by='7d', triggers=None, schedule=True, schedule_triggers=None,
         **kwargs):
    '''
    https://www.vaultproject.io/api/secret/pki/index.html#generate-certificate

    pki-vault-www.example.com:
      vaultx.cert:
      - name: www.example.com
      - ca: my-ca
      - role: tls
      - files:
          /etc/mystuff/ca.pem: chain

    files:
      /etc/mystuff/full.pem: [ key, pub, ca ]
      /etc/mystuff/chain.pem: [ pub, ca ]
      /etc/mystuff/ca.pem: ca
      /etc/mystuff/cert.pem: pub
      /etc/mystuff/cert.key: key

    :param name:
    :param ca:
    :param role:
    :param url:
    :param files:
    :param owner:
    :param renew_by:
    :param triggers:
    :param schedule:
    :param kwargs:
    :return:
    '''
    LIST_KWARGS = ('alt_names', 'ip_sans', 'uri_sans')
    for kwarg in kwargs.keys():
        if kwarg.startswith('__'):
            del kwargs[kwarg]

    def _sancast(*sANs):
        """because '2001:bc8:4700:2300::26:217' != '2001:BC8:4700:2300:0:0:26:217\n'"""
        psANs = set()
        for kwarg in sANs:
            if not isinstance(kwarg, (list, tuple)):
                kwarg = [kwarg]
            for sAN in kwarg:
                try:
                    sAN = ipaddress.ip_address(sAN.strip())
                except ValueError:
                    pass
                psANs.add(sAN)
        return psANs

    ret = {'name': 'Vault Cert', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url, token)
    path = 'pki/{}'.format(ca)

    if renew_by:
        renew_byd = _to_sec(renew_by)

    data = {
        'common_name': name,
    }
    data.update(kwargs)

    for kwarg in LIST_KWARGS:
        try:
            kval = data[kwarg]
            if not isinstance(kval, (list, tuple)):
                data[kwarg] = [kval]
        except KeyError:
            continue

    # If you specified alt names
    if 'alt_names' in kwargs or '.' not in name or not re.match(
            r'^(?=.{2,253}\.?$)(?!.*\.[0-9]+$)(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\.|$))+',
            name):
        data['exclude_cn_from_sans'] = True

    # I think Vault does this already
    # if re.match(
    #         r'^(?=.{2,253}\.?$)(?!.*\.[0-9]+$)(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\.|$))+',
    #         name):
    #     alt_names = kwargs.get('alt_names', [])
    #     if name not in alt_names:
    #         alt_names.append(name)
    #         kwargs['alt_names'] = alt_names

    cert_info = None
    cert_work = None
    for _fp in files.keys():
        try:
            _fp_cert = __salt__['tls.cert_info'](_fp)
            if _fp_cert['subject']['CN'] == name:
                cert_info = _fp_cert
                break
        except (FileNotFoundError, OpenSSL.crypto.Error, KeyError):
            continue

    if not cert_info:
        cert_work = '{role} certificate issued to {name}'
    elif renew_by and cert_info['not_after'] <= time.time() + renew_byd:
        cert_work = '{role} certificate for {name} renewed'
    elif cert_info['extensions']['authorityKeyIdentifier'].split(':', 1)[1] \
            != __salt__['tls.cert_info'](
                vault.read(path + '/cert/ca')['data']['certificate'])['extensions']['subjectKeyIdentifier']:
        cert_work = 'new {role} certificate issued to {name}, different CA'
    else:
        san_req = _sancast(name, kwargs.get('alt_names', []), kwargs.get('ip_sans', []))
        san_cur = _sancast(cert_info['subject']['CN'], cert_info.get('subject_alt_names', []))
        log.debug('Comparing sANs: {} VS {}'.format(san_req, san_cur))
        if san_req != san_cur:
            cert_work = 'new {role} certificate issued to {name}, changed subjectAltNames'

    if not cert_work:
        ret['comment'].append('- Cert OK')
    else:
        # yugh... hashicorp strikes again
        for kwarg in LIST_KWARGS:
            try:
                data[kwarg] = ','.join(data[kwarg])
            except KeyError:
                continue

        res, v_ret = _vault_merge(
            ret, vault.write,
            cert_work.format(role=role, name=name), changes='cert issued',
            path=path + '/issue/{}'.format(role),
            **data
        )
        if not res:
            return ret

        cert = v_ret['data']
        # cert_info = __salt__['tls.cert_info'](cert['certificate'])
        for fpath, comps in files.items():
            mode = '0644'
            contents = []
            if not isinstance(comps, (list, tuple)):
                comps = [comps]
            for comp in comps:
                if comp == 'key':
                    mode = '0600'
                compres = {
                    'chain': cert.get('ca_chain', cert['issuing_ca']),
                    'root': cert.get('ca_chain', [cert['issuing_ca']])[-1],
                    'int': cert.get('ca_chain', [None])[:-1],
                    'ca': cert['issuing_ca'],
                    'pub': cert['certificate'],
                    'key': cert['private_key']
                }[comp]
                if isinstance(compres, (list, tuple)):
                    contents.extend(compres)
                elif compres:
                    contents.append(compres)
            fres = __states__['file.managed'](
                fpath, contents='\n'.join(contents), mode=mode, user=owner, show_changes=False
            )
            state = _state_merge(ret, fres, 'Error writing {}'.format(fpath))
            if not state:
                return ret
            __salt__['event.send']('vault/' + name, data={'comment': 'Certificate updated'})

    if triggers:
        for t_name, t_params in triggers.items():
            if isinstance(t_params, dict):
                t_res = __salt__[t_name](**t_params)
            elif isinstance(t_params, (list, tuple)):
                t_res = __salt__[t_name](*t_params)
            else:
                t_res = __salt__[t_name](t_params)

            if t_res:
                ret['comment'].append('- {}: {}'.format(t_name, t_res))

    if schedule:
        # Schedule automated renewal
        job_kwargs = {
           'ca': ca,
           'role': role,
           'files': files,
           'owner': owner,
           'renew_by': renew_by,
           'schedule': schedule,
           'triggers': schedule_triggers,
           'schedule_triggers': schedule_triggers
        }
        job_kwargs.update(kwargs.pop('kwargs', kwargs))

        sched_res = __states__['schedule.present'](
            name='vault-cert-{0}-{1}'.format(ca, name),
            days=1,
            splay=43200,
            jid_include=True,
            function='state.single',
            job_args=['vaultx.cert', name],
            job_kwargs=job_kwargs,
            queue=True
        )
        # ppr(sched_res)
        state = _state_merge(ret, sched_res, 'Error writing schedule')
        if not state:
            return ret

    ret['comment'] = '\n'.join(ret['comment'])
    ret['result'] = True
    return ret


def pub(name, files, serial=None, ca=None, owner='root', url=None, token=None, **kwargs):
    '''
    Fetch the pub
    :param name: CA name or serial (s)
    :param ca: CA if requesting a pub by serial
    :param files:
    :param owner:
    :param url:
    :param token:
    :param kwargs:
    :return:
    '''
    ret = {'name': 'Vault Public Cert', 'changes': {}, 'result': None, 'comment': []}
    vault = _get_vapi(url, token)

    if name and ca is None:
        ca = name
        name = None

    if not isinstance(ca, (list, tuple)):
        ca = [ca]

    ca_chains = []
    for _ca in ca:
        path = 'pki/{}/cert'.format(_ca)

        res, v_ret = _vault_merge(
            ret, vault.read, '',
            path='{}/ca_chain'.format(path),
            **kwargs
        )
        if not res:
            return ret
        elif v_ret is None:
            ret['result'] = False
            ret['comment'] = 'No return for {}, invalid CA?'.format(_ca)
            return ret
        ca_chain = v_ret['data']['certificate']

        # If there is a chain, the CA is in there. If there isn't, it remains empty
        if ca_chain:
            delim = '-----BEGIN '
            ca_chain = [delim + cert.strip() for cert in ca_chain.strip().split(delim) if cert]
        else:
            res, v_ret = _vault_merge(
                ret, vault.read, '',
                path='{}/ca'.format(path),
                **kwargs
            )
            if not res:
                return ret
            ca_chain = [v_ret['data']['certificate']]

        ca_chains.append(ca_chain)

        cert_pub = None
        if serial:
            res, v_ret = _vault_merge(
                ret, vault.read, '',
                path='{}/{}'.format(path, serial),
                **kwargs
            )
            if not res:
                return ret
            cert_pub = v_ret['data']['certificate']

    for fpath, comps in files.items():
        mode = '0644'
        contents = []
        if not isinstance(comps, (list, tuple)):
            comps = [comps]
        for comp in comps:
            for ca_chain in ca_chains:
                compres = {
                    'chain': ca_chain if len(ca_chain) > 1 else None,
                    'root': ca_chain[-1] if len(ca_chain) > 1 else None,
                    'int': ca_chain[:-1] if len(ca_chain) > 2 else None,
                    'ca': ca_chain[0],
                    'pub': cert_pub,
                }[comp]
                if isinstance(compres, (list, tuple)):
                    contents.extend(compres)
                elif compres:
                    contents.append(compres)
        if not contents:
            ret['comment'].append('- Nothing in {}, skipped'.format(fpath))
        else:
            fres = __states__['file.managed'](
                fpath, contents='\n'.join(contents), mode=mode, user=owner, show_changes=False
            )
            state = _state_merge(ret, fres, 'Error writing {}'.format(fpath))
            if not state:
                return ret

    ret['comment'] = '\n'.join(ret['comment'])
    ret['result'] = True
    return ret
