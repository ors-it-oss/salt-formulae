# -*- coding: utf-8 -*-
'''
Retrieve Disk information
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os
import re

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.platform
import salt.modules.udev

has_udev = salt.modules.udev.__virtual__()
__salt__ = {
    'udev.info': salt.modules.udev.info,
    'udev.env': salt.modules.udev.env
}

# Logging & Debugging
import logging
# import pprint
# ppr = pprint.PrettyPrinter(indent=2).pprint
log = logging.getLogger(__name__)


def __virtual__():
    return salt.utils.platform.is_linux()


def _sysval(prop):
    '''
    Simply read a single lined file
    '''
    return open(prop, 'r').read().strip()


def _disk_info(dev):
    '''
    Fetch & return disk info
    '''
    devdir = '/sys/class/block/' + dev
    # rota = bool(int(__sysval(devdir + '/queue/rotational')))

    b_size = int(_sysval(devdir + '/queue/physical_block_size'))
    size = 512 * int(_sysval(devdir + '/size'))

    props = {
        'size': size,
        'block_size': b_size,
    }

    scheduler = re.search(r'\[([a-z]+)\]', _sysval(devdir + '/queue/scheduler'))
    if scheduler:
        props['scheduler'] = scheduler.group(1)

    if os.path.exists(devdir + '/bcache/dev'):
        props['bcache'] = os.path.basename(os.readlink(devdir + '/bcache/dev'))
    elif os.path.exists(devdir + '/bcache/set'):
        props['bcache'] = os.path.basename(os.readlink(devdir + '/bcache/set'))

    if not has_udev:
        return props

    log.debug('Parsing udev for ' + dev)
    # udev = __udev_info('/dev/' + dev)
    # udev = __salt__['udev.info']('/dev/' + dev)
    udev_env = __salt__['udev.env']('/dev/' + dev)

    rpm = udev_env.get('ID_ATA_ROTATION_RATE_RPM', udev_env.get('ID_SCSI_MRR', False))
    if rpm:
        props['rpm'] = rpm

    serial = udev_env.get('ID_SCSI_SERIAL', udev_env.get('ID_SERIAL_SHORT', None))
    if serial:
        props['serial'] = serial

    for prop in ('MODEL', 'WWN', 'REVISION', 'PATH',
                 'LOC_PORT', 'LOC_SLOT', 'LOC_ENCLOSURE',
                 'FS_UUID', 'FS_TYPE', 'FS_LABEL'):
        udev_prop = 'ID_' + prop
        prop = prop.replace('LOC_', '').lower()
        if udev_prop in udev_env:
            props[prop.lower()] = udev_env[udev_prop]

    return props


def _part_info(part):
    '''
    Fetch and return partition info
    '''
    pdir = '/sys/class/block/' + part

    props = {
        'size': int(_sysval(pdir + '/size'))
    }

    if not has_udev:
        return props

    udev_env = __salt__['udev.env']('/dev/' + part)
    for prop in ('FS_UUID', 'FS_TYPE', 'FS_LABEL', 'PART_ENTRY_TYPE', 'PART_ENTRY_UUID'):
        udev_prop = 'ID_' + prop
        if udev_prop in udev_env:
            props[prop.lower()] = udev_env[udev_prop]
    return props


def all_disks():
    '''
    Gather all disk info
    '''
    disks = ({}, {})
    parts = {}
    for dev in next(os.walk('/sys/class/block'))[1]:
        devdir = '/sys/class/block/' + dev

        # Take only [vs]d[a-z] full drives, no virtual (VirtIO != virtual!) and no removable ones
        if not re.match(r'[hsv]d[a-z]+$', dev) \
                or '/virtual/' in os.readlink(devdir) \
                or int(_sysval(devdir + '/removable')) != 0:
            continue

        rota = bool(int(_sysval(devdir + '/queue/rotational')))
        info = _disk_info(dev)

        disks[rota][dev] = info
        parts.update(_all_parts(dev))

    grains = {}
    if len(disks[0]):
        grains['disks_solid'] = disks[0]

    if len(disks[1]):
        grains['disks_magnetic'] = disks[1]

    if len(parts):
        grains['disk_partitions'] = parts

    return grains


def _all_parts(disk):
    '''
    All partition info
    '''
    parts = {}
    for part in next(os.walk('/sys/class/block/' + disk))[1]:
        if not part.startswith(disk):
            continue

        # parts[int(re.split('([0-9]+)',part)[1])] = __part_info(part)
        parts[part] = _part_info(part)

        # pp.pprint(parts)
    return parts
