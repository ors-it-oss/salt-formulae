# -*- coding: utf-8 -*-
'''
Collect Secure SHell Grains of Salt.
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import re

# Salt - grains need modules imported as well because they're loaded before them
import salt.modules.cmdmod
import salt.modules.ssh
__salt__ = {
    'cmd.run': salt.modules.cmdmod._run_quiet,
    'ssh.host_keys': salt.modules.ssh.host_keys,
    'ssh._fingerprint': salt.modules.ssh._fingerprint,
}

# Logging & Debugging
import logging
log = logging.getLogger(__name__)

SSH = salt.utils.path.which_bin(['ssh'])


def __virtual__():
    '''
    Only work when ssh is installed.
    '''
    return SSH is not None


def get_ssh_grains():
    '''
    Get SSH grains (version & public key info)
    '''

    ssh_ver = __salt__['cmd.run']('{0} -V'.format(SSH))
    ssh_ver = re.match(r'[A-z0-9_-]+SSH_([^, ]+)[, ]+.*', ssh_ver).group(1).split('.')
    ssh_pver = re.match(r'([^p]+)(p.*)*', ssh_ver.pop()).groups()
    ssh_ver = [int(v) for v in ssh_ver]
    ssh_ver.append(int(ssh_pver[0]))
    if ssh_pver[1] is not None:
        ssh_ver.append(ssh_pver[1])

    ssh_keys = {}
    # TODO: magic juju for keydir
    for key, val in __salt__['ssh.host_keys'](keydir='/etc/ssh', private=False).items():
        if not key.endswith('.pub'):
            continue
        ktype, pub = val.split(' ')[0:2]

        finger = __salt__['ssh._fingerprint'](pub, fingerprint_hash_type='sha256')
        ssh_keys[ktype] = {'pub': pub, 'fp': finger}

    return {'ssh': {'version': ssh_ver, 'keys': ssh_keys}}
