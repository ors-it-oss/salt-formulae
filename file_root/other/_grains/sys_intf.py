# -*- coding: utf-8 -*-
'''
Determine if this is an EFI or BIOS system
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os

# Salt
import salt.utils.platform

# Logging & Debugging
import logging
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work on Linux for now
    '''
    return salt.utils.platform.is_linux()


def sys_interface():
    '''
    Return which firmware interface the system is running under
    '''
    sys_intf = False

    for sysi in ('efi', 'acpi', 'devicetree'):
        if os.path.exists(os.path.join('/sys/firmware/', sysi)):
            sys_intf = sysi
            break

    if sys_intf:
        return {'sys_interface': sys_intf}
    else:
        log.debug('No sys_interface grain generated, in a container?')
        return
