# -*- coding: utf-8 -*-
'''
DMI device type 38 == IPMI
That makes 'ipmi' in grains True.
If ipmi-config or bmc-config are executable in path,
grain contains MAC and IPv4 address
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import re

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.path
import salt.modules.cmdmod
import salt.modules.smbios
__salt__ = {
    'cmd.run': salt.modules.cmdmod._run_quiet,
    'smbios.get': salt.modules.smbios.get,
    'smbios.records': salt.modules.smbios.records,
}

# Logging & Debugging
import logging
log = logging.getLogger(__name__)

DMIDECODE = salt.utils.path.which_bin(['dmidecode'])
#log.debug('IPMI DMIdecode: {0}'.format(DMIDECODE))
IPMICONFIG = salt.utils.path.which_bin(['bmc-config', 'ipmi-config'])
#log.debug('IPMI config reader bin: {0}'.format(IPMICONFIG))


def __virtual__():
    '''
    Only work when dmidecode is installed.
    '''
    return DMIDECODE is not None


def get_ipmi():
    '''
    Add IPMI MAC & IP to grains lists/interfaces
    '''

    dmi_out = __salt__['smbios.records'](38)
    #dmi_out = __salt__['cmd.run']('{0} -t 38'.format(DMIDECODE))
    #log.trace('IPMI DMI check: {0}'.format(dmi_out))

    if dmi_out is None or not len(dmi_out):
        return
    elif IPMICONFIG is None:
        log.debug('IPMI found, missing ipmi-config')
        return {'ipmi': True}

    log.trace('IPMI config found: {0}'.format(IPMICONFIG))

    grains = {}
    lanconf = __salt__['cmd.run']('{0} -S Lan_Conf --checkout'.format(IPMICONFIG))
    # log.debug('Parsing through: {0}'.format(lanconf))
    for conf in lanconf.splitlines():
        conf = re.split(r'\s+', conf.strip())
        # log.debug(conf)
        if conf[0] == 'IP_Address':
            log.debug('IPMI: Found IP {0}'.format(conf[1]))
            grains['inet'] = conf[1]
        elif conf[0] == 'MAC_Address':
            log.debug('IPMI: Found MAC {0}'.format(conf[1]))
            grains['hwaddr'] = conf[1].lower()

    if len(grains):
        return {'ipmi': grains}
