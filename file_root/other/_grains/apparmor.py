# -*- coding: utf-8 -*-
'''
Collect AppArmor Grains of Salt.
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os
import re
import collections

# Salt
import salt.utils.files

# Logging & Debugging
import logging
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work when AppArmor is actually started/running.
    '''
    # return os.path.exists('/sys/kernel/security/apparmor')
    return os.path.exists('/sys/module/apparmor')


def get_apparmor_grains():
    '''
    Get AppArmor grains
    '''

    grains = {'apparmor': {'enabled': False}}

    with salt.utils.files.fopen('/proc/self/mountinfo') as mounts:
        mount = [line.split()[4] for line in mounts if 'securityfs' in line]

    if mount is not None and len(mount) > 0:
        mount = mount[0]
    else:
        log.debug('SecurityFS appears not mounted, AppArmor not there')
        return grains

    apparmor_profiles = os.path.join(mount, 'apparmor', 'profiles')

    if not os.path.exists(apparmor_profiles):
        return grains

    profiles = collections.defaultdict(list)
    with salt.utils.files.fopen(apparmor_profiles) as apparmor_profiles:
        for profile in apparmor_profiles:
            # ripped from aa-status.get_profiles()
            match = re.search(r'^([^\(]+)\s+\((\w+)\)$', profile)
            profiles[match.group(2)].append(match.group(1))

    if not len(profiles):
        return grains

    profiles = dict([(key, sorted(values)) for key, values in profiles.items()])
    grains['apparmor'] = dict((('enabled', True), ('profiles', profiles)))

    return grains
