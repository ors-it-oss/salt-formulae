# -*- coding: utf-8 -*-
'''
Retrieve Ethernet link speeds
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.platform

# Logging & Debugging
import logging

# Salt - grains need modules imported as well because they're loaded before them
import salt.modules.cmdmod
__salt__ = {
    'cmd.run': salt.modules.cmdmod.run
}
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only works on Linux. 4 now.
    '''
    return salt.utils.platform.is_linux()


def get_ifspeeds():
    '''
    Retrieve interface speeds
    '''
    grains = {}
    for intf in next(os.walk('/sys/class/net'))[1]:
        ifdir = '/sys/class/net/' + intf

        try:
            if '/virtual/' in os.readlink(ifdir) or int(open(ifdir + '/carrier', 'r').read().strip()) == 0:
                continue
            grains[intf] = int(open('/sys/class/net/' + intf + '/speed', 'r').read().strip())
        except IOError:
            continue

    if len(grains):
        return {'hwspeed_interfaces': grains}


def dhcp_server():
    '''
    DHCP server
    :return:
    '''
    dhcp = None
    intf = __salt__['cmd.run']("ip route | awk '/default/{ print $5 }'", python_shell=True, output_loglevel='quiet')
    if salt.utils.path.which('netplan'):
        data = __salt__['cmd.run']('netplan ip leases {}'.format(intf), output_loglevel='quiet')
        for l in data.splitlines():
            if l.startswith('SERVER_ADDRESS'):
                dhcp = l.split('=')[-1].strip()
                break
    elif salt.utils.path.which('nmcli'):
        data = __salt__['cmd.run']('nmcli -f DHCP4 d sh {}'.format(intf), output_loglevel='quiet')
        if data:
            for l in data.splitlines():
                if 'dhcp_server_identifier' in l:
                    dhcp = l.split('=')[-1].strip()
                    break

    if dhcp:
        return {'ip4_dhcp': dhcp}


# def public_as():
#
#     url = 'https://stat.ripe.net/data/routing-status/data.json?resource=185.95.31.151'