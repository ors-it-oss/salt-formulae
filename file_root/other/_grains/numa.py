# -*- coding: utf-8 -*-
'''
Do stuff 
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os
import re

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.platform

# Logging & Debugging
import logging
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work when 
    '''
    return salt.utils.platform.is_linux()


def nodes():
    '''
    NUMA nodes in a system
    '''
    try:
        nodecount = len([node for node in os.listdir('/sys/devices/system/node') if re.match(r'^node[0-9]+$', node)])
        if nodecount > 1:
            return {'numa_nodes': nodecount}
    except OSError:
        pass
