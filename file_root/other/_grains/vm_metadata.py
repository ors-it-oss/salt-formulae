# -*- coding: utf-8 -*-
'''
Do stuff
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import json
import yaml
import requests

# Salt - grains need modules imported as well because they're loaded before them
import salt.modules.cmdmod
import salt.utils.path
__salt__ = {
    'cmd.run': salt.modules.cmdmod.run,
}

# Logging & Debugging
import logging
# import pprint
# ppr = pprint.PrettyPrinter(indent=2).pprint
log = logging.getLogger(__name__)

SCW = salt.utils.path.which('/usr/local/bin/scw-metadata-json')


def __virtual__():
    '''
    whatever
    '''
    return True


def scw():
    '''
    SCW metadata
    '''
    if not SCW:
        return {}

    scw_data = json.loads(SCW)
    scw_res = {
        'tags': scw_data['tags'],
        'public_ip': scw_data['public_ip'],
        'name': scw_data['name'],
        'location': scw_data['location'],
        'commercial_type': scw_data['commercial_type'],
        'organization': scw_data['organization']
    }
    return {'provider': scw_res}


def cloudstack():
    '''
    Retrieve DHCP server metadata
    :param vrouter:
    :return:
    '''
    def _fetch(ep):
        url = 'http://{0}/latest/{1}'.format(vrouter, ep)
        fres = fetcher.get(url)
        if fres.status_code != 200:
            return None
        return fres.content

    def _dhcp_server():
        '''
        TODO: Fluorine (2019.2) is rumoured to have __grains__ in grains
        '''
        if not salt.utils.path.which('ip'):
            return None

        dhcp = None
        for l in __salt__['cmd.run']('ip route', output_loglevel='quiet').splitlines():
            if l.startswith('default'):
                intf = l.split(' ')[4]
                break

        if salt.utils.path.which('netplan'):
            data = __salt__['cmd.run']('netplan ip leases {}'.format(intf), output_loglevel='quiet')
            for l in data.splitlines():
                if l.startswith('SERVER_ADDRESS'):
                    dhcp = l.split('=')[-1].strip()
                    break
        elif salt.utils.path.which('nmcli'):
            data = __salt__['cmd.run']('nmcli -f DHCP4 d sh {}'.format(intf), output_loglevel='quiet')
            if data:
                for l in data.splitlines():
                    if 'dhcp_server_identifier' in l:
                        dhcp = l.split('=')[-1].strip()
                        break
        return dhcp

    vrouter = _dhcp_server()
    if not vrouter:
        return {}

    fetcher = requests.Session()

    # Might (not) do anything about the password server;
    # http://bazaar.launchpad.net/~cloud-init-dev/cloud-init/trunk/view/head:/cloudinit/sources/DataSourceCloudStack.py

    try:
        user_data = json.loads(_fetch('user-data'))
    except requests.exceptions.RequestException as e:
        log.trace('Unable to fetch cloud metadata: {}'.format(e))
        return {}
    except (AttributeError, ValueError):
        try:
            user_data = [d for d in yaml.load_all(user_data)]
            if len(user_data) == 1:
                user_data = user_data[0]
        except (AttributeError, ValueError):
            pass

    csdata = {
        'user-data': user_data
    }

    for meta in _fetch('meta-data').splitlines():
        data = _fetch(meta.decode('utf8')).splitlines()
        if len(data) == 1:
            data = data[0]
        csdata[meta] = data

    return {'provider': csdata}
