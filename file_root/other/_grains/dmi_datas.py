# -*- coding: utf-8 -*-
'''
Get a lot of extra info through DMI(decode)
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import uuid
import re

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.path
import salt.modules.cmdmod
import salt.modules.smbios
__salt__ = {
    'cmd.run': salt.modules.cmdmod._run_quiet,
    'smbios.get': salt.modules.smbios.get,
    'smbios.records': salt.modules.smbios.records,
}

# Logging & Debugging
import logging
# import pprint
# pp = pprint.PrettyPrinter(indent=4)
log = logging.getLogger(__name__)

DMIDECODE = salt.utils.path.which_bin(['dmidecode'])
CPU_FIELDS = ('family', 'manufacturer', 'signature', 'version')
RAM_FIELDS = ('array_handle', 'bank_locator', 'asset_tag', 'form_factor',
              'locator', 'manufacturer', 'part_number',
              'size', 'serial_number', 'set', 'speed', 'type')


def __virtual__():
    '''
    Only work when dmidecode is installed.
    '''
    return DMIDECODE is not None


def get_uuid():
    '''
    Retrieve system's UUID
    '''

    sys_uuid = __salt__['smbios.get']('system-uuid')
    if uuid is not None:
        return {'uuid': sys_uuid}


def better_serial():
    '''
    Retrieve system's serial and sanitize the return to make it sane
    '''

    for serial in ('system-serial-number', 'chassis-serial-number', 'baseboard-serial-number'):
        serialnr = __salt__['smbios.get'](serial)
        if serialnr is not None:
            return {'serialnumber': serialnr}

    return {'serialnumber': None}


def get_xtra_cpu():
    '''
    Retrieve additional CPU info
    '''

    cpu_grains = __salt__['smbios.records'](4, CPU_FIELDS)
    cpu_sockets = len(cpu_grains)
    cpu_grains = dict([('cpu_' + key, val) for key, val in cpu_grains[0]['data'].items()
                  if key in CPU_FIELDS and val is not None and len(str(val))])

    if cpu_grains['cpu_manufacturer'] == 'Bochs':
        return {'num_sockets': cpu_sockets}

    cpu_grains['num_sockets'] = cpu_sockets
    cpu_grains['cpu_manufacturer'] = re.match(r'^[a-zA-Z0-9- .]+',
                                              cpu_grains['cpu_manufacturer']).group(0)

    model = cpu_grains.get('cpu_version', cpu_grains['cpu_family'])
    manufacturer = cpu_grains['cpu_manufacturer']

    model = re.sub(r'(?i)(\((tm|r|c)\)|cpu|processor|@ .*$)', '', model)
    model = re.sub(r'(?i)({0})'.format(manufacturer), '', model)
    model = re.sub(r'\s+', ' ', model).strip()
    cpu_grains['cpu_version'] = model

    return cpu_grains


def get_ram_info():
    '''
    Retrieve info's about RAM
    '''
    ram_data = __salt__['smbios.records'](17, RAM_FIELDS)

    ram_grains = {}
    for ram in ram_data:
        ram = dict([(key, val) for key, val in ram['data'].items()
                    if key in RAM_FIELDS and val is not None and len(str(val))])

        # Get rid of the other cruft
        if not re.match(r'[0-9]+', ram['size']):
            continue

        ram['size'] = int(re.match(r'([0-9]+)', ram['size']).groups()[0])

        if 'type' in ram:
            ram['type'] = '{0} {1}'.format(ram['type'], ram.pop('form_factor'))
        if 'speed' in ram:
            ram['type'] += ' @' + ram['speed']
            ram['speed'] = int(re.match(r'([0-9]+)', ram['speed']).groups()[0])
        if 'locator' in ram:
            del ram['array_handle']
            ram_grains[ram.pop('locator')] = ram
        else:
            ram_grains[ram.pop('array_handle')] = ram

    # pp.pprint(ram_grains)

    return {'memory': ram_grains}
