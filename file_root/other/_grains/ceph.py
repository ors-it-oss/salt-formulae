# -*- coding: utf-8 -*-
'''
Collect Ceph Grains of Salt.
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.path
import salt.modules.cmdmod
__salt__ = {
    'cmd.run': salt.modules.cmdmod._run_quiet,
}

# Logging & Debugging
import logging
log = logging.getLogger(__name__)

CEPH = salt.utils.path.which_bin(['ceph'])
CEPH_BASE = '/var/lib/ceph'
CEPH_TYPES = ["osd", "mon", "mds"]


def __virtual__():
    '''
    Only work when Ceph is actually installed.
    '''
    return CEPH is not None


def _get_ceph_version():
    '''
    Get Ceph version
    '''

    ceph_ver = __salt__['cmd.run']('{0} -v'.format(CEPH)).split()[2:4]

    versioninfo = [int(v) for v in ceph_ver[0].split('.')]
    versioninfo.append(ceph_ver[1][1:-1])

    return ceph_ver[0], versioninfo


def _get_ceph_types():
    types = []
    ids = {}

    for ceph_type in CEPH_TYPES:
        dir = '{0}/{1}'.format(CEPH_BASE, ceph_type)

        if os.path.isdir(dir):
            try:
                subdirs = os.listdir(dir)

                if len(subdirs) > 0:
                    ids[ceph_type] = []
                    types.append(ceph_type)

                for subdir in subdirs:
                    cluster, cid = subdir.split('-', 1)
                    ids[ceph_type].append(cid)

            except Exception as e:
                raise e

    return types, ids


def get_ceph_info():
    verion, versioninfo = _get_ceph_version()
    res = {
        'version': verion,
        'versioninfo': versioninfo,
    }

    types, ids = _get_ceph_types()
    if types:
        res['types'] = types
    if ids:
        res['ids'] = ids

    return {'ceph': res}
