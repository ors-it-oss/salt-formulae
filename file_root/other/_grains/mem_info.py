# -*- coding: utf-8 -*-
'''
Add swap / vmem info
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import os

# Salt - grains need modules imported as well because they're loaded before them
import salt.utils.files
import salt.utils.platform
import salt.modules.cmdmod
__salt__ = {
    'cmd.run': salt.modules.cmdmod._run_quiet,
}

# Logging & Debugging
import logging
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only works on Linux. 4 now.
    '''
    return salt.utils.platform.is_linux()


def get_mem_info():
    '''
    Get Additional memory informations
    '''
    if salt.utils.platform.is_linux():
        meminfo = '/proc/meminfo'
        if not os.path.isfile(meminfo):
            return {}

        with salt.utils.files.fopen(meminfo, 'r') as ifile:
            for line in ifile:
                comps = line.rstrip('\n').split(':')
                if not len(comps) > 1:
                    continue
                if comps[0].strip() == 'MemTotal':
                    mem_total = int(comps[1].split()[0]) / 1024
                elif comps[0].strip() == 'SwapTotal':
                    swap_total = int(comps[1].split()[0]) / 1024
        vmem_total = mem_total + swap_total
    elif salt.utils.platform.is_freebsd():
        # TODO
        return

    return {'mem_swap': swap_total, 'mem_vtotal': vmem_total}
