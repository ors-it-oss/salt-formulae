# -*- coding: utf-8 -*-
'''
Various tools for Virtualisation
'''
from __future__ import absolute_import, print_function, unicode_literals

# Python
import collections
import datetime
import json
import os
import re
import random
import time

# Salt
import salt.utils.path
from salt._compat import ipaddress
import salt.ext.six as six
from salt.utils.decorators import depends


# Logging & Debug
import logging
import pprint
ppr = pprint.PrettyPrinter(indent=2).pprint

MIB = (1024 ** 2)
GIB = (1024 ** 3)

log = logging.getLogger(__name__)

CONNTRACK = salt.utils.path.which('conntrack') is not None
RSYNC = salt.utils.path.which('rsync') is not None
BTRFS = salt.utils.path.which('btrfs') is not None
QIMG = salt.utils.path.which('qemu-img') is not None


def __virtual__():
    '''
    Be false sometimes
    '''
    return True


@depends(CONNTRACK)
def conns(vms=None, threshold=None):
    '''
    Get an IPv4 connection count per VM
    :param vms:
    :param threshold:
    :return:
    '''
    if vms is None:
        vms = frozenset(__salt__['virt.list_vms']())
    elif isinstance(vms, six.string_types):
        vms = frozenset([vms])
    else:
        vms = frozenset(vms)

    addrmap = {}
    macmap = {}

    for ip in __salt__['network.ip_addrs']():
        addrmap[ip] = 'hypervisor'

    for mac in __grains__['hwaddr_interfaces'].values():
        macmap[mac] = 'hypervisor'

    for vm in vms:
        for mac in __salt__['virt.get_macs'](vm):
            macmap[mac] = vm

        ips = set()
        # Very CS specific :(
        brules = __salt__['cmd.run']('ebtables -t nat -L {0}-in-ips'.format(vm)).splitlines() + \
                 __salt__['cmd.run']('ebtables -t nat -L {0}-out-ips'.format(vm)).splitlines()

        # IPv6 would be nice once CS does it
        for line in brules:
            if line.startswith('-p ARP'):
                ip = re.findall(r'[0-9\.]+', line)[0]
                ips.add(ip)

        for ip in ips:
            log.debug('Adding IP {1} for VM {0}'.format(vm, ip))
            addrmap[ip] = vm

    missing = vms - set(addrmap.values())
    if len(missing):
        log.error("Can't find IP's for VM's {0}".format(missing))

    log.warning(addrmap)
    log.warning(macmap)

    alladdrs = frozenset(addrmap.keys())
    ctrack = collections.defaultdict(int)
    spoofips = set()

    log.info('Parsing conntrack table...')
    # stderr gives useless reports clobbering the parser
    for line in __salt__['cmd.run_stdout']('conntrack -L -o extended', output_loglevel='quiet').splitlines():
        centries = set()
        for col in line.split():
            if col.startswith(('src', 'dst')):
                entry = col.split('=')[1]
                # if re.match(r'[0-9\.]+', entry) or re.match(r'[0-9a-f:]+', entry):
                centries.add(entry)

        # log.warning(line)
        # log.warning(centries)
        if len(centries - alladdrs) == len(centries):
            log.debug('{0} has no ip addresses at all assigned to me or VMs'.format(centries))
            spoofips.update(centries)

        for ip in centries:
            ctrack[ip] += 1

    log.info('Continuing')

#    log.warning('Spoofs: {0}'.format(spoofips))
    spoofips.discard('255.255.255.255')
    spoofips.discard('0.0.0.0')
    spoofconns = {}
    for ip in spoofips:
        if ip.endswith('255'):
            continue
        else:
            ipaddr = ipaddress.ip_address(ip)
            if ipaddr.is_loopback or ipaddr.is_link_local or ipaddr.is_multicast:
                continue
            spoofconns[ip] = ctrack[ip]

    ipconns = collections.defaultdict(int)
    for ip, count in ctrack.items():
        if ip in alladdrs:
            ipconns[addrmap[ip]] += count

#    log.warning('Spoofs: {0}'.format(spoofconns))
#    log.warning('CTrack: {0}'.format(ctrack))
    if threshold:
        if '%' in threshold:
            threshold = max(ipconns.values()) * (int(threshold[:-1]) / 100)

        ipconns = dict([(k, v) for k, v in ipconns.items() if v > threshold])

    return {'hypervisor': ipconns.pop('hypervisor', None), 'instances': ipconns, 'suspects': spoofconns}


def tcpdump(vm, time=5, filter='', direction=None):
    '''
    Sample the netflow of a VM
    TODO: Only deals with nic0 :(
    '''
    intf = __salt__['virt.get_nics'](vm).values()[0]['target']

    return __salt__['nettools.tcpdump'](time, intf, filter, direction=direction)


def find_mac(mac):
    for vm in __salt__['virt.list_vms']():
        if mac in __salt__['virt.get_macs'](vm):
            return vm


def is_running(vms=None, interval=1, count=5, win=3):
    '''
    Check if a VM is actually running or if it's just burning CPU cycles
    '''
    if vms is None:
        vms = __salt__['virt.list_vms']()
    elif isinstance(vms, six.string_types):
        vms = [vms]

    vms = dict(((vm, [0, None]) for vm in vms))
    for vm in vms:
        score, regs = vms[vm]
        nregs = registers(vm)

    i = 0
    while i < count:
        i += 1

    pass


def registers(vm):
    '''
    Retrieve CPU register info for a VM
    '''
    cmd = 'virsh qemu-monitor-command {0} '.format(vm)
    cmd += """'{ "execute": "human-monitor-command", "arguments": { "command-line": "info registers" } }'"""

    reg_out = __salt__['cmd.run_all'](cmd, output_loglevel='quiet')
    if reg_out['retcode'] != 0:
        return None

    reg_out = json.loads(reg_out['stdout'])['return']

    registers = {}
    for line in reg_out.splitlines():
        regs = iter(re.split(r'(?:^|\s)([A-Z0-9]+\s*)=', line)[1:])
        for reg in regs:
            reg = reg.rstrip(' =')
            val = next(regs).strip()
            registers[reg] = val

    return registers


def reg_changed(vm, interval=1, count=3):
    '''
    Return changed CPU register names over count x interval measurements
    '''
    changed = set()
    ref_reg = registers(vm)
    while count > 0:
        next_reg = registers(vm)
        changed.update([reg for reg, val in next_reg.items() if val != ref_reg[reg]])
        ref_reg = next_reg
        count -= 1
        time.sleep(interval)

    return tuple(changed)


def frozen(vms=None, interval=1, count=3, threshold=2):
    if vms is None:
        vms = frozenset(__salt__['virt.list_vms']())
    elif isinstance(vms, six.string_types):
        vms = frozenset([vms])
    else:
        vms = frozenset(vms)

    frozen_vms = []
    for vm in vms:
        if len(reg_changed(vm, interval, count)) < threshold:
            log.debug('{0} seems frozen'.format(vm))
            frozen_vms.append(vm)
        else:
            log.debug('{0} seems running'.format(vm))

    return frozen_vms


@depends(BTRFS, QIMG, RSYNC)
def drsave(target='/var/backups/libvirt/.current', retention=3):
    '''
    Save all QCOW images to a target for disaster recovery purposes

    Assumes a BTRFS filesystem is in place like this

    ls {target}/../:
       - {target-subvol}
       - 2017-01-01 (ro snap)
       - 2017-01-02

    etc.; dates will be readonly snapshots of {target}

    :param target: 
    :param retention: 
    :return: 
    '''
    def rsync_qcow(name, src, dst):
        log.debug('RSyncing QCOW {0}...'.format(name))
        res = __salt__['cmd.run_all'](
            'rsync --checksum-seed=32761 --archive --inplace {0} {1}'.format(src, dst),
            output_loglevel='debug'
        )
        if res['retcode'] != 0:
            log.warning('Error RSyncing {0}: {1}'.format(name, res['stderr']))

        return res['retcode'] == 0

    target_d = os.path.dirname(target)
    btrfs_vol = __salt__['cmd.run_all']('btrfs subvolume show {0}'.format(target))
    if btrfs_vol['retcode'] != 0:
        log.error('Error determining BTRFS info for {0}: {1}'.format(target, btrfs_vol['stderr']))
        return False
    else:
        # Don't keep doing this while we're busy
        # (& don't care what the return is if it's not)
        __salt__['cmd.run_bg'](
            'btrfs balance cancel {0}'.format(target_d),
            cwd=target_d,
            output_loglevel='debug'
        )

    date_backup = datetime.date.today()
    done = set()
    vms = __salt__['virt.list_domains']()

    vms = ['i-207-192-VM']
    for vm in vms:
        log.info('Backing up VM {0}...'.format(vm))
        for dev, disk in __salt__['virt.get_disks'](vm).items():
            if disk.get('file format', False) != 'qcow2':
                continue

            try:
                chain = json.loads(__salt__['cmd.run_stdout'](
                    'qemu-img info --output=json --backing-chain {0}'.format(disk['file']),
                    output_loglevel='quiet'
                ))
            except Exception:
                log.warning('Error fetching chain info for {0}, skip'.format(disk['file']))
                continue

            # We process the rsyncable ones first; the active one last
            for qcow in chain[1:]:
                qcow = qcow['filename']
                qcow_b = os.path.basename(qcow)

                if qcow_b in done:
                    log.debug('QCOW {0} already rsynced, skipping'.format(qcow_b))
                elif rsync_qcow(qcow_b, qcow, os.path.join(target, qcow_b)):
                    done.add(qcow_b)

            # Told you
            active = chain[0]['filename']
            active_b = os.path.basename(active)
            active_t = os.path.join(target, active_b)

            log.debug('Blockcopying active QCOW {0}'.format(active_b))
            blockcopy_cmd = 'virsh blockcopy {vm} {dev} {target} --shallow --wait --finish'.format(
                vm=vm, dev=dev, target=active_t
            )
            if os.path.isfile(active_t):
                blockcopy_cmd += ' --reuse-external'

            res = __salt__['cmd.run_all'](
                blockcopy_cmd, output_loglevel='debug',
                timeout=3600
            )
            if res['retcode'] == 0:
                done.add(active_b)
            elif 'imed out' in res['stdout']:
                # TODO: I'm not sure any of this is very effective in terms of actually making a half-decent backup
                log.warning('Blockcopy of {0} timed out, falling back to rsync method...'.format(active_b))
                if rsync_qcow(active_b, active, active_t):
                    done.add(active_b)
            else:
                log.warning('Error blockcopying {0}: {1}'.format(active_b, res['stderr']))

    log.info('All VMs backed up, purging orphan stuff...')
    for leftover in set(__salt__['file.readdir'](target)[2:]) - done:
        log.debug('Removing {0}'.format(leftover))
        __salt__['file.remove'](os.path.join(target, leftover))

    target_snap = os.path.join(target_d, date_backup.isoformat())
    log.debug('Snapshotting {0} to {1}...'.format(target, target_snap))
    res = __salt__['cmd.run_all'](
        'btrfs subvolume snapshot -r {current} {snap}'.format(current=target, snap=target_snap),
        cwd=target_d,
        output_loglevel='debug'
    )
    if res['retcode'] != 0:
        log.error('Error creating BTRFS snapshot'.format(target_snap, res['stderr']))

    log.debug('Purging old snapshots...')
    date_cutoff = date_backup - datetime.timedelta(days=retention)
    for node in __salt__['file.readdir'](target_d)[2:]:
        try:
            if datetime.datetime.strptime(node, '%Y-%m-%d').date() <= date_cutoff:
                snap = os.path.join(target_d, node)
                res = __salt__['cmd.run_all'](
                    'btrfs subvolume delete {0}'.format(snap),
                    cwd=target_d,
                    output_loglevel='debug'
                )
                if res['retcode'] != 0:
                    log.error('Error deleting BTRFS snapshot {0}'.format(snap))
        except Exception:  # pylint: disable=broad-except
            # TODO: It *is* too broad in this way
            continue

    # Rebalance ~1/10 times
    if random.randint(1, 10) == 5:
        log.info('Rebalancing backup filesystem')
        __salt__['cmd.run_bg'](
            'btrfs balance start {0}'.format(target_d),
            cwd=target_d,
            output_loglevel='debug'
        )

    log.info('Backup done')
