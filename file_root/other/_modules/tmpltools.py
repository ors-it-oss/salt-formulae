# -*- coding: utf-8 -*-
'''
Handydandy functions meant for templating
'''
from __future__ import absolute_import, print_function, unicode_literals

# Python
import datetime
import decimal
import os
import textwrap
import re

# External
import salt.ext.six as six

# Salt
import salt.utils.dictupdate
import salt.utils
from salt.defaults import DEFAULT_TARGET_DELIM

# Logging & Debug
import logging

# Date format for the timestamp
DATE_FMT = '%Y-%m-%d %H:%M'
DATE_FMT_ISO = '%Y-%m-%dT%H:%M:%S'
UNITS = {
    'k': 1,
    'm': 2,
    'g': 3,
    't': 4,
    'p': 5,
    'e': 6
}

log = logging.getLogger(__name__)


def coalesce_by(dicts, keys, skip_none=True, skip_empty=False, lookup=None, default=None):
    '''
    document:
    one dict, one key
    more dict, one key
    more dict, more key
    [one dict, two dict], [one key, two key]

    Semi-analogous to SQL's COALESCE + utils.filter_by
    :param dicts: list of dicts to lookup key(s)
    :param keys: key or list of keys to lookup
    :param skip_none: skip values of None
    :param skip_empty: skip empty values
    :param default: if no matches found, return this
    :param lookup: dict to lookup returned value in
    :return:
    '''
    if isinstance(dicts, dict):
        dicts = [dicts]
        keys = [keys]
    elif isinstance(keys, six.string_types):
        dkeys = []
        for _ in dicts:
            dkeys.append(keys)
        keys = dkeys

    rval = None
    for idx, dictl in enumerate(dicts):
        dkeys = keys[idx]
        if isinstance(dkeys, six.string_types):
            dkeys = [dkeys]

        for key in dkeys:
            log.trace('Checking {0} against {1}'.format(key, dictl))
            rval = salt.utils.traverse_dict_and_list(dictl, key, KeyError)
            if all((
                rval is not KeyError,
                rval is not None or not skip_none,
                len(rval) or not skip_empty
            )):
                break

    if rval is not None and lookup is not None:
        rval = salt.utils.traverse_dict_and_list(lookup, rval, None)

    if rval is None and default is not None:
        rval = default

    return rval


def get_from(from_dict, key, default=None, delimiter=DEFAULT_TARGET_DELIM):
    '''
    Wrap around the .get(bla:bla:bla) function
    '''
    return salt.utils.traverse_dict_and_list(from_dict, key, default, delimiter)


def rhel():
    '''
    RHEL definer; if it's RedHat but not Fedora, set it to major

    :return: major version if RHEL, False otherwise
    '''

    if __grains__['os_family'].lower() == 'redhat' and __grains__['os'].lower() != 'fedora':
        return __grains__['osrelease_info'][0]
    else:
        return False


def ubuntu():
    '''
    Ubuntu definer; if it's Ubuntu, return the version as float

    :return: <YY>.<MM> version if Ubuntu, False otherwise
    '''

    if __grains__['os'] == 'Ubuntu':
        return float(__grains__['osrelease'])
    else:
        return False


def deepjoin(coll, sep=False):
    '''
    deepjoiner; iterate deeply over collections en concatenate everything into a single string with a separator

    :param coll: Collection to concatenate
    :param sep: separator or False to return the resulting list
    :return: a very long string
    '''
    res = []

    def join(i):
        if isinstance(i, dict):
            join(i.values())
        elif not hasattr(i, '__iter__') or isinstance(i, six.string_types):
            res.append(str(i))
        else:
            for j in i:
                join(j)

    join(coll)

    if sep is False:
        return res
    else:
        return sep.join(res)


def datadefault(key, default=None, persist=False, sync=False):
    '''
    data-store default; fetch & return pillar key (and save if sync=True or erase if persist=False).
    :param key: pillar-key to fetch
    :param default: default if no values found
    :param persist: keep the default in data
    :param sync: update data with pillar
    :return: pillar value or data value or default
    '''

    val = __salt__['pillar.get'](key, None)

    if val is not None:
        if not persist:
            __salt__['data.pop'](key)
        elif sync:
            __salt__['data.update'](key, val)
    else:
        val = __salt__['data.get'](key)
        if val is None:
            val = default
            __salt__['data.update'](key, val)

    return val


def xround(val, method='half_even', unit=None, low=None, high=None):
    '''
    handydandy rounder wrapper

    :param val: value to round
    :param method: one of the methods of decimal ROUND_
    :param unit: increments / precision
    :param low: minimum
    :param high: maximum
    :return: number

    e.g.

    1/2 of RAM rounded to 128MB's
    xround(grains.ram/2, unit=128, min=512, max=2048)

    xround(150.31/122.23, unit=.00)

    '''
    # putting in float(1.1234) still screws it up
    val = str(val)
    prec = 0
    if unit:
        unit = decimal.Decimal(str(unit))
        val = decimal.Decimal(val) / unit
        prec = unit.as_tuple().exponent
        if prec != 0:
            prec = decimal.Decimal(10) ** prec

    res = decimal.Decimal(val).quantize(
        prec,
        rounding=getattr(decimal, 'ROUND_{}'.format(method.upper()))
    )

    if unit:
        res *= unit

    if high:
        res = min((res, decimal.Decimal(str(high))))
    if low:
        res = max((res, decimal.Decimal(str(low))))

    if prec:
        res = float(res)
    else:
        res = int(res)

    return res


def datestamp():
    stamp_date = datetime.datetime.today()
    stamp_date = stamp_date.strftime(DATE_FMT)
    return stamp_date


def datedelta(start=None, stop=None, outfmt=None):
    '''
    Difference between two dates in outfmt or DATE_FMT
    :param fromdt:
    :param todt:
    :param outfmt:
    :return:
    '''
    if start is None and stop is None:
        raise ValueError('Need either start or stop')
    elif start is None:
        start = datetime.datetime.today()
    elif stop is None:
        stop = datetime.datetime.today()


def bconv(value, out, base=None):
    '''
    Byte convert
    :param value: 10mib, '100 gb' etc
    :param out: 't'
    :param base: override base (2/10)
    :return:
    '''
    value = value.lower()
    out = out.lower()
    if 'i' in out or base == 2:
        base = 1024

    o_power = UNITS[out.strip(' ib')]
    value, i_power = re.split(r'[0-9]+', value, maxsplit=1)
    value = int(value)
    i_power = UNITS[i_power.strip(' ib')]

    pwr = abs(o_power - i_power)
    res = value / (base ** pwr)

    return res


def dhparams(path, generator=2, strength=4096, user='root', group='root', outform='PEM'):
    '''
    DH Parameter files

    :return: DH Param generating state YAML
    '''
    return textwrap.dedent('''\
        {path}-dhparam:
          cmd.run:
          - name: openssl dhparam -out {path} -outform {outform} -{generator} {strength}
          - unless: test -f {path}
          file.managed:
          - name: {path}
          - mode: 600
          - replace: False
          - user: {user}
          - group: {group}
    '''.format(**locals()))


def pki(name=None,
        pillar=None,
        ca=None,
        role='tls',
        alt_names=[],
        ip_sans=[],
        files=None,
        user='root', group='root',
        dhparams=False,
        schedule=True,
        triggers=None,
        schedule_triggers=None
        ):
    '''
    PKI stuffs
    :param name: CN
    :param pillar:
    :param ca:
    :param role:
    :param alt_names:
    :param ip_sans:
    :param files:
    :param user:
    :param group:
    :param dhparams:
    :param schedule:
    :return:
    '''
    assert pillar is not None or ca is not None, 'Need pillar or ca'

    res = ''

    if not files:
        cert_store, key_store = __salt__['grains.filter_by']({
            'Debian': ['/etc/ssl/certs', '/etc/ssl/private'],
            'RedHat': ['/etc/pki/tls/certs', '/etc/pki/tls/private']
        }, default='RedHat')
        files = {
            os.path.join(cert_store, name + '-ca.pem'): 'chain',
        }
        if name:
            files.update({
                os.path.join(cert_store, name + '.pem'): ['pub', 'chain'],
                os.path.join(key_store, name + '-key.pem'): 'key'
            })
        else:
            files.update({
                os.path.join(cert_store, ca + '.pem'): ['ca'],
            })

    if pillar:
        cert = __salt__['pillar.get'](pillar)
        for fpath, comps in files.items():
            mode = '0644'
            contents = []
            if not isinstance(comps, (list, tuple)):
                comps = [comps]
            for comp in comps:
                if comp == 'key':
                    mode = '0600'
                compres = {
                    'chain': cert.get('ca_chain', cert['issuing_ca']),
                    'root': cert.get('ca_chain', [cert['issuing_ca']])[-1],
                    'int': cert.get('ca_chain', [None])[:-1],
                    'ca': cert['issuing_ca'],
                    'pub': cert['certificate'],
                    'key': cert['private_key']
                }[comp]
                if isinstance(compres, (list, tuple)):
                    contents.extend(compres)
                elif compres:
                    contents.append(compres)
                contents = textwrap.indent(contents, 22 * ' ')
            res += textwrap.dedent('''\
                pki-{ca}-{name}-{path}:
                  file.managed:
                  - name: {path}
                  - contents: |
                      {contents}
                  - mode: {mode}
                  - user: {user}
                  - show_changes: False
            '''.format(**locals()))

    elif ca:
        if not name:
            if isinstance(ca, (list, tuple)):
                ca_name = ','.join(ca)
                res += textwrap.dedent('''\
                    pki-{ca_name}-bundle:
                      vaultx.pub:
                      - ca: {ca}
                ''')
            else:
                res += textwrap.dedent('''\
                    pki-{ca}-pub:
                      vaultx.pub:
                      - ca: {ca}
                ''')
            if files:
                res += '\n  - files: {files}'

        else:
            res += textwrap.dedent('''\
                pki-{ca}-{name}:
                  vaultx.cert:
                  - ca: {ca}
            ''')
            res += '\n  - name: {name}'
            res += '\n  - role: {role}'
            res += '\n  - owner: {user}'
            res += '\n  - schedule: {schedule}'
            if alt_names:
                res += '\n  - alt_names: {alt_names}'
            if ip_sans:
                res += '\n  - ip_sans: {ip_sans}'
            if files:
                res += '\n  - files: {files}'
            if triggers:
                res += '\n  - triggers: {triggers}'
            if schedule_triggers:
                res += '\n  - schedule_triggers: {schedule_triggers}'

    if dhparams:
        res += dhparams(user=user, group=group)

    return res.format(**locals())
