# -*- coding: utf-8 -*-
'''
Inventory
'''
from __future__ import absolute_import, print_function, unicode_literals

# Python
import re
import os
import collections
from salt._compat import ipaddress

# Salt
import salt.utils.path
# Logging & Debugging
import logging

log = logging.getLogger(__name__)

MIB = [(1024**2), 'MiB']
GIB = [(1024**3), 'GiB']
SIZE = MIB
ETHTOOL = salt.utils.path.which('ethtool')
LSPCI = salt.utils.path.which('lspci')
UDEVADM = salt.utils.path.which('udevadm')


def net():
    '''
    Report of all IP's & MAC's
    '''
    mac = set()
    allips = {
        'inet': set(),
        'inet6': set()
    }
    for ifname, data in __salt__['network.interfaces']().items():
        if not data['up'] or 'hwaddr' not in data or data['hwaddr'] == '00:00:00:00:00:00':
            continue

        ifmac = data['hwaddr']
        if ETHTOOL:
            try:
                r_mac = __salt__['cmd.run']('ethtool -P {0}'.format(ifname))
                r_mac = re.search(r'(([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2})', r_mac).groups()
                if len(r_mac) > 0 and r_mac[0] != '00:00:00:00:00:00':
                    ifmac = r_mac[0]
            except:
                pass
        mac.add(ifmac)

        for proto in ('inet', 'inet6'):
            for addr in data.get(proto, []):
                try:
                    ifip = ipaddress.ip_interface('{0}/{1}'.format(
                        addr['address'], addr.get('netmask', addr.get('prefixlen', None))))
                    if not ifip.is_link_local and not ifip.is_loopback:
                        allips[proto].add(str(ifip))
                except:
                    pass

    net = {}
    if len(mac):
        net['hwaddr'] = sorted(mac)

    for proto, ips in allips.items():
        if len(ips):
            net[proto] = sorted(ips)

    return net


def eth():
    if __grains__['virtual'] != 'physical':
        return

    eths = {}
    for ifname, data in __salt__['network.interfaces']().items():
        if data['hwaddr'] == '00:00:00:00:00:00':
            continue
        elif not ETHTOOL:
            log.warning('ethtool not found, continuing...')
            continue

        ethif = {}
        driver = {}
        for line in __salt__['cmd.run']('ethtool -i {0}'.format(ifname)).split('\n'):
            key, value = line.split(':', 1)
            key = key.strip()
            value = value.strip()
            if value:
                driver[key] = value
        if not driver.get('bus-info', None) or not re.match(r'[0-9A-Za-z:.]+$', driver['bus-info']):
            continue

        for line in __salt__['cmd.run']('lspci -mmvs {0}'.format(driver['bus-info'])).split('\n'):
            key, value = line.split(':', 1)
            key = key.lower().strip()
            if key == 'vendor':
                ethif['manufacturer'] = value.strip()
            elif key == 'device':
                ethif['model'] = value.strip()

        if driver.get('firmware-version', None):
            ethif['fwversion'] = driver['firmware-version']

        speed = __salt__['cmd.run']('ethtool {0}'.format(ifname))
        speed = sorted(re.findall(r'([0-9]+)base', speed))
        if len(speed):
            ethif['speed'] = int(speed[-1])

        if len(ethif):
            eths[ifname] = ethif

    if len(eths):
        return eths
    else:
        return None


def platform():
    sw_platform = {
        'kernel': __grains__['kernelrelease'],
        'arch': __grains__['osarch'],
    }

    if 'lsb_distrib_description' in __grains__:
        sw_platform['os'] = __grains__['lsb_distrib_description']
    else:
        sw_platform['os'] = '{0} {1}'.format(__grains__['osfullname'], __grains__['osrelease'])

    platform = {
        'platform': sw_platform
    }

    if __grains__['virtual'] != 'physical':
        return platform

    platform['system'] = {
        'manufacturer': __grains__['manufacturer'],
        'product': __grains__['productname'],
        'fwversion': __grains__['biosversion']
    }
    if __grains__['serialnumber'] is not None:
        platform['system']['serial'] = __grains__['serialnumber']

    bmc = ipmi()
    if bmc:
        platform['ipmi'] = bmc

    ether = eth()
    if ether:
        platform['ether'] = ether

    return platform


def ipmi():
    if 'ipmi' not in __grains__:
        return {}

    ipmi_data = __grains__['ipmi']
    if isinstance(ipmi_data, bool) and ipmi_data:
        ipmi_data = {'detected': ipmi_data}
    else:
        ipmiv6 = __salt__['pillar.get']('netconf:nets:ipmi:ip6:net', False)
        if ipmiv6 and 'hwaddr' in ipmi_data:
            ipmi_data['inet6'] = __salt__['nettools.mac2eui64'](ipmi_data['hwaddr'], ipmiv6)

    try:
        for line in __salt__['cmd.run']('/usr/sbin/bmc-info --get-device-id').split('\n'):
            key, value = line.split(':', 1)
            key = key.strip().lower().replace(' ', '_')
            if re.search(r'firmware.*rev', key, flags=re.IGNORECASE):
                ipmi_data['fwversion'] = value.strip()
                break
    except Exception:
        pass

    return ipmi_data


def part():
    pass


def disk():
    disks = {}
    for grain_id, report_id in (('disks_solid', 'ssd'), ('disks_magnetic', 'hdd')):
        if grain_id not in __grains__:
            continue
        disks[report_id] = {}
        for dev, drive in __grains__[grain_id].items():
            size = drive['size'] / SIZE[0]
            disk = {
                'size': size,
                'size_unit': SIZE[1],
                'hsize': '{0} {1}'.format(size, SIZE[1])
            }
            if __grains__['virtual'] != 'physical':
                disks[report_id][dev] = disk
                continue

            disk.update({
                'product': drive['model'],
                'serial': drive['serial'],
                'revision': drive['revision'],
                'speed_unit': 'rpm'
            })
            if 'rpm' in drive:
                disk['speed'] = drive['rpm']
            else:
                # An average 7.2k drive has
                # 1000/(1/(7200 / 60000)) =~ 120 IOPS
                # An average SSD can be counted on doing ~5.000
                # That would add up to 300.000 RPM ;)
                disk['speed'] = 300000

            for loc in ('enclosure', 'slot', 'port'):
                if loc in drive:
                    disk[loc] = drive[loc]

            try:
                smart = {}
                for attr, data in __salt__['disk.smart_attributes'](
                        dev, (5, 187, 188, 197, 198), ('attribute_name', 'raw_value')):
                    smart[data['attribute_name'].lower()] = data['raw_value']
                disk['smart'] = smart
            except Exception:
                pass

            disks[report_id][dev] = disk

    return disks


def svc():
    server_ports = collections.defaultdict(dict)
    for entry in __salt__['network.netstat']():
        if entry['inode'] == '0' or ('state' in entry and entry['state'] != 'LISTEN'):
            continue
        port = int(entry['local-address'].rsplit(':', 1)[1])

        if '/' not in entry['program']:
            # log.warning(entry)
            cmd = 'kernel?'
        else:
            pid, proc = entry['program'].split('/', 1)
            pid = int(pid)
            try:
                cmd = ' '.join(__salt__['ps.proc_info'](pid, ['cmdline'])['cmdline'])
            except Exception:
                cmd = proc

        proto = entry['proto']

        if cmd not in server_ports:
            server_ports[cmd] = collections.OrderedDict()

        if port not in server_ports[cmd]:
            server_ports[cmd][port] = set()

        server_ports[cmd][port].add(proto)

    for cmd, open_ports in server_ports.items():
        opened = []
        for port, proto in open_ports.items():
            opened.append('{0}/{1}'.format(port, ','.join(sorted(proto))))

        server_ports[cmd] = opened

    return server_ports


def mem():
    mem = {
        'mem': {
            'total': __grains__['mem_vtotal'],
            'physical': __grains__['mem_total'],
            'swap': __grains__['mem_swap'],
        }
    }

    if __grains__['virtual'] != 'physical':
        return mem

    physical = []
    for bank, dimm in __grains__['memory'].items():
        ram = {
            'manufacturer': dimm['manufacturer'],
            'product': dimm.get('part_number', None),
            'serial': dimm.get('serial_number', None),
            'location': bank,
            'model': '{0}MiB {1}'.format(dimm['size'], dimm['type']),
            'hsize': '{0} MiB'.format(dimm['size']),
            'size': dimm['size'],
            'size_unit': 'MB',
        }
        if 'speed' in dimm:
            ram.update({
                'speed_unit': 'Mhz',
                'speed': dimm['speed']
            })

        physical.append(ram)

    mem['ram'] = physical

    return mem


def cpu():
    cpu = {
        'cores': __grains__['num_cpus']
    }

    if __grains__['virtual'] != 'physical':
        return cpu

    cpu.add({
        'count': __grains__['num_sockets'],
        'manufacturer': __grains__['cpu_manufacturer'],
        'product': __grains__['cpu_version'],
    })
    return cpu


def report():

    report = {}

    uuid = __grains__['uuid']
    if uuid is not None:
        report['uuid'] = uuid

    # svc_all = set(__salt__['service.get_all']())
    # svcs = svc_all.difference(SVC_BLACKLIST)
    # svc_blist = SVC_BLACKLIST.difference(svc_all)
    # for svc in svcs.copy():
    #     for black in svc_blist:
    #         if re.match(black, svc):
    #             svcs.discard(svc)
    # report['services'] = sorted([svc for svc in svcs if __salt__['service.status'](svc)])

    report['services'] = svc()

    report['net'] = net()

    report.update(platform())

    report.update(disk())

    report.update(mem())

    return {__grains__['nodename']: report}


def ssd():
    return disk().get('ssd', None)


def hdd():
    return disk().get('hdd', None)


def pick(query):
    '''
    Pick a device from the inventory

    pickers:
    largest|smallest|fastest|slowest

    domains:
    ssd|hdd|disk|net

    example:
    pick('smallest ssd')
    pick('fastest smallest disk')
    '''
    # TODO: fs? part?
    domains = {
        'net': [eth],
        'ssd': [ssd],
        'hdd': [hdd],
        'disk': [ssd, hdd],
    }

    pickers = {
        'largest': ('size', -1),
        'smallest': ('size', 0),
        'fastest': ('speed', -1),
        'slowest': ('speed', 0),
    }

    # keep the order of pickers
    picker = query.split(' ')
    query = set(picker)

    # Parse pickers out of the query
    picker = [pick for pick in picker if pick in query.intersection(pickers.keys())]
    query = query.difference(picker)

    # Now parse out the requested search domain
    domain = query.intersection(domains.keys())
    if len(domain) != 1:
        return {'error': 'Too many search domains: {0}'.format(', '.join(sorted(domain)))}
    query = query.difference(domain)

    # TODO; stuff like min_size, max_size, min_speed, max_speed etc.
    if len(query):
        log.debug('unparsed: Unparsed strings: {0}'.format(', '.join(sorted(query))))

    # Call every function in the search domain to get a list of nodes
    nodes = []
    for func in domains[domain.pop()]:
        res = func()
        if res is None:
            continue
        for key, node in res.items():
            node['key'] = key
            nodes.append(node)

    if not len(nodes):
        return None

    # The picker value corresponds to a field name
    # take that field, and order the entries by keying on it
    # and then sorting it
    for pick in picker:
        pick_order = collections.defaultdict(list)
        key, idx = pickers[picker.pop()]
        for node in nodes:
            pick_order[node[key]].append(node)
        nodes = pick_order[sorted(pick_order.keys())[idx]]

    return nodes


def root(top=False):
    '''
    Return base root blkdev
    if disk=True it will return the top level blockdev
    :return:

    '''
    root_dev = __salt__['mount.active']()['/']['device']
    root_dev = os.path.realpath(os.path.expandvars(root_dev))
    root_dev = os.path.basename(root_dev)

    if top and __salt__['udev.env']('/dev/{0}'.format(root_dev))['DEVTYPE'] == 'partition':
        root_dev = re.sub(r'[0-9]+$', '', root_dev)

    # TODO: check before output
    return root_dev
