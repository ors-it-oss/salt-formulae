# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

import json
import urllib3
import logging

log = logging.getLogger(__name__)


def prefixes(asNumber=48635):
    if asNumber == None:
        return False

    ret = []

    try:
        # https://stat.ripe.net/data/routing-status/data.json?resource=8.8.8.8
        http = urllib3.PoolManager()
        uri = "https://stat.ripe.net/data/routing-history/data.json?min_peers=0&resource=AS%d" % asNumber
        r = http.request("GET", "%s" % uri)
        if r.status >= 400:
            return False

        data = json.loads(r.data.decode("utf-8"))
        for origin in data["data"]["by_origin"]:
            for prefix in origin["prefixes"]:
                ret.append(prefix["prefix"])
    except Exception as e:
        log.error("Failed to fetch prefixes for AS%d: %s" % (asNumber, e))
        pass

    return ret
