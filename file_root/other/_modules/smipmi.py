# -*- coding: utf-8 -*-
'''
SuperMicro IPMI stuff
'''
# https://www.thomas-krenn.com/en/wiki/Supermicro_IPMI_Security_Updates_July_2014#Plaintext_Login_Credentials_in_save_config.bin

# Import python libs
from __future__ import absolute_import, print_function, unicode_literals
# import salt.ext.six as six

import logging
import re
import datetime
import xml.etree.cElementTree
import tempfile
import shutil
import os

try:
    import requests
    HAS_REQUESTS = True
except Exception:
    HAS_REQUESTS = False

# Import salt libs
import salt.version
from salt.utils.odict import OrderedDict

log = logging.getLogger(__name__)

# these two lines enable debugging at httplib level (requests->urllib3->httplib)
# you will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# the only thing missing will be the response.body which is not logged.
# import httplib
# httplib.HTTPConnection.debuglevel = 1

# cache for instantiated sessions
_IPMI = {}


def __virtual__():
    '''
    Needs requests
    '''
    return HAS_REQUESTS


class SuperMicroIPMI(object):
    '''
    Do the stuff that IPMI doesn't do
    Through the BUI
    '''
    ACCEPT_XML = {'Accept': 'application/xml, text/xml, */*'}
    USER_AGENT = 'Salt/{0}'.format(salt.version.__version__)

    def __init__(self, host, name='ADMIN', pwd='ADMIN'):
        if not host.startswith('http'):
            host = 'http://' + host
        elif host.startswith('https'):
            # We really shouldn't
            requests.packages.urllib3.disable_warnings()
        if not host.endswith('/'):
            host += '/'
        host += 'cgi/'

        # little confused here, didn't I just say that???
        # calling like SuperMicroIPMI(host, None, None) breaks that, so redo here(???)
        if name is None:
            name = 'ADMIN'
        if pwd is None:
            pwd = 'ADMIN'

        self._session = requests.Session()
        self._session.headers['User-Agent'] = self.USER_AGENT
        self._session.post(host + 'login.cgi', data={'name': name, 'pwd': pwd})
        if 'SID' not in self._session.cookies:
            log.error('No SID returned')
            raise Exception
        self._host = host

    def get(self, url, params=None, **kwargs):
        params = self.stamp(params)

        log.debug('SMIPMI get {0}{1} with {2}'.format(self._host, url, params))
        resp = self._session.get(self._host + url, params=params, verify=False, **kwargs)
        if resp.status_code != 200:
            raise Exception
        else:
            log.debug(resp.content)
            return resp.headers['Content-Type'], resp.content

    # def get_xml(self, url, params=None, **kwargs):
    #     ctype, resp = self.get(url, params, headers=self.ACCEPT_XML, **kwargs)
    #     if 'xml' not in ctype:
    #         raise Exception
    #     resp = xml.etree.cElementTree.fromstring(resp)
    #     resp = self._xml2dict(resp)
    #
    #     log.debug(resp)
    #
    #     return resp

    def post(self, url, data, stamp=True, **kwargs):
        if stamp:
            data = self.stamp(data)
        log.debug('SMIPMI post {0}{1} with {2}'.format(self._host, url, data))
        resp = self._session.post(self._host + url, verify=False, data=data, **kwargs)
        if resp.status_code != 200:
            log.error(resp.content)
            raise Exception
        else:
            return resp.headers['Content-Type'], resp.content

    @staticmethod
    def stamp(params=None):
        '''
        All gets must be timestamped in this (ridiculous) format
        '''
        if params is None:
            params = []
        else:
            params = params.items()

        params.append(('time_stamp', datetime.datetime.utcnow().strftime('%a %b %d %Y %T GMT (UTC)')))
        return params


def _gimme_ipmi(host, name='ADMIN', pwd='ADMIN'):
    '''
    if IPMI is already a requests session, return that
    else, return a new IPMI Session
    '''
    log.debug('passed on {0}'.format(host))

    idx = (host, name, pwd)
    if idx not in _IPMI:
        _IPMI[idx] = SuperMicroIPMI(host, name, pwd)
    return _IPMI[idx]


def _post(host, url, params=None, name=None, pwd=None, **kwargs):
    ipmi = _gimme_ipmi(host, name, pwd)
    ctype, resp = ipmi.post(url, params, **kwargs)
    return ctype, resp


def _get(host, url, params=None, name=None, pwd=None, **kwargs):
    ipmi = _gimme_ipmi(host, name, pwd)
    return ipmi.get(url, params=params, **kwargs)


# def _get_xml(host, url, params=None, name=None, pwd=None, full=False, **kwargs):
#     ipmi = _gimme_ipmi(host, name, pwd)
#     resp = ipmi.get_xml(url, params=params, **kwargs)
#
#     if not full:
#         resp = _reduce(resp)
#     return resp


def _post_xml(host, url, params=None, name=None, pwd=None, full=False, **kwargs):
    ipmi = _gimme_ipmi(host, name, pwd)
    ctype, resp = ipmi.post(url, data=params, headers=SuperMicroIPMI.ACCEPT_XML, **kwargs)
    if 'xml' not in ctype:
        log.warning(resp)
        raise Exception

    resp = xml.etree.cElementTree.fromstring(resp)
    resp = _xml2dict(resp)

    log.debug(resp)

    if not full:
        resp = _reduce(resp)
    return resp


def _reduce(data):
    '''
    It seems always two levels deep are empty;
    Kick 'em out if they're lonely
    { IPMI:
        { GENERIC_INFO:
            { GENERIC: None, KERNAL: None }
        }
    }
    '''
    if len(data) == 1:
        data = data[data.keys()[0]]
    if len(data) == 1:
        data = data[data.keys()[0]]
    return data


def _xml2dict(tree):
    '''
    Put XML returns into a nice dict.
    Currently it seems text/content is never returned
    So only attributes are evaluated
    '''
    tdict = {}
    for item in tree:
        if len(item.getchildren()) > 0:
            tdict.update(_xml2dict(item))
        else:
            attrib = {}
            for attr, val in item.attrib.items():
                val = val.strip()
                if not len(val) or re.match('0000', val):
                    continue
                try:
                    val = int(val)
                except Exception:
                    try:
                        val = float(val)
                    except Exception:
                        pass
                attrib[attr.lower()] = val

            if not len(attrib):
                continue
            ikey = item.tag.lower()
            if ikey in tdict:
                if isinstance(tdict[ikey], list):
                    tdict[ikey].append(attrib)
                else:
                    tdict[ikey] = [tdict[ikey], attrib]
            else:
                tdict[ikey] = attrib

    return {tree.tag.lower(): tdict}


def _update(host, url, name, pwd, new, current=None, bools=None):
    log.debug('current:{0}, new:{1}'.format(current, new))

    if current is not None:
        for key in current.keys():
            if key in new:
                current[key] = new[key]

        new = current
        # new['_'] = ''
        # if bools is not None:
        #     log.debug(bools)
        #     for key in bools:
        #         if new[key] == 0:
        #             new[key] = 'off'
        #         else:
        #             new[key] = 'on'

    ctype, resp = _post(host, url, params=new, stamp=False, name=name, pwd=pwd)

    log.debug('return {0} {1}'.format(ctype, resp))

    return resp


def info(host, name=None, pwd=None):
    url = 'ipmi.cgi'
    params = {'GENERIC_INFO.XML': '(0,0)'}
    return _post_xml(host, url, params, name=name, pwd=pwd)


def config(host, name=None, pwd=None):
    url = 'ipmi.cgi'
    params = {'CONFIG_INFO.XML': '(0,0)'}
    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def ipv6(host, name=None, pwd=None):
    url = 'ipmi.cgi'
    params = {'CONFIG_IPV6.XML': '(0,0)'}
    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def smbios(host, name=None, pwd=None):
    url = 'ipmi.cgi'
    params = {'SMBIOS_INFO.XML': '(0,0)'}
    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def date_time(host, name=None, pwd=None, settings=None):
    '''
    Supported settings I know of:
    timezone  : (+/- seconds from UTC)
    dst_en    : 1/0 for DST
    ntp       : on/off
    ntp_server_pri/ntp_server_2nd
    year, month, day, hour, min, sec
    '''

    if settings is not None:
        url = 'config_date_time.cgi'
        current = date_time(host, name, pwd)['info']
        resp = _update(host, url, name, pwd, current, settings, bools=['ntp'])
        # log.debug(resp)

    url = 'ipmi.cgi'
    params = {'GET_DATE_TIME.XML': '(0,0)'}

    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def set_iso(host, smburl, name=None, pwd=None):
    '''
    host, path, user, pwd
    '''

    smburl = re.sub(r'^[a-z]+://', '', smburl)
    smbuser = ''
    smbpass = ''
    if '@' in smburl:
        smbcred, smburl = re.split(r'@', smburl, maxsplit=1)
        smbuser, smbpass = re.split(r':', smbcred, maxsplit=1)

    smbserver, smbpath = re.split(r'[/\\]', smburl, maxsplit=1)
    smbpath = '\\' + re.sub(r'/', '\\', smbpath)

    url = 'virtual_media_share_img.cgi'
    params = {
        'host': smbserver,
        'path': smbpath,
        'user': smbuser,
        'pwd': smbpass
    }

    return _post(host, url, params=params, name=name, pwd=pwd)


def vmedia(host, name=None, pwd=None):
    '''
    Current virtual media settings
    '''
    # url = 'vmstatus.cgi'
    # res = _post_xml(host, url, name=name, pwd=pwd)

    url = 'ipmi.cgi'
    params = {'VIRTUAL_MEDIA_SHARE_IMAGE.XML': '(0,0)'}

    res = _post_xml(host, url, params=params, name=name, pwd=pwd)

    return res


def fru(host, name=None, pwd=None):
    '''
    List FRU info (not that it ever seems to return anything useful, mind you)
    '''
    url = 'ipmi.cgi'
    params = {'FRU_INFO.XML': '(0,0)'}
    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def platform(host, name=None, pwd=None):
    '''
    Return platform information
    '''
    url = 'ipmi.cgi'

    params = {'Get_PlatformCap.XML': '(0,0)'}
    res = _post_xml(host, url, params=params, name=name, pwd=pwd)

    try:
        params = {'Get_PlatformInfo.XML': '(0,0)'}
        res.update(_post_xml(host, url, params=params, name=name, pwd=pwd))
    except Exception:
        pass

    return res


def mouse(host, name=None, pwd=None, mode=None):
    if mode is not None:
        url = 'config_mouse_mode.cgi'
        resp = _post_xml(host, url,  {'mouse_mode': mode}, name, pwd, stamp=False)
        # just return here?
        log.debug('resp'+str(resp))

    url = 'ipmi.cgi'
    params = {'GET_MOUSE_MODE.XML': '(0,0)'}

    return _post_xml(host, url, params=params, name=name, pwd=pwd)


def extract(fwbin):
    '''
    Extract the key from the bin
    :param fwbin:
    :return:

    https://github.com/devicenull/ipmi_firmware_tools
    '''

    temp_dir = tempfile.mkdtemp(prefix='smipmifw-')
    temp_fw = '{0}/{1}'.format(temp_dir, os.path.basename(fwbin))
    temp_fwdir = '{0}/_{1}.extracted'.format(temp_dir, os.path.basename(fwbin))

    shutil.copy(fwbin, temp_fw)

    __salt__['cmd.run']('binwalk -e {0}'.format(temp_fw))

    #mount cramfs
    #strings bin/ipmi_conf_backup_tool | grep -A 1 -B 1 -m 1 openssl | head -1

