# -*- coding: utf-8 -*-
'''
Simple net toolies
'''
from __future__ import absolute_import, print_function, unicode_literals
# Python
import base64
import binascii
import hashlib
import re
import xml.etree.cElementTree

# Salt
import salt.utils.path
import salt.utils.http
from salt.utils.decorators import depends
from salt._compat import ipaddress

# 3rd Party
try:
    try:
        import Cryptodome.Random as CRand  # pylint: disable=E0611
    except ImportError:
        import Crypto.Random as CRand  # pylint: disable=E0611
    HAS_RANDOM = True
except ImportError:
    HAS_RANDOM = False

# Logging & Debugging
import logging
#import pprint
#ppr = pprint.PrettyPrinter(indent=2).pprint

log = logging.getLogger(__name__)

HAS_DIG = salt.utils.path.which('dig') is not None
HAS_ETHTOOL = salt.utils.path.which('ethtool') is not None


def split(cidr, ip=None):
    '''
    Simple utility function which splits a cidr into a net, prefix, netmask tuple
    but only if ip is in it.
    '''
    try:
        cidr = ipaddress.ip_interface(cidr)
        if ip is not None and ipaddress.ip_address(ip) not in cidr.network:
            return False
        return str(cidr.network), str(cidr.network.prefixlen), str(cidr.netmask)
    except Exception:
        return


def propose(cidr, nid, first=None, taken=None):
    '''
    Propose an IP from CIDR
    :param cidr: CIDR range to pick from
    :param nid: integer identifying server
    :param first: start with this IP
    :param taken: blacklist
    :return: proposed IP

    e.g.
    propose('10.0.0.0/24', 5) -> 10.0.0.5
    propose('10.0.0.0/24', 3, 4) -> 10.0.0.6
    propose('10.0.0.0/24', 5, '10.0.0.4') -> 10.0.0.8
    propose('10.0.0.0/24', 5, taken=['10.0.0.5']) -> 10.0.0.6
    '''
    try:
        cidr = ipaddress.ip_network(cidr, strict=True)

        while True:
            if first is None:
                ip = cidr[nid]
            else:
                try:
                    ip = cidr[first + nid - 1]
                except Exception:
                    first = ipaddress.ip_address(first)
                    ip = first + nid - 1
            ip = str(ip)
            if taken is not None and ip in taken:
                nid += 1
                continue

            return str(ip)
    except Exception:
        return


def tcpdump(timeout, intf, filter='', direction=None):
    '''
    Run tcpdump for timeout on intf with filter
    Optional direction in, out or inout
    '''
    cmd = 'timeout {0} tcpdump -Ktqennp'.format(timeout)
    if direction is not None:
        cmd = '{0} -P {1}'.format(cmd, direction)
    cmd = "{0} -i {1} '{2}'".format(cmd, intf, filter)
    log.debug(cmd)
    return __salt__['cmd.run_all'](cmd, output_loglevel='quiet', ignore_retcode=True)


def mac2eui64(mac, prefix=None):
    '''
    Convert a MAC address to a EUI64 address
    or, with prefix provided, a full IPv6 address

    http://tools.ietf.org/html/rfc4291#section-2.5.1
    '''
    eui64 = re.sub(r'[.:-]', '', mac).lower()
    eui64 = eui64[0:6] + 'fffe' + eui64[6:]
    eui64 = hex(int(eui64[0:2], 16) ^ 2)[2:].zfill(2) + eui64[2:]

    if prefix is None:
        return ':'.join(re.findall(r'.{4}', eui64))
    else:
        try:
            net = ipaddress.ip_network(prefix, strict=False)
            euil = int('0x{0}'.format(eui64), 16)
            return str(net[euil])
        except Exception:
            return


def ipmi_v6(prefix=None, grain=False):
    '''
    Try to generate the IPMI IPv6 address
    grain disabled; it won't properly merge with existing grain
    :return:
    '''

    ipmi = __salt__['grains.get']('ipmi:hwaddr', None)
    if ipmi is None:
        log.error('IPMI MAC grain not found')
        return False
    elif prefix is None:
        prefix = __salt__['pillar.get']('netconf:nets:ipmi:ip6:net', None)
        if prefix is None:
            log.error('No prefix given, pillar data not found')
            return False

    ipmi = __salt__['nettools.mac2eui64'](ipmi, prefix)

    # if grain:
    #     __salt__['grains.setval']('ipmi', {'inet6': ipmi})

    return ipmi


def get_net(ip, netconf, full=False):
    '''
    Return the netconf of an IP
    :param ip: IP adress
    :param netconf: netconf dict
    :param full: return full netconf, else matching proto only
    :return:
    '''

    ip = ipaddress.ip_interface(ip)
    proto = 'ip{0}'.format(ip.version)

    for name, net in netconf['nets'].items():
        try:
            cidr = ipaddress.ip_network(net.get(proto).get('net'), strict=True)
            if ip in cidr:
                if full:
                    return {name: net}
                else:
                    return {name: net['proto']}
        except Exception:
            pass


@depends(HAS_DIG)
def dns_secured(rectype, name, to_ip=False):
    '''    
    Perform a DNS lookup with dig, but only return something when it's DNSsec validated
    :param rectype:
    :param name:
    :param to_ip: Recurse on 'till only IP's are found (TODO)
    :return:
    '''


    cmd = 'dig -t {0} +noall +dnssec +answer +adflag +nocl +nottl '.format(rectype)
    if to_ip:
        cmd += '+additional '
        #TODO: actually make that work

    cmd += name

    res = __salt__['cmd.run'](cmd, output_loglevel='quiet', loglevel='debug')

    validated = {name: False}
    answer = []
    for line in res.splitlines():
        record, rtype, rvalue = re.split(r'\s+', line, maxsplit=2)
        log.warning(record)
        if rtype == 'RRSIG':
            validated = True
        elif rtype == rectype:
            answer.append(rvalue)

    if not len(answer):
        return
    elif not validated:
        return False
    else:
        return answer


def intf_of(ip):
    '''
    Return the interface of an IP
    :param ip: IP adress
    :return: ifname
    '''

    ip = ipaddress.ip_interface(ip)
    if ip.version == 6:
        ipfam = 'inet6'
    else:
        ipfam = 'inet'
    ip = str(ip.ip)

    for intf, data in __salt__['network.interfaces']().items():
        if ipfam in data:
            for addr in data[ipfam]:
                if addr['address'] == ip:
                    return intf

    return False


def collapse(cidrs, keep_addrs=True, as_str=True):
    '''
    Collapse CIDRs into the smallest possible set

    Keeps addresses as addresses
    :param cidrs: list of CIDR things
    :param keep_addrs: Collapse usually only returns nets, even if it's an address (becomes a /32 or /128 net)
    :param as_str: ipaddress objects can't be serialized
    :return:
    '''

    cidr_objs = {4: set(), 6: set()}
    ips = set()

    for cidr in cidrs:
        try:
            cidr = ipaddress.ip_address(cidr)
            ips.add(cidr)
        except Exception:
            try:
                cidr = ipaddress.ip_network(cidr, strict=True)
            except Exception:
                raise
        cidr_objs[cidr.version].add(cidr)

    for version in (4, 6):
        cidrs = cidr_objs[version]
        if not len(cidrs):
            continue

        cidrs = ipaddress.collapse_addresses(cidrs)

        if keep_addrs:
            cidr_aobjs = []
            for cidr in cidrs:
                if cidr.num_addresses == 1 and cidr.network_address in ips:
                    cidr_aobjs.append(cidr.network_address)
                else:
                    cidr_aobjs.append(cidr)
            cidrs = cidr_aobjs

        if as_str:
            cidrs = [str(cidr) for cidr in cidrs]

        cidr_objs[version] = cidrs

    return list(cidr_objs[4]) + list(cidr_objs[6])


@depends(HAS_ETHTOOL)
def mac_of(intf):
    '''
    Return the (as) REAL (as possible) MAC address of an interface
    :param intf: interface
    '''

    r_mac = __salt__['cmd.run_stdout']('ethtool -P {0}'.format(intf), output_loglevel='quiet')
    r_mac = re.search(r'(([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2})', r_mac).groups()[0]
    if len(r_mac) > 0 and r_mac != '00:00:00:00:00:00':
        return r_mac
    else:
        return False


def pick_wan(interfaces, filter_own=False):
    '''
    Pick IPv6 global scope ip's out of (lists of) network.interfaces 
    :param interfaces: (lists of) network.interfaces output
    :param filter_own: Exclude minion's own addresses
    '''
    wan_ips = set()

    if isinstance(interfaces, dict):
        interfaces = [interfaces]

    for intfs in interfaces:
        for intf in intfs.values():
            wan_ips.update((ip['address'] for ip in intf.get('inet6', []) if ip['scope'] == 'global'))

    if filter_own:
        wan_ips -= pick_wan(__salt__['network.interfaces'](), filter_own=False)

    return sorted(wan_ips)


def o365addrs(include_products=[], exclude_products=[], include_types=[], exclude_types=[], as_str=True):
    '''
    Download & parse the list of Office 365 addresses 
    
    __salt__['nettools.o365addrs'](include_products=['o365', 'EXO', 'Identity', 'CRLs', 'EOP', 'SPO'], include_types=['IPv4'])
    salt myminion nettools.o365addrs include_products="[o365, EXO, Identity, CRLs, EOP, SPO]" include_types="[IPv4]"

    :param include_products: 
    :param exclude_products: 
    :param include_types: 
    :param exclude_types: 
    :return: 
    '''
    URL = 'https://support.content.office.net/en-us/static/O365IPAddresses.xml'
    o365xml = salt.utils.http.query(URL)['body']
    dom_tree = xml.etree.cElementTree.fromstring(o365xml)

    # URL = '/home/me/Downloads/O365IPAddresses.xml'
    # with salt.utils.files.fopen(URL, 'r') as o365xml:
    #     dom_tree = xml.etree.cElementTree.fromstring(o365xml.read())

    updated = dom_tree.get('updated')
    # ppr('UPDATED ON: ' + updated)

    list_types = set()

    products = {}
    for prod in dom_tree.iter('product'):
        name = prod.get('name')
        if (include_products and name not in include_products) or (exclude_products and name in exclude_products):
            continue

        lists = {}
        for addr_list in prod.iter('addresslist'):
            addr_type = addr_list.get('type')
            if (include_types and addr_type not in include_types) or (exclude_types and addr_type in exclude_types):
                continue
            list_types.add(addr_type)
            lists[addr_type] = [addr.text for addr in addr_list.iter('address')]

        if any(lists.values()):
            products[name] = lists

    res = dict(((list_type, []) for list_type in list_types))
    for product, data in products.items():
        for list_type in list_types:
            res[list_type].extend(data.get(list_type, []))

    # Dedup & collapse
    if 'URL' in res:
        res['URL'] = tuple(set(res['URL']))

    for list_type in ('IPv4', 'IPv6'):
        if list_type in res:
            res[list_type] = tuple(collapse(res[list_type], as_str=as_str))

    return res


def ula_gen(seed=None, netmask=48):
    '''
    Generate an RFC 4193 'Unique' Local Address range
    The *example* in 3.2.2 takes some semi random stuff
    then proceeds to SHA1 it.
    So we do all that, but with sha256
    or just take some random characters.

    :param seed: seed string
    :param netmask: size of network (in multiples of 8)
    :return: CIDR network
    '''
    assert not netmask % 8 and 8 < netmask < 127, 'Netmask wrong'

    prgid = int((netmask - 8) / 8)
    if seed:
        prgid = hashlib.sha256(base64.b64encode(bytes(seed))).digest()[-prgid:]
    else:
        prgid = CRand.get_random_bytes(prgid)
    # Pseudo-Random Global ID
    prgid = b'\xfd' + prgid

    prefix = '{}::/{}'.format(
        b':'.join(re.findall(b'.{4}', binascii.hexlify(prgid))),
        netmask
    )

    return prefix


def valid_hostname(hostname):
    '''
    https://tools.ietf.org/html/rfc2396#section-3.2.2
    https://tools.ietf.org/html/rfc1034#section-3.1
    https://tools.ietf.org/html/rfc1123#page-13
    https://tools.ietf.org/html/rfc952

    https://tools.ietf.org/html/rfc2181#section-10

    :param hostname:
    :return:
    '''

    valid_hostname = r'^(?=.{2,253}\.$)(?!.*\.[0-9]+$)(?:[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9](?:\.|$))+'
    valid_srv = r'^(?=.{2,253}\.$)(?!.*\.[0-9]+$)(?:[a-zA-Z0-9-_]{2,63}[a-zA-Z0-9](\.|$))+'
