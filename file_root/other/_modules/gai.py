# -*- coding: utf-8 -*-
'''
Get AddrInfo lookupper
Deprecated as soon as https://github.com/saltstack/salt/pull/22511
hits
'''
from __future__ import absolute_import, print_function, unicode_literals

import logging
import socket

# import pprint
# pp = pprint.PrettyPrinter(indent=2)

log = logging.getLogger(__name__)

def AR(host):
    '''
    Return all Address record(s) for `host`.
    Always returns a list.
    CLI Example:
    .. code-block:: bash
        salt ns1 gai.AR www.google.com
    '''
    return _gai(host, socket.AF_INET) + _gai(host, socket.AF_INET6)


def A(host):
    '''
    Return the A record(s) for `host`.
    Always returns a list.
    CLI Example:
    .. code-block:: bash
        salt ns1 dnsutil.A www.google.com
    '''
    return _gai(host, socket.AF_INET)


def AAAA(host):
    '''
    Return the AAAA record(s) for `host`.
    Always returns a list.
    '''
    return _gai(host, socket.AF_INET6)


def _gai(host, family):
    '''
    Resolve a name using getaddrinfo
    '''
    # fall back to the socket interface, if we don't care who resolves
    try:
        addresses = [sock[4][0] for sock in socket.getaddrinfo(host, None, family, 0, socket.SOCK_RAW)]
        return addresses
    except socket.gaierror:
        return []
