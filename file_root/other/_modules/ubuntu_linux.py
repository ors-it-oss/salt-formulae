# -*- coding: utf-8 -*-
'''
Small helper module to find the latest LTS kernel on Ubuntu
'''
# Import python libs
from __future__ import absolute_import, print_function, unicode_literals
import logging

log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work for Ubuntus
    '''
    return __grains__['os'] == 'Ubuntu'


def latest_lts():
    pkg = __salt__['cmd.run']('apt-cache -q search linux-generic-hwe-')
    if not pkg:
        pkg = __salt__['cmd.run']('apt-cache -q search linux-generic-lts- ')

    pkg = sorted(pkg.splitlines())[-1]
    pkg = pkg.split()[0].strip()

    return pkg
