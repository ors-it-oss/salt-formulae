# -*- coding: utf-8 -*-
'''
Query Ceph for pillardata

Requires working config on Salt Master,
e.g. ceph --cluster MyCluster mon dump must work

Without mons there's no way to decently deliver data

So bootstrapping mons is a problem with this.
Probably we don't; use a bootstrap pillar with an fsid and a monmap in it to get mon minions to bootstrap?

- OR - the monmap is a fnmatch as well, returning salt data. Then an fsid could conceivably be datadefaulted

'''
from __future__ import absolute_import

# Python
import fnmatch
import json

# Salt libs
import salt.utils.path

# Set up logging
import logging
log = logging.getLogger(__name__)

# HAS_CEPH = salt.utils.path.which('ceph')


def __virtual__():
    '''
    Check for required lib and make this pillar available
    depending on outcome.
    '''
    return salt.utils.path.which('ceph') is not None


def _ceph_get(stuff, config=None, keyring=None, cluster=None):
    ceph = ['ceph', '--connect-timeout 5', '-f json']
    if cluster:
        ceph += ['--cluster', cluster]
    if config:
        ceph += ['-c', config]
    if keyring:
        ceph += ['--keyring', keyring]
    ceph.append(stuff)
    ceph = ' '.join(ceph)

    res = __salt__['cmd.run_all'](ceph, output_loglevel='quiet')
    if res['retcode'] != 0:
        log.error('Ceph command \'{0}\' failed: {1}'.format(ceph, res['stderr']))
        return {}

    try:
        return json.loads(res['stdout'])
    except ValueError:
        return {}


def ext_pillar(minion_id,
               pillar,  # pylint: disable=W0613
               cephmap):
    '''
    Read pillar data from Ceph via its API.
    confmap:
        dict w/url's as keys and a list of fnmatched domains for which the url is
    '''
    ceph_minion, dom = minion_id.split('.', 1)

    config = None
    for name, ceph in cephmap.items():
        for selector in ceph.pop('minions'):
            # TODO: Matching should be better
            if fnmatch.fnmatch(dom, selector):
                ceph['cluster'] = ceph.get('cluster', name)
                config = ceph
                break
    if not config:
        return {}

    conf_file = config.get('config', False)
    keyring = config.get('keyring', False)
    cluster = config.get('cluster', False)

    if conf_file and not __salt__['file.file_exists'](conf_file):
        log.error('Ceph Pillar config {0} not found'.format(conf_file))
        return {}
    elif keyring and not __salt__['file.file_exists'](keyring):
        log.error('Ceph Pillar config {0} not found'.format(config))
        return {}

    log.info('Querying Ceph at {0} for information for {1}'.format(cluster or conf_file, minion_id))

    mon_dump = _ceph_get('mon dump', **config)
    if not mon_dump:
        # We are not in contact with the cluster.
        return {}

    mons = {}
    for mon in mon_dump['mons']:
        name = mon.pop('name')
        mons[name] = mon['addr'].split('/')[0]
    ceph_pillar = {'ceph': {
        'fsid': mon_dump['fsid'],
        'mon': mons
    }}

    tree = _ceph_get('osd tree', **config)['nodes']
    bucket = [buck for buck in tree if buck['name'] == ceph_minion]
    if not bucket or len(bucket) != 1:
        log.info('{0} does not appear to be a Ceph bucket'.format(minion_id))
        return ceph_pillar

    children = [child for child in bucket[0].get('children', []) if child >= 0]
    if not children:
        return ceph_pillar

    osds = _ceph_get('osd dump', **config)['osds']
    if not osds:
        return ceph_pillar

    ceph_pillar['ceph']['osd'] = {}
    for osd in osds:
        if osd['osd'] not in children:
            continue

        data = {
            'state': osd['state'],
        }
        key = _ceph_get('auth get-key osd.{0}'.format(osd['osd']), **config)
        if isinstance(key, dict):
            data['key'] = key['key']

        if 'uuid' in osd and osd['uuid'] != '00000000-0000-0000-0000-000000000000':
            data['uuid'] = osd['uuid']

        ceph_pillar['ceph']['osd'][osd['osd']] = data

    return ceph_pillar
