# -*- coding: utf-8 -*-
'''
Passthrough certain vars & settings from the master to the minions
'''
from __future__ import absolute_import
# Python
import os

# Salt
import salt.utils.files
import salt.config
import salt.pillar.nodegroups

# Logging & Debugging
import logging
log = logging.getLogger(__name__)


def _ssh():
    '''
    Return the SSH pub key that Salt SSH will use
    '''
    # TODO: Automagically derive filename
    pub_path = os.path.join(
      __salt__['config.get']('pki_dir'),
      'ssh',
      'salt-ssh.rsa.pub')

    try:
        with salt.utils.files.fopen(pub_path, 'r') as pub_file:
            pub_key = pub_file.read().strip()
        return pub_key
    except (OSError, IOError) as e:
        log.warning('Unable to read SSH public key {0}: {1}'.format(pub_path, e))


def _version():
    '''
    Return Salt Masters major version
    '''
    return float('.'.join(__salt__['test.version']().split('.')[0:2]))


def _nodegroups(minion_id):
    '''
    Return ext_pillar nodegroups cauz that is superuseful
    '''
    salt.pillar.nodegroups.__opts__ = __opts__
    return next(iter(salt.pillar.nodegroups.ext_pillar(minion_id, {}).values()))


def ext_pillar(minion_id,
               pillar, env=None):  # pylint: disable=W0613

    if env and __opts__.get('pillarenv', '') not in env:
        return {}

    passthrough = {
        'nodegroups': _nodegroups(minion_id),
        'master': {
            'version': _version()
        }
    }
    pub = _ssh()
    if pub:
        passthrough['ssh'] = {'pub': pub}

    return {'salt': passthrough}
