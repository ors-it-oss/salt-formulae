# -*- coding: utf-8 -*-
'''
Pillar for accessing CloudStack info
'''
from __future__ import absolute_import

# Import python libs
import logging
# import time

# Import salt libs


# Set up logging
log = logging.getLogger(__name__)


def _host(minion_id, manager, data):
    # I wanted to use the CloudStack API, but that is not even remotely capable of returning all these fields
    # Then there was a huge-ass ugly-ass query here.
    # That's all been Saltified better; the data gathering is now in the minion module

    name, domain = minion_id.split('.', 1)
    domain_nothomas = domain.replace('zone01.', '')

    cluster = None
    for a_cluster in data['cluster'].values():
        # glob match? re? allover filter? split up into multiple calls?
        clu_name = a_cluster['name']
        if any((
            domain.startswith(clu_name),
            clu_name.startswith(domain),
            domain_nothomas.startswith(clu_name),
            clu_name.startswith(domain_nothomas)
        )):
            cluster = a_cluster
            break
    if cluster is None:
        log.debug('Unable to match domain {0} to cluster for minion {1}'.format(domain, minion_id))
        return False

    zone = data['zone'][cluster.pop('data_center_id')]
    pod = data['pod'][cluster.pop('pod_id')]

    hvdata = {
        'management': manager,
        'cluster': cluster,
        'pod': pod,
        'zone': zone,
    }

    host = None
    for cshost in data['host'].values():
        # SystemVM's don't have a cluster_id
        if cshost.get('cluster_id', None) == cluster['id'] and cshost['name'] in (name, minion_id):
            host = cshost
            break

    if host is None:
        log.debug('Unable to match host {0} to cluster {1} for minion {2}'.format(name, cluster['id'], minion_id))
    else:
        for key in host.keys():
            if key not in ('host', 'resource', 'pool'):
                del(host[key])

        hvdata.update(host)

    return hvdata


def _management(minion_id, manager, data):
    '''
    Management Minion needs to know about image stores
    '''
    if minion_id != manager:
        return False

    image_stores = {}

    for is_uuid, image_store in data.get('image_store', {}).items():
        is_host = image_store.get('host', False)

        if not is_host:
            # You're on your own here
            is_host = image_store.get('ip_address', False)
        elif 'infra.cldin.net' in is_host:
            is_host = is_host.split('.')[0].split('-')
            is_host = '{0}.{1}.cldin.net'.format(is_host[0], is_host[1])

        p_data = {
            'host': is_host,
            'export': image_store['export'],
            'name': image_store['name']
        }
        image_stores[is_uuid] = p_data

    return {'image_store': image_stores}


def _image_store(minion_id, manager, data):
    image_stores = data['image_store']

    target = []

    for image_store in image_stores.values():
        is_host = image_store.get('host', False)
        is_ip = image_store.get('ip_address', False)

        if is_host:
            if minion_id == is_host:
                target.append(image_store)
                break

            is_ip = []
            for res in __salt__['dnsutil.A'](is_host), __salt__['dnsutil.AAAA'](is_host):
                if res and len(res):
                    is_ip.extend(res)

            if any(ip in __grains__.get('ipv4', []) + __grains__.get('ipv6', []) for ip in is_ip):
                target.append(image_store)
                break

        elif is_ip:
            is_ip = image_store.get('ip_address', False)
            if is_ip and is_ip in __grains__.get('ipv4', []) + __grains__.get('ipv6', []):
                target.append(image_store)
                break
        else:
            raise KeyError('Strange image_store; no host or ip_address')

    if not len(target):
        return False

    return {'management': manager, 'image_store': target}


def ext_pillar(minion_id,
               pillar,  # pylint: disable=W0613
               tgt,
               expr_form='glob',
               refresh=300):
    '''
    Cache & fetch cloudstack.infradata from tgt / expr_form minion
    Match it against the querying minion & return applicable data.

    Now if only some cloud management software were capable of doing this...
    '''

    # TODO: salt/salt/utils/cache.py!!!
    # TODO2: Does this even have any use now that we have pillar caches?
    # ckey = '_pillar_cs_{0}_{1}'.format(tgt, expr_form)
    # data = __salt__['data.get'](ckey)
    # if data is None or time.time() - data['time'] > refresh:
    #     log.debug('Refreshing CloudStack infradata for {0}...'.format(provider))
    #     data = __salt__['saltutil.cmd'](cloudstack.management, 'cloudstack.infradata').values()[0]
    #     if 'retcode' not in data or data['retcode'] != 0:
    #         # what to do? Return stale or nuttin'?
    #         return {}
    #     data = data['ret']
    #     __salt__['data.update'](ckey, {'time': time.time(), 'infradata': data})
    # else:
    #     log.debug('Pillaring up cached CloudStack infradata from {0} for {1}...'.format(data['time'], provider))
    #     data = data['infradata']

    # TODO3: Deal with the possibility of multiple minions as managers
    try:
        # res = __salt__['saltutil.cmd'](tgt, 'cloudstack.infradata', expr_form=expr_form)
        manager, data = __salt__['saltutil.cmd'](tgt, 'cloudstack.infradata', expr_form=expr_form).items()[0]
        log.debug('CloudStack ext_pillar cmd result: manager: {0}, data: {1}'.format(manager, data))

        if data['retcode'] != 0:
            log.error('CloudStack ext_pillar retrieval error: {0}'.format(data))
            return {}

        data = data['ret']
        cs_pillar = _management(minion_id, manager, data)
        if cs_pillar:
            return {'cloudstack': cs_pillar}
        elif __grains__.get('virtual', 'physical') == 'physical':
            cs_pillar = _host(minion_id, manager, data)
            if cs_pillar:
                return {'cloudstack': cs_pillar}

        cs_pillar = _image_store(minion_id, manager, data)
        if cs_pillar:
            return {'cloudstack': cs_pillar}

        return {}

    except Exception as e:
        log.error('CloudStack ext_pillar Exception caught!: {0}'.format(e))
    return {}
