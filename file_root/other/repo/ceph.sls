{%- set rhel = salt.tmpltools.rhel() %}
{%- set release = salt['pillar.get']('ceph:release', 'kraken') %}
ceph:
  pkgrepo.managed:
  - humanname: Ceph
{#    - key_url: https://download.ceph.com/keys/release.asc#}
  - key_url: salt://repo/keys/ceph.asc
{%- if grains.os_family == 'RedHat' %}
{%-   if grains.os == 'CentOS' %}
  - baseurl: http://eu.ceph.com/rpm-{{ release }}/el{{ grains.osmajorrelease }}/{{ grains.osarch }}/
{%-   else %}
  - baseurl: http://eu.ceph.com/rpm-{{ release }}/rhel{{ grains.osmajorrelease }}/{{ grains.osarch }}/
{%-   endif %}
  - gpgcheck: 0
{%- elif grains.os_family == 'Debian' %}
  - name: deb http://eu.ceph.com/debian-{{ release }} {{ grains.oscodename }} main
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/ceph.list
  - clean_file: True
{%- endif %}
