{%- set rhel = salt.tmpltools.rhel() %}
{%- set version = salt['pillar.get']('varnish:version', '5.1') %}
{%- set base_ver = version|replace('.', '') %}
{%- set base_url = 'https://packagecloud.io/varnishcache/varnish' ~ base_ver %}

varnish-repo:
  pkgrepo.managed:
  - key_url: {{ base_url }}/gpgkey
{%- if rhel %}
  - name: varnishcache_varnish{{ base_ver }}
  - baseurl: {{ base_url }}/el/{{ rhel }}/$basearch
  - gpgcheck: 0
  - repo_gpgcheck: 1
  - sslverify: 1
{%- elif grains.os_family == 'Debian' %}
  - humanname: Varnish Cache
  - name: deb {{ base_url }}/{{ grains.os|lower }}/ {{ grains.oscodename }} main
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/varnish.list
{%- endif %}
