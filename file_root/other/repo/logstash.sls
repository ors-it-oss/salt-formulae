{%- set version = salt['pillar.get']('logstash:version', '5.0') %}

logstash-repo:
  pkgrepo.managed:
  - humanname: Logstash
{%- if grains.os == 'CentOS' %}
  - baseurl: http://packages.elastic.co/logstash/{{ version }}/centos
  - gpgcheck: 1
  - gpgkey:  https://packages.elastic.co/GPG-KEY-elasticsearch
{%- elif grains.os_family == 'Debian' %}
  - key_url: https://packages.elastic.co/GPG-KEY-elasticsearch
  - name: deb http://packages.elastic.co/logstash/{{ version }}/debian stable main
  - dist: stable
  - file: /etc/apt/sources.list.d/logstash.list
{%- endif %}
