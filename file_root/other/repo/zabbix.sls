{%- set rhel = salt.tmpltools.rhel() %}
{%- set release = salt['pillar.get']('zabbix:release', 3.4) %}

{%- if rhel %}
{%-   set keyid = '79EA5ED4' %}
zabbix-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-Zabbix
  - source: salt://{{ slspath }}/keys/RPM-GPG-KEY-ZABBIX-{{ keyid }}
  - require_in:
    - pkgrepo: zabbix
    - pkgrepo: zabbix-non-supported
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-Zabbix
  - onchanges:
    - file: zabbix-gpg-key

zabbix:
  pkgrepo.managed:
  - humanname: Zabbix Official Repository - $basearch
  - baseurl: https://repo.zabbix.com/zabbix/zabbix/{{ release }}/rhel/$releasever/$basearch/
  - gpgcheck: 1
  - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Zabbix

zabbix-non-supported:
  pkgrepo.managed:
  - humanname: Zabbix Official Repository non-supported - $basearch
  - baseurl: https://repo.zabbix.com/zabbix/non-supported/rhel/$releasever/$basearch/
  - gpgcheck: 1
  - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Zabbix

{%- elif grains.os_family == 'Debian' %}
{%-   if grains.oscodename in ['squeeze' ] %}
{%-     set release = 2.2 %}
{%-   elif grains.oscodename in ['precise'] %}
{%-     set release = 2.4 %}
{%-   endif %}
{%-   if grains.oscodename in ['trusty'] %}
{%-     set keyid = '79EA5ED4' %}
{%-   elif release >= 3.0 %}
{%-     set keyid = 'A14FE591' %}
{%-   endif %}

zabbix-repo:
  pkgrepo.managed:
  - name: deb https://repo.zabbix.com/zabbix/zabbix/{{ release }}/{{ grains.os|lower }} {{ grains.oscodename }} main
  - dist: {{ grains.oscodename }}
  - clean_file: True
  - file: /etc/apt/sources.list.d/zabbix.list
{#    - keyid: {{ keyid }}#}
{#    - keyserver: keyserver.ubuntu.com#}
  - key_url: salt://{{ slspath }}/keys/zabbix_{{ keyid }}.key
{%- endif %}
