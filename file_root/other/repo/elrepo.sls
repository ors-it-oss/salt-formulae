{%- set rhel = salt.tmpltools.rhel() -%}
{## TODO:
    we might have use for a generic way to enable/disable some of these repo's
#}
{%- if rhel -%}
{%-   set base_url = 'http://www.elrepo.org/' -%}
{%-   set vers = {
        5: '5-5',
        6: '6-6',
        7: '7.0-2'
}[rhel] -%}
elrepo-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-elrepo.org
  - source: salt://{{ slspath }}/keys/RPM-GPG-KEY-elrepo.org

install-elrepo-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-elrepo.org
  - onchanges:
    - file: elrepo-gpg-key

elrepo-release:
  pkg.installed:
  - pkgs:
  - sources:
    - elrepo-release: {{ base_url ~ 'elrepo-release-' ~ vers ~ '.el' ~ rhel ~ '.elrepo.noarch.rpm' }}
  - onchanges_in:
    - pkg: elrepo-refreshed

elrepo-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - elrepo-release
{%- endif %}
