{%- set rhel = salt.tmpltools.rhel() %}
{%- set version = salt['pillar.get']('elasticsearch:version', '5.x') %}
{%- set major = version|string %}
{%- set major = major[0]|int %}

elasticsearch-repo:
  pkgrepo.managed:
  - humanname: Elasticsearch
{%- if grains.os_family == 'Debian' %}
{%-   if major < 5 -%}
  - name: deb http://packages.elastic.co/elasticsearch/{{ version }}/debian stable main
{%-   else %}
  - name: deb https://artifacts.elastic.co/packages/{{ version }}/apt stable main
{%-   endif %}
  - dist: stable
  - file: /etc/apt/sources.list.d/elasticsearch.list
  - key_url: salt://{{ slspath }}/keys/GPG-KEY-elasticsearch
{%- elif rhel %}
{%-   if major < 5 -%}
  - baseurl: http://packages.elastic.co/elasticsearch/{{ version }}/centos
{%-   else %}
  - baseurl: https://artifacts.elastic.co/packages/{{ version }}/yum
{%-   endif %}
  - file: /etc/yum.repo.list.d/elasticsearch.repo
  - gpgcheck: 1
  - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-elasticsearch

elasticsearch-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-elasticsearch
  - source: salt://{{ slspath }}/keys/GPG-KEY-elasticsearch
  - require_in:
    - pkgrepo: elasticsearch-repo

install-elasticsearch-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-elasticsearch
  - onchanges:
    - file: elasticsearch-gpg-key
{%- endif %}
