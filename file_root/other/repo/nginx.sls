{%- set rhel = salt.tmpltools.rhel() %}
{%- set version = salt['pillar.get']('nginx:version', 'mainline') %}
{%- set baseurl = 'http://nginx.org/packages/' %}
{%- if version == 'mainline' %}
{%-   set baseurl = baseurl ~ version ~ '/' %}
{%- endif %}

nginx-repo:
  pkgrepo.managed:
  - humanname: nginx repo
{%- if grains.os_family == 'Debian' %}
  - file: /etc/apt/sources.list.d/nginx.list
  - clean_file: True
  - name: deb {{ baseurl }}{{ grains.os|lower }}/ {{ grains.oscodename }} nginx
  - dist: {{ grains.oscodename }}
  - key_url: salt://{{ slspath }}/keys/nginx_signing.key
{%- elif rhel %}
  - baseurl: {{ baseurl }}{{ grains.os|lower }}/$releasever/$basearch/
  - gpgcheck: 1
  - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-nginx

nginx-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-nginx
  - source: salt://{{ slspath }}/keys/nginx_signing.key
  - require_in:
    - pkgrepo: nginx-repo

install-nginx-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-nginx
  - onchanges:
    - file: nginx-gpg-key
{%- endif %}
