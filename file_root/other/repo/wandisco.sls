{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel %}
{%-   set release = {
        6: '6-1',
        7: '7-2'
}[rhel] -%}
wandisco-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-WANdisco
  - source: salt://{{ slspath }}/keys/RPM-GPG-KEY-WANdisco
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-WANdisco
  - onchanges:
    - file: wandisco-gpg-key

wandisco-release:
  pkg.installed:
  - pkgs:
  - sources:
    - wandisco-git-release: http://opensource.wandisco.com/git/wandisco-git-release-{{ release }}.noarch.rpm
  - onchanges_in:
    - pkg: wandisco-refreshed

wandisco-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - wandisco-release
{%- endif %}
