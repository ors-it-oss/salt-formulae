{%- set rhel = salt.tmpltools.rhel() %}
{%- set version = salt['pillar.get']('mariadb:version', '10.2') %}

{%- if rhel %}
mariadb-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-MariaDB
  - source: salt://{{ slspath }}/keys/RPM-GPG-KEY-MariaDB
  - require_in:
    - pkgrepo: mariadb

install-mariadb-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-MariaDB
  - onchanges:
    - file: mariadb-gpg-key

mariadb:
  pkgrepo.managed:
  - humanname: MariaDB
  - baseurl: http://mirror.i3d.net/pub/mariadb/yum/{{ version }}/{{ grains.os|lower }}/$releasever/$basearch
  - gpgcheck: 1

{%- elif grains.os_family == 'Debian' %}
mariadb-repo:
  pkgrepo.managed:
  - humanname: MariaDB
  - name: deb http://mirror.i3d.net/pub/mariadb/repo/{{ version }}/{{ grains.os|lower }} {{ grains.oscodename }} main
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/mariadb.list
  - key_url: salt://{{ slspath }}/keys/mariadb.gpg
{%- endif %}
