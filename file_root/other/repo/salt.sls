{%- from 'salt/map.jinja' import release with context %}
{%- set rhel = salt.tmpltools.rhel() %}
{%- set release = (release, 2018.3)|min %} {# TODO: YUGH HARDCODED VERSION, YUGH HARDCODED RPM release #}
{%- if rhel %}
salt-repo-release:
  pkg.installed:
  - pkgs:
  - sources:
    - salt-repo: https://repo.saltstack.com/yum/redhat/salt-repo-{{ release }}-1.el{{ rhel }}.noarch.rpm
  - onchanges_in:
    - pkg: salt-repo-refreshed

salt-repo-wrong:
  file.absent:
  - name: /etc/yum.repos.d/saltstack.repo

salt-repo-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - salt-repo
{%- elif grains.os_family == 'Debian' %}
{%-   if grains.os == 'Debian' %}
{%-     set os_release = grains.osmajorrelease %}
{%-   else %}
{%-     set os_release = grains.osrelease %}
{%-   endif %}
salt-repo:
  pkgrepo.managed:
  - humanname: SaltStack {{ release }} Repo
  - clean_file: True
  - key_url: salt://{{ slspath }}/keys/SALTSTACK-GPG-KEY.pub
  - name: deb http://repo.saltstack.com/apt/{{ grains.os|lower }}/{{ os_release }}/amd64/{{ release }} {{ grains.oscodename }} main
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/saltstack.list
{%- endif %}
