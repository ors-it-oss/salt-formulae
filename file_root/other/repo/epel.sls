{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel %}
epel-release:
  pkg.installed:
  - pkgs:
  - sources:
{%-   if rhel == 5 %}
    - epel-release: http://download.fedoraproject.org/pub/epel/5/x86_64/epel-release-5-4.noarch.rpm
{%-   else %}
    - epel-release: http://download.fedoraproject.org/pub/epel/epel-release-latest-{{ rhel }}.noarch.rpm
{%-   endif %}
  - onchanges_in:
    - pkg: epel-refreshed

epel-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - epel-release
{%- endif %}
