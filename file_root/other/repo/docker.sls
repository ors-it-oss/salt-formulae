{%- set rhel = salt.tmpltools.rhel() %}
{%- if grains.os_family == 'RedHat' %}
docker-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-Docker
  - source: salt://{{ slspath }}/keys/docker-{{ grains.os|lower }}.gpg

install-docker-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-Docker
  - onchanges:
    - file: docker-gpg-key

docker-repo:
  cmd.run:
  - name:  yum-config-manager --add-repo https://download.docker.com/linux/{{ grains.os|lower }}/docker-ce.repo
  - creates: /etc/yum.repos.d/docker-ce.repo

{%- elif grains.os_family == 'Debian' %}
{%-   set channel = salt['pillar.get']('docker:channel', 'stable') %}
docker-repo:
  pkgrepo.managed:
  - humanname: Docker
  - key_url: salt://{{ slspath }}/keys/docker-{{ grains.os|lower }}.gpg
  - name: deb https://download.docker.com/linux/{{ grains.os|lower }} {{ grains.oscodename }} {{ channel }}
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/docker.list
  - clean_file: True
{%- endif %}
