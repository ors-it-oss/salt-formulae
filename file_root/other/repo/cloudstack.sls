{%- set rhel = salt.tmpltools.rhel() %}
{%- set release = salt['pillar.get']('cloudstack:release', '4.9') %}
cloudstack-repo:
  pkgrepo.managed:
  - humanname: Apache CloudStack {{ release }} Repo
{%- if rhel %}
  - file: /etc/yum.repo.list.d/cloudstack.list
  - gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CloudStack
  - baseurl: https://download.cloudstack.org/{{ grains.os|lower }}/$releasever/{{ release }}/
  - gpgcheck: 0

cloudstack-gpg-key:
  file.managed:
  - name: /etc/pki/rpm-gpg/RPM-GPG-KEY-CloudStack
  - source: salt://{{ slspath }}/keys/RPM-GPG-KEY-CloudStack
  - require_in:
    - pkgrepo: cloudstack-repo

install-cloudstack-gpg-key:
  cmd.run:
  - name: rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CloudStack
  - onchanges:
    - file: cloudstack-gpg-key

{%- elif grains.os_family == 'Debian' %}
{%-   set point_release = release|string %}
{%-   set point_release = point_release[-1]|int %}
  - clean_file: True
  - key_url: salt://{{ slspath }}/keys/cloudstack.asc
  - name: deb https://download.cloudstack.org/ubuntu {{ grains.oscodename }} {{ release }}
  - dist: {{ grains.oscodename }}
  - file: /etc/apt/sources.list.d/cloudstack.list
{%- endif %}
