{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel and rhel < 7 %}
repoforge-release:
  pkg.installed:
  - pkgs:
  - sources:
    - rpmforge-release: http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el{{ rhel }}.rf.x86_64.rpm
  - onchanges_in:
    - pkg: repoforge-refreshed

repoforge-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - rpmforge-release

{%- endif %}
