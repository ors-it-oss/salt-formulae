beats-repo:
  pkgrepo.managed:
  - name: Beats
{% if grains.os == 'CentOS' %}
  - gpgkey: https://packages.elastic.co/GPG-KEY-elasticsearch
  - baseurl: https://packages.elastic.co/beats/yum/el/$basearch
  - gpgcheck: 1
{% elif grains.os_family == 'Debian' %}
  - name: deb https://packages.elastic.co/beats/apt stable main stable main
  - dist: stable
  - key_url: https://packages.elastic.co/GPG-KEY-elasticsearch
  - file: /etc/apt/sources.list.d/beats.list
{% endif %}
