{%- set version = salt['pillar.get']('powerdns:version', '4.0') %}

powerdns-repo:
  pkgrepo.managed:
  - humanname: PowerDNS
  - key_url: salt://{{ slspath }}/keys/powerdns-FD380FBB.asc
{%- if grains.os == 'CentOS' %}
  - baseurl: http://repo.powerdns.com/centos/{{ grains.osarch }}/{{ grains.osmajorrelease }}/auth-{{ version|replace('.') }}
  - gpgcheck: 1
{%- elif grains.os_family == 'Debian' %}
  - name: deb [arch={{ grains.osarch }}] http://repo.powerdns.com/ubuntu {{ grains.oscodename }}-auth-{{ version|replace('.') }} main
  - dist: {{ grains.oscodename }}-auth-{{ version }}
  - file: /etc/apt/sources.list.d/powerdns.list
  - key_url: salt://{{ slspath }}/keys/powerdns-FD380FBB.asc
{%- endif %}
