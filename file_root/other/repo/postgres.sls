{##
  https://www.postgresql.org/download/
  https://download.postgresql.org/pub/repos/yum/9.5/redhat/rhel-7-x86_64/pgdg-redhat95-9.5-3.noarch.rpm
  https://download.postgresql.org/pub/repos/yum/9.6/fedora/fedora-24-x86_64/pgdg-fedora96-9.6-3.noarch.rpm
  https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm

  TODO: Ubuntu/Debian! Fedora.
#}
{%- set rhel = salt.tmpltools.rhel() %}
{%- set base_url = 'https://download.postgresql.org/pub/repos' %}
{%- set release = salt['pillar.get']('postgres:release', '10.0') %}
{%- if rhel %}
{# If it is needed, this should become a map #}
{%-   set rpm_release = 3 %}

{%-   set base_url = base_url ~ '/yum/' ~ release ~ '/redhat/rhel-' ~ rhel ~ '-x86_64' %}
{%-   set rpm_name = 'pgdg-' ~ grains.os.lower() ~ release.replace('.', '') %}
{%-   set rpm = rpm_name ~ '-' ~ release ~ '-' ~ rpm_release ~ '.noarch.rpm' %}
include:
- repo.epel

postgres-release:
  pkg.installed:
  - pkgs:
  - sources:
    - {{ rpm_name }}: {{ base_url }}/{{ rpm }}
  - onchanges_in:
    - pkg: postgres-refreshed

postgres-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - postgres-release
{%- endif %}
