{%- set base_ver = 'stable' %}
{%- set base_url = 'https://packagecloud.io/grafana/' ~ base_ver %}

grafana-repo:
  pkgrepo.managed:
  - humanname: Grafana
  - key_url: {{ base_url }}/gpg.key
{%- if grains.os_family == 'Debian' %}
  - name: deb {{ base_url }}/{{ grains.os|lower }}/ {{ grains.oscodename }} main
  - file: /etc/apt/sources.list.d/grafana.list
{%- endif %}
