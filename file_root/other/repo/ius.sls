{%- set rhel = salt.tmpltools.rhel() %}
{%- if grains.os == 'CentOS' %}
{%-   set ossf = 'centos' %}
{%- else %}
{%    set ossf = 'rhel' %}
{%- endif %}
{%- if rhel %}
ius-release:
  pkg.installed:
  - pkgs:
  - sources:
    - ius-release: https://{{ ossf }}{{ rhel }}.iuscommunity.org/ius-release.rpm
  - onchanges_in:
    - pkg: ius-refreshed

ius-refreshed:
  pkg.uptodate:
  - refresh: True
  - pkgs:
    - ius-release
{%- endif %}
