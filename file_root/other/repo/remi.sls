{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel %}
{%-   set release = salt['pillar.get']('remi:release', '5.6') %}
{%-   set enablerepos = 'remi,remi-php' ~ release|replace ('.','') %}
remi-release:
  pkg.installed:
  - pkgs:
  - sources:
    - remi-release: http://rpms.famillecollet.com/enterprise/remi-release-{{ rhel }}.rpm

yum-config-manager --enable {{ enablerepos }}:
  cmd.wait:
  - watch:
    - pkg: remi-release
{%- endif %}
