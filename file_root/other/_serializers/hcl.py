# -*- coding: utf-8 -*-
'''
salt.serializers.hcl
~~~~~~~~~~~~~~~~~~~~

Implements HCL serializer.

It's just a wrapper around pyhcl module.
'''
from __future__ import absolute_import, print_function, unicode_literals

# Import pyhcl
try:
    import hcl
    available = True
except ImportError:
    available = False

# Import Salt libs
from salt.serializers import DeserializationError, SerializationError

# Import 3rd-party libs
from salt.ext import six

__all__ = ['deserialize', 'serialize', 'available']


def deserialize(stream_or_string, **options):
    '''
    Deserialize from HCL into Python data structure.

    :param stream_or_string: hcl stream or string to deserialize.
    :param options: options given to lower pyhcl module.
    '''

    try:
        if not isinstance(stream_or_string, (bytes, six.string_types)):
            return hcl.load(stream_or_string, **options)

        if isinstance(stream_or_string, bytes):
            stream_or_string = stream_or_string.decode('utf-8')

        return hcl.loads(stream_or_string)
    except Exception as error:
        raise DeserializationError(error)


def serialize(obj, **options):
    '''
    Serialize Python data to HCL.

    :param obj: the data structure to serialize.
    :param options: options given to lower pyhcl module.
    '''

    try:
        if 'file_out' in options:
            return hcl.dump(obj, options['file_out'], **options)
        else:
            return hcl.dumps(obj, **options)
    except Exception as error:
        raise SerializationError(error)
