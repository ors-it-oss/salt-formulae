# -*- coding: utf-8 -*-
'''
Do stuff 
'''
# Import python libs
from __future__ import absolute_import
import logging
import os

# Import salt libs
import salt.utils.files
import salt.utils.minions
import salt.pillar
import salt.config
import salt.runner
import salt.client
import salt.payload
import salt.key
import salt.version
import salt.utils.dictupdate

log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work when 
    '''
    return True


def _ipmi(ipmi_ip=None, ipmi_cidr=None, ipmi_mac=None, ipmi_user=None, ipmi_pass=None):
    ipmi = {}

    return ipmi


def _mine(minion_id):
    '''
    Taken from salt.utils.minions.py
    Gathers the data from the specified minions' mine, pass in the target,
    function to look up and the target type
    '''
    serial = salt.payload.Serial(__opts__)

    mine = os.path.join(
        __opts__['cachedir'],
        'minions',
        minion_id,
        'mine.p')
    try:
        with salt.utils.files.fopen(mine, 'rb') as fp_:
            fdata = serial.load(fp_)
            if fdata:
                return fdata
    except Exception:
        return None


def _pillar(minion_id, grains={}, saltenv='base'):
    '''
    TODO:
    Probably need to fix salt-run pillar.show_pillar
    no git pillars show up, but can't think of a good reason why yet :(
    '''
    pillar_dirs = {}
    pillar = salt.pillar.Pillar(
        __opts__,
        grains,
        minion_id,
        saltenv,
        # ext=load.get('ext'),
        # pillar=load.get('pillar_override', {})
    )
    data = pillar.compile_pillar(pillar_dirs=pillar_dirs)
    # pdata = pillar.compile_pillar()
    return data


def issue_25371():
    # https://github.com/saltstack/salt/issues/25371
    minion_id = 'ceph04.ceph.zone01.ams02.cldin.net'
    sls = 'kakie'
    caller = salt.client.Caller()

    # Works
    res1 = caller.sminion.functions['i7y.report']()

    minion, grains, _ = salt.utils.minions.get_minion_data(minion_id, __opts__)

    pillar = salt.pillar.Pillar(
        __opts__,
        grains,
        minion_id,
        'base',
        # ext=load.get('ext'),
        # pillar=load.get('pillar_override', {})
    )
    pillar = pillar.compile_pillar(pillar_dirs={})

    # Without this, the following call won't have i.e. i7y.report in the module index
    # when running formulas
    # caller = salt.client.Caller()

    # Works
    res2 = caller.sminion.functions['i7y.report']()

    # Breaks complaining i7y.report doesn't exist
    # Rendering SLS 'base:kakie' failed: Jinja variable 'salt.loader.LazyLoader object' has no attribute 'i7y'
    res3 = caller.sminion.functions['state.sls'](sls, pillar={'deploy': 'bullshit'}, concurrent=True)

    return res3


def iso(minion_id, sls, force=True, deploy_data=None, ipmi_ip=None, ipmi_cidr=None, ipmi_mac=None, ipmi_user=None, ipmi_pass=None):
    '''
    Gather all minion data including it's keys and call a state on the master's minion
    :param minion_id: salt minion id
    :param sls: formula to run
    :param force: Create new keys when the old ones can't be reached or when it's a new minion
    :param deploy_data: Additional settings
    :param ipmi_ip:
    :param ipmi_cidr:
    :param ipmi_mac:
    :param ipmi_user:
    :param ipmi_pass:
    :return:
    '''

    # There's not that many usage scenarios involving *all* of these lol
    client = salt.client.LocalClient()
    # runner = salt.runner.RunnerClient(__opts__)
    wheel = salt.wheel.WheelClient(__opts__)
    # caller = salt.client.Caller()

    minion_data = {}

    minion_data['settings'] = {
        'dest': '/mnt/boot',
    }

    # -------------------------------- Determine Salt Bootstrap version --------------------------------
    saltversion = salt.version.__version__
    if '-' in saltversion:
        saltversion = 'git {0}'.format(saltversion[0:6])
    else:
        saltversion = saltversion[0:6]

    # -------------------------------- Add provided deployment data --------------------------------
    if deploy_data is not None:
        minion_data['data'] = deploy_data

    # -------------------------------- Find/add/create keys --------------------------------
    # The Wheel class needs some TLC, .cmd seems broken
    minion_keys = wheel.call_func('key.list_all')

    for queue in ['minion_pre', 'minion_denied', 'minion_rejected']:
        if queue in minion_keys and minion_id in minion_keys[queue]:
            log.warning('{0} found under {1}!'.format(minion_id, queue.split('_')[1]))
            if not force:
                return False

    key = None
    pub = None
    if minion_id in minion_keys['minions']:
        key = client.cmd(minion_id, 'cp.get_file_str', ['/etc/salt/pki/minion/minion.pem'])
        if minion_id not in key or 'BEGIN' not in key[minion_id]:
            key = None
        else:
            key = key[minion_id]
            pub = client.cmd(minion_id, 'cp.get_file_str', ['/etc/salt/pki/minion/minion.pub'])
            if minion_id not in pub or 'BEGIN' not in pub[minion_id]:
                key = pub = None
            else:
                pub = pub[minion_id]
        pass

    if key is None or pub is None:
        if not force:
            log.error('Unable to retrieve key for {0}'.format(minion_id))
            return False
        else:
            log.warning('Generating new keys for {0}'.format(minion_id))
            if pub is not None:
                res = wheel.cmd('key.delete', minion_id)
            newkp = wheel.call_func('key.gen_accept', id_=minion_id, force=True)
            key = newkp.get('priv')
            pub = newkp.get('pub')
            if key is pub is None:
                log.error('Unable to create key for {0}'.format(minion_id))
                return False

    minion_data['runner'] = {
        'mid': minion_id,
        'key': key,
        'pub': pub,
        'saltversion': saltversion
    }

    # -------------------------------- Try to fetch minion cache --------------------------------
    minion, grains, pillar = salt.utils.minions.get_minion_data(minion_id, __opts__)

    if minion != minion_id:
        minion = grains = pillar = None

    if grains is not None:
        minion_data['grains'] = grains

    mine = _mine(minion_id)
    if mine is not None:
        minion_data['mine'] = mine

    # -------------------------------- Compile the Pillar data locally --------------------------------
    # TODO: This runner seems broken for now
    # if pillar is None:
    #     pillar = runner.cmd('pillar.show_pillar', [minion_id])

    # TODO: For some *REALLY* *WEIRD* reason,
    # This will clobber up any subsequent calls to the Caller
    # That's why that one is initialized only later
    # https://github.com/saltstack/salt/issues/25371
    # if pillar is None: (?)
    pillar = _pillar(minion_id)

    if pillar is not None:
        minion_data['pillar'] = pillar

    #
    # if ipmi_ip or (ipmi_cidr and ipmi_mac):
    #     ipmi = _ipmi(ipmi_ip, ipmi_cidr, ipmi_mac, ipmi_user, ipmi_pass)
    #

    # log.debug(minion_data)
    caller = salt.client.Caller()

    # -------------------------------- Prefill FQDN --------------------------------
    fqdn_ip4 = caller.sminion.functions['dnsutil.A'](minion_id)
    if len(fqdn_ip4):
        minion_data['runner']['fqdn_ip4'] = fqdn_ip4
        minion_data['runner']['fqdn'] = minion_id

    fqdn_ip6 = caller.sminion.functions['dnsutil.AAAA'](minion_id)
    if len(fqdn_ip6):
        minion_data['runner']['fqdn_ip6'] = fqdn_ip6
        minion_data['runner']['fqdn'] = minion_id

    # -------------------------------- Call state --------------------------------
    res = caller.sminion.functions['state.sls'](sls, pillar={'deploy': minion_data}, concurrent=True)

    return res
