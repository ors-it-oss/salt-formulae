# -*- coding: utf-8 -*-
'''
Runner for doing stuff to CloudStack
'''
from __future__ import absolute_import

# Import python libs
import logging
import collections
import time

# Import salt libs
# import salt.log
import salt.utils.minions
import salt.loader
#from salt.exceptions import SaltClientError

try:
    import salt.cloud
    import libcloud.compute.drivers.cloudstack
    HAS_SALTCLOUD = True
except ImportError:
    HAS_SALTCLOUD = False

# Set up logging
log = logging.getLogger(__name__)


def __virtual__():
    '''
    Only work when SaltCloud is actually available
    '''
    return HAS_SALTCLOUD is not None


def domain_cmd(domain, fun, arg=None, kwarg=None, provider=None):
    '''
    Track a single customers' VM's and execute a fun on each HV minion
    '''
    __salt__ = salt.loader.minion_mods(__opts__)

    csvms = __salt__['cloudstack.list_vms'](customer=domain, systemvms=False, provider=provider)
    if isinstance(csvms, dict):
        csvms = [csvms]

    vms = collections.defaultdict(dict)
    for csvm in csvms:
        if 'hostid' not in csvm:
            continue
        vm = {
            'instance': csvm['instancename'],
            'display': csvm['displayname'],
            'name': csvm['name'],
            'profile': csvm['serviceofferingname'],
            'state': csvm['state'],
            'ip4': csvm['nic'][0]['ipaddress'],
            'hwaddr': csvm['nic'][0]['macaddress'],  # Technically speaking there might be more
            'traffic': '{0}/{1}'.format(csvm['networkkbsread'], csvm['networkkbswrite']),
            'disk': '{0}/{1}'.format(csvm['diskkbsread'], csvm['diskkbswrite']),
            'io': '{0}/{1}'.format(csvm['diskioread'], csvm['diskiowrite']),
        }

        vms[csvm['hostid']][vm['instance']] = vm

    if not len(vms):
        log.info('Apparently no running VMs found for customer {0}'.format(domain))

    checker = salt.utils.minions.CkMinions(__opts__)
    for cshost in vms.keys():
        # greedy=False?
        minion = checker.check_minions('cloudstack:host:uuid:{0}'.format(cshost), 'pillar')
        if len(minion) != 1:
            log.error('Strange match for {0}: {1}'.format(cshost, minion))
            continue
        vms[minion[0]] = vms[cshost]
        del(vms[cshost])

    client = salt.client.get_local_client(__opts__['conf_file'])
    for res in client.cmd_iter(vms.keys(), fun, arg=[arg], kwarg=kwarg, expr_form='list'):
        for minion, data in res.items():
            vms[minion]['ret'] = data.get('ret', None)

    return vms


def _minion2cshv(tgt, expr_form, client=None):
    if client is None:
        client = salt.client.get_local_client(__opts__['conf_file'])

    cs_minions = {}
    for res in client.cmd_iter(tgt, 'pillar.get', ['cloudstack:host:uuid', None], expr_form=expr_form):
        for minion, data in res.items():
            cs_uuid = data.get('ret', None)
            if cs_uuid is None:
                log.warning('minion {0} has no CloudStack pillar data!'.format(minion))
            else:
                cs_minions[minion] = cs_uuid

    log.debug('Minion matched CloudStack mapped: {0}'.format(cs_minions))
    return cs_minions


def spoof_vms(tgt, expr_form='glob', time=5, provider=None, destroy=False):
    '''
    Find spoofing VM's
    '''
    client = salt.client.get_local_client(__opts__['conf_file'])
    __salt__ = salt.loader.minion_mods(__opts__)
    cs_minions = _minion2cshv(tgt, expr_form, client)

    vms = collections.defaultdict(list)
    for res in client.cmd_iter(cs_minions.keys(), 'cloudstack.spck', expr_form='list'):
        for minion, data in res.items():
            data = data.get('ret', None)
            if data is None or not len(data):
                continue

            csvms = {}
            for csvm in __salt__['cloudstack.list_vms'](host=cs_minions[minion], provider=provider):
                csvms[csvm['instancename']] = csvm

            for mvm, mvmdata in data.items():
                csvm = csvms[mvm]
                vm = {
                    'uuid': csvm['id'],
                    'domain': csvm['domain'],
                    'instance': csvm['instancename'],
                    'display': csvm['displayname'],
                    'name': csvm['name'],
                    'profile': csvm['serviceofferingname'],
                    'state': csvm['state'],
                    'ip4': csvm['nic'][0]['ipaddress'],
                    'hwaddr': mvmdata['hwaddr'],
                    'traffic': '{0}/{1}'.format(csvm['networkkbsread'], csvm['networkkbswrite']),
                    'disk': '{0}/{1}'.format(csvm['diskkbsread'], csvm['diskkbswrite']),
                    'io': '{0}/{1}'.format(csvm['diskioread'], csvm['diskiowrite']),
                    'hits': mvmdata['hits'],
                }
                vms[minion].append(vm)

    for vmlist in vms.values():
        for vm in vmlist:
            domain = vm['domain']
            if domain not in vms:
                vms[domain] = []
            vms[domain].append(vm['display'])

    return vms


def rogue_vms(tgt, expr_form='glob', provider=None, destroy=False):
    '''
    Find rogue VM's
    (VM's that should not be running according to CloudStack)
    '''
    client = salt.client.get_local_client(__opts__['conf_file'])
    __salt__ = salt.loader.minion_mods(__opts__)
    cs_minions = _minion2cshv(tgt, expr_form, client)

    rogues = {}

    # cmd_iter doesn't seem to work with expr_form list?
    for minion, vms in client.cmd(cs_minions.keys(), 'virt.vm_state', expr_form='list').items():
        vms = {vm: state for vm, state in vms.items() if state != 'shutdown'}

        if not len(vms):
            log.warning('Strange return from {0}: {1}'.format(minion, vms))
            continue

        log.debug('Looking for VMs on host {0}'.format(cs_minions[minion]))
        csvms = __salt__['cloudstack.list_vms'](host=cs_minions[minion], systemvms=True, provider=provider)

        if isinstance(csvms, dict):
            csvms = [csvms]
        elif csvms is None:
            mrogue = set(vms.keys())
        else:
            csvms = {vm.get('instancename', vm['name']): vm for vm in csvms}
            log.debug('Comparing {0} to {1}'.format(vms, csvms))
            mrogue = set(vms.keys()) - set(csvms.keys())

        if len(mrogue):
            rogues[minion] = sorted(mrogue)
            log.warning('Found {0} rogue VMs on {1}'.format(mrogue, minion))
            if destroy:
                for vm in rogues[minion]:
                    log.warning('Destroying VM {0} on {1}'.format(vm, minion))
                    # Still broken, probably 'cauz' vm needs to be arg=
                    ret = client.cmd(minion, ['virt.destroy', vm])
                    log.warning(ret)

    return rogues


def hv2maint(tgt, expr_form='glob', provider=None):
    '''
    Stop all VM's on a hypervisor, store them in data first
    :param tgt:
    :param provider:
    :return:
    '''

    # We're probably going to talk to the minion
    client = salt.client.get_local_client(__opts__['conf_file'])
    # Take the data in the caller context to allow easy access to the list
    mopts = salt.config.minion_config('/etc/salt/minion')
    mopts['file_client'] = 'local'
    caller = salt.client.Caller(mopts=mopts)
    __salt__ = caller.sminion.functions

    csuuid = _minion2cshv(tgt, expr_form, client=client)
    if len(csuuid) != 1:
        log.error('Could not match {0} to a CloudStack host Minion'.format(csuuid))
        return None
    else:
        csuuid = csuuid.values()[0]
        log.debug('{0} resolved to CloudStack Host {1}'.format(tgt, csuuid))
    log.debug(csuuid)

#    if __salt__['data.get']('cshv_list', {}).get(csuuid, None) is not None:
#        log.error('List of VMs already found for CS Host {0}, aborting'.format(csuuid))
#        return None

    cloudstack = __salt__['cloudstack.gimme_cloudstack'](provider, block=True)

    # SystemVMs no clue... We won't touch 'em, let's see how CS deals with it
    csvms = __salt__['cloudstack.list_vms'](host=csuuid, systemvms=False, provider=cloudstack)

    uuids = set()
    viids = set()
    for vm in csvms:
        # all vms will die upon calling maintenance
        uuids.add(str(vm['id']))
        viids.add(str(vm['instancename']))

    uuids = list(uuids)
    viids = list(viids)

    log.debug(uuids)
    log.debug(viids)

    __salt__['data.update']('cshv_list', {csuuid: {'uuid': uuids, 'instance': viids}})

    cluster = __salt__['cloudstack.list_hosts'](host=csuuid, provider=cloudstack)['clusterid']
    log.warning('CLUSTER: {}'.format(cluster))
    res = cloudstack.call('updateCluster', {'id': cluster, 'managedstate': 'Unmanaged'})
    log.warning(res)
    # Stoopid HA worker will HA even if we disable HA. Or sumtin
#    for vmuuid in uuids:
#        log.warning('Stopping instance {0}...'.format(vmuuid))
#        res = cloudstack.call('stopVirtualMachine', {'id': vmuuid})
#        log.debug('Instance {0} stopped: {1}'.format(vmuuid, res))
    jobs = []
    for vmid in viids:
         log.warning('Calling stop on instance {0}...'.format(vmid))
         res = client.cmd_async(tgt, 'virt.shutdown', arg=[vmid], expr_form=expr_form)
         jobs.append(res)
         time.sleep(1)

    log.warning('Wait for jobs {}'.format(jobs))


   # res = __salt__['cloudstack.set_host_state'](csuuid, 'maintenance', cloudstack)
    log.warning(res)


def hv2prod(tgt, expr_form='glob', provider=None):
    '''
    Exit maintenance and restore VM's
    :param tgt:
    :param expr_form:
    :param provider:
    :return:
    '''
    # We're probably going to talk to the minion
    client = salt.client.get_local_client(__opts__['conf_file'])
    # Take the data in the caller context to allow easy access to the list
    mopts = salt.config.minion_config('/etc/salt/minion')
    mopts['file_client'] = 'local'
    caller = salt.client.Caller(mopts=mopts)
    __salt__ = caller.sminion.functions

    csuuid = _minion2cshv(tgt, expr_form, client=client)
    if len(csuuid) != 1:
        log.error('Could not match {0} to a CloudStack host Minion'.format(csuuid))
        return None
    else:
        csuuid = csuuid.values()[0]
        log.debug('{0} resolved to CloudStack Host {1}'.format(tgt, csuuid))
    log.debug(csuuid)

    cloudstack = __salt__['cloudstack.gimme_cloudstack'](provider, block=True)

    __salt__['cloudstack.set_host_state'](csuuid, 'enabled', cloudstack)

    res = client.cmd('cloudstack:host:uuid:{0}'.format(csuuid), 'service.restart', ['cloudstack-agent'], expr_form='pillar')
    log.warning(res)

    cluster = __salt__['cloudstack.list_hosts'](host=csuuid, provider=cloudstack)['clusterid']
    log.warning(cluster)
    res = cloudstack.call('updateCluster', {'id': cluster, 'managedstate': 'Managed'})
    log.warning(res)

    restvms = __salt__['data.get']('cshv_list', {}).get(csuuid, None)
    if restvms is None:
        log.warning('No cshv_list for {0} found in data :/'.format(csuuid))
    else:
        for vmuuid in restvms['uuid']:
            log.warning('Starting instance {0}...'.format(vmuuid))
            res = cloudstack.call('startVirtualMachine', {'id': vmuuid, 'hostid': csuuid})
            log.debug('Instance {0} started: {1}'.format(vmuuid, res))

    #should be started now


def kick_blocked(host=None, cluster=None, pod=None):
    pinger = 'ssvm01.ssvm.p02.zone01.ams02.cldin.net'
    # We're probably going to talk to the minion
    client = salt.client.get_local_client(__opts__['conf_file'])

    # Take the data in the caller context to allow easy access to the list
    mopts = salt.config.minion_config('/etc/salt/minion')
    mopts['file_client'] = 'local'
    caller = salt.client.Caller(mopts=mopts)
    __salt__ = caller.sminion.functions
    cloudstack = __salt__['cloudstack.gimme_cloudstack'](None, block=True)

    for instance in __salt__['cloudstack.list_vms'](host=host, cluster=cluster, pod=pod, provider=cloudstack):
        instance_name = instance['instancename']
        instance_id = instance['id']
        instance_ip = instance['nic'][0]['ipaddress']
        log.warning('Checking instance {} ({})'.format(instance_name, instance_ip))
        if not client.cmd(pinger, 'network.ping', arg=[instance_ip], kwarg={'timeout': 2, 'return_boolean': True}).values()[0]:
            log.warning('Restarting instance {} due to no connectivity...'.format(instance_name))
            res = cloudstack.call('stopVirtualMachine', {'id': instance_id})
            log.warning(res)

            res = cloudstack.call('startVirtualMachine', {'id': instance_id})
            log.warning(res)
