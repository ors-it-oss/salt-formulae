{%- if data.get('act', '') == 'pend' and data.id == opts.id and data.pub == salt.cp.get_file_str('/etc/salt/pki/minion/minion.pem') %}
minion_add:
  wheel.key.accept:
  - match: {{ data.id }}
{%- endif %}
