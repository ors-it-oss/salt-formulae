#!py
# -*- coding: utf-8 -*-
'''
Send an audit msg for most/certain jobs/states
TODO: it might be bettered:
 - Thorium? https://docs.saltstack.com/en/latest/topics/thorium/index.html
 - or some (custom) engine
 - Output module in 2018.3+: https://docs.saltstack.com/en/develop/ref/modules/all/salt.modules.out.html #}
'''
blacklist = {
  'fun': (
    'cloudstack.infradata',
    'data.get',
    'data.items',
    'grains.get',
    'grains.items',
    'i7y.',
    'network.interfaces',
    'pillar.get'
    'pillar.items',
    'saltutil.',
    'service.status',
    'state.show_',
    'test.',
  ),
  'state': (
    'salt.minion',
  )
}


def run():
    '''
      Renderer is #!py
    '''
    slack = __opts__['slack']

    # ------------ MODULE/FUNCTION CALLED ---------------- #
    mod_fun = data['fun']
    module, fun = mod_fun.split('.', 2)
    if mod_fun in ('state.sls', 'state.apply', 'state.single', 'state.highstate'):
        audit_type = 'state'
    else:
        audit_type = 'module'
    ref = 'https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.{module}.html#salt.modules.{mod_fun}'
    ref = ref.format(module=module, mod_fun=mod_fun)

    # ------------ MATCHER ---------------- #
    target_type = data['tgt_type']
    target = data['tgt']
    if target != 'glob':
        if target == 'compound':
            target = ' '.join(target)
        target = '`{0}`-matching `{1}`'.format(target_type, target)

    args = []
    for arg in data['arg']:
        try:
            if arg.pop('__kwarg__', False):
                for kwarg, val in data.items():
                    args.append('{}={}'.format(kwarg,val))
        except AttributeError:
            args.append(arg)

    # ------------ PICK & PREP MSG ---------------- #
    msg = None
    if audit_type == 'module':
        if not mod_fun.startswith(blacklist['fun']):
            msg = 'Calling `<{ref}|{mod_fun}>{args}` on {target}'
    elif audit_type == 'state':
        if fun == 'highstate':
            msg = 'Highstating {target}'
        elif not args[0].startswith(blacklist['state']):
            msg = 'Applying `{state}` to {{target}}'.format(state=args[0])
    if not msg:
        return {}
    else:
        if args:
          args.insert(0,'')
          args = ' '.join(args)
        else:
          args = ''
        msg = '_' + msg.format(ref=ref, mod_fun=mod_fun, args=args, target=target) + '_'

    # ------------ COMPOSE JOB ---------------- #
    job = {
      'audit_new_job': {
        'caller.slack.post_message': [{'args': [
          {'channel': slack['channel']},
          {'message': msg},
          {'from_name': slack['from_name']},
          {'api_key': slack['api_key']},
          {'icon': slack['icon']}
        ]}]
      }
    }
    return job
