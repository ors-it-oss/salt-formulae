{#- specifically for RHEL6 which doesn't have this one by default #}
kernel-sysctl:
  file.directory:
  - name: /etc/sysctl.d
  - user: root
  - group: root
  - mode: 755
  - makedirs: True

{##-
VM tunes; Start flushing out the buffers to the disks sooner. Prevents spikes on the disks
TODO: Verify these!
https://lonesysadmin.net/2013/12/22/better-linux-disk-caching-performance-vm-dirty_ratio/
#}
kernel-tunes:
  sysctl.present:
  - names:
    - vm.swappiness:
      - value: 5
    - vm.dirty_ratio:
      - value: 20
    - vm.dirty_background_ratio:
      - value: 5
{%- for name, val in salt['pillar.get']('kernel:tune', {}).items() %}
    - {{ name }}:
      - value: {{ val }}
{%- endfor %}
