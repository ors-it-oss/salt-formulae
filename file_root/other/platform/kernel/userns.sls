{#- http://rhelblog.redhat.com/2015/07/07/whats-next-for-containers-user-namespaces/#comment-6796 #}
{%- if grains.os_family == 'RedHat' %}
{%-   if salt.tmpltools.rhel() %}
userns-boot:
  file.blockreplace:
  - name: /etc/default/grub
  - marker_start: "# BEGIN CONTAINER USER NAMESPACING"
  - marker_end: "# END CONTAINER USER NAMESPACING"
  - content: |
      GRUB_CMDLINE_LINUX_DEFAULT="$GRUB_CMDLINE_LINUX_DEFAULT user_namespace.enable=1"
  - show_changes: True
  - append_if_not_found: True
  cmd.wait:
  - order: last
  - name: grub2-mkconfig > /boot/grub2/grub.cfg
  - onchanges:
    - file: userns-boot
{%-   endif %}

{#- usermod in RHEL isn't adapted to subuid's/subgid's yet so user creation fails, copied from Ubuntu #}
userns-subuids:
  file.append:
  - name: /etc/subuid
  - text: cntremap:165536:65536

userns-subgids:
  file.append:
  - name: /etc/subgid
  - text: cntremap:165536:65536

userns-remapper:
  group.present:
  - name: cntremap
  - system: True
  user.present:
  - name: cntremap
  - fullname: Container UID Remapper for User Namespacing
  - shell: /bin/false
  - home: /home/cntremap
  - gid_from_name: True
  - createhome: False
  - system: True
{%- endif  %}
