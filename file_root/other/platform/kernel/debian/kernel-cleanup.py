#!/usr/bin/python
# -*- coding: utf-8 -*-
'''

'''
# Python
import argparse
import subprocess
import sys
import re

# Logging & debugging
import logging
#import pprint
#ppr = pprint.PrettyPrinter(indent=2).pprint

logging.basicConfig(
    level=logging.INFO,
    format='%(levelname)s %(message)s',
    handlers=[logging.StreamHandler(sys.stdout)]
)
log = logging.getLogger(__name__)

BLACKLIST = 'linux-base', 'linux-firmware', 'linux-libc-dev'


def run(*cmd):
    cmd = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = cmd.communicate()
    if cmd.returncode != 0:
        raise OSError(stderr)
    return stdout, stderr


def pkg_ver(pkg_name):
    try:
        version = re.search(r'[0-9]+\.[0-9]+\.[0-9]+.*$', pkg_name).group(0)
    except AttributeError:
        raise ValueError('No kernel version in pkg')

    version = tuple(int(i) for i in re.split(r'[^0-9]+', version) if i)
    log.debug('%s numeric version: %s', pkg_name.strip(), ','.join(str(v) for v in version))
    return version


def pkg_list():
    dpkg_q, _ = run('dpkg-query', '-W', '--showformat', '${Package}/${Status}/${Depends}\n', 'linux-*')

    images = set()
    pkgs = {}
    all_deps = {}
    for line in dpkg_q.splitlines():
        name, status, deps = line.split('/')
        if 'not-installed' in status or name in BLACKLIST:
            continue

        # Deconstruct dependencies by stripping out version specs & alternates
        ddeps = []
        for dep_def in deps.split(', '):
            for dep in dep_def.split(' | '):
                dep = dep.split(' (')[0]
                if dep not in BLACKLIST:
                    ddeps.append(dep)
        all_deps[name] = ddeps

        try:
            version = pkg_ver(name)
        except ValueError:
            continue

        if re.search(r'image-[0-9]+\.', name):
            images.add(version)
        if version not in pkgs:
            pkgs[version] = []

        pkgs[version].append(name)

    res = {
        'kernels': images,
        'pkgs': pkgs,
        'deps': all_deps
    }
    log.debug(res)
    return res


def purgeable_pkgs(kernels, pkgs, deps, retention):
    current = pkg_ver(
        run('uname', '-r')[0]
    )

    whitelist = set()
    if len(kernels) <= retention:
        whitelist.update(kernels)
    else:
        whitelist.add(current)
        whitelist.update(sorted(kernels)[-retention:])

    purge_pkgs = []
    for version, installed in pkgs.items():
        if version in whitelist:
            continue
        purge_pkgs.extend(installed)

    # Resolve dependencies
    # Pkgs that are under selection and depend on the purgeables
    # are also eligible for annihilation
    def dependants(pkg):
        rdeps = [pkg_name for pkg_name, pkg_deps in deps.items() if pkg in pkg_deps]
        for pkg in rdeps:
            rdeps.extend(dependants(pkg))
        return rdeps

    for pkg in purge_pkgs:
        rev_deps = dependants(pkg)
        if rev_deps:
            log.debug('%s forces removal of %s', pkg, ', '.join(set(rev_deps)))
            purge_pkgs.extend(rev_deps)

    return list(set(purge_pkgs))


def grub_hook(enable=True):
    '''
    TODO:
    disablegrubhook() {
  [ -d /etc/kernel.bak ] || mkdir /etc/kernel.bak
  [ -f /etc/kernel/postrm.d/zz-update-grub ] && mv -f /etc/kernel/postrm.d/zz-update-grub /etc/kernel.bak/zz-update-grub-postrm
  [ -f /etc/kernel/postinst.d/zz-update-grub ] && mv -f /etc/kernel/postinst.d/zz-update-grub /etc/kernel.bak/zz-update-grub-postinst
  [ -f /etc/kernel-img.conf ] && mv -f /etc/kernel-img.conf /etc/kernel.bak && sed 's/^\(postrm_hook   = update-grub\)$/#\1/' /etc/kernel.bak/kernel-img.conf > /etc/kernel-img.conf
}

enablegrubhook() {
  [ -f /etc/kernel.bak/zz-update-grub-postrm -a ! -f /etc/kernel/postrm.d/zz-update-grub ] && mv /etc/kernel.bak/zz-update-grub-postrm /etc/kernel/postrm.d
  [ -f /etc/kernel.bak/zz-update-grub-postinst -a ! -f /etc/kernel/postinst.d/zz-update-grub ] && mv /etc/kernel.bak/zz-update-grub-postinst /etc/kernel/postinst.d
  [ -f /etc/kernel.bak/kernel-img.conf ] && mv -f /etc/kernel.bak/kernel-img.conf /etc/kernel-img.conf
  rm -fr /etc/kernel.bak
}

    which does nuttin' on Xenial, btw 
    
    :return: 
    '''
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Ubuntu/Debian kernel scrubber',
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-r', '--retention', dest='retention', type=int, default=3, action='store',
                        help='Kernels to keep (default %(default)s)')
    parser.add_argument('-n', '--no-i-am-not-so-sure-quite-yet', dest='dry', action='store_true')
    args = parser.parse_args()

    pkgl = pkg_list()
    purge = purgeable_pkgs(
        retention=args.retention,
        **pkgl
    )
    if not purge:
        log.info('No kernel packages to scrub')
        sys.exit(0)
    elif args.dry:
        log.info('Want to purge {}'.format(', '.join(purge)))
        sys.exit(0)

    log.info('Purging '.format(', '.join(purge)))
    run('dpkg', '-P', *purge)
