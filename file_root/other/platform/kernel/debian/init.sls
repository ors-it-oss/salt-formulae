{##
  So dig this story (or at least Ronald's interpretation of it):
 https://wiki.ubuntu.com/1204_HWE_EOL, https://wiki.ubuntu.com/Kernel/LTSEnablementStack, https://wiki.ubuntu.com/Kernel/Support
 IF you're on LTS point release 0 or 1, on a VM, you strongly advised to stick with stock kernel
 In all other cases, only latest LTS kernel is supported

 And then they started doing hwe(-edge) etc. :(
#}

{%- set kernel = salt['pillar.get']('kernel:release', False) %}
{%- if kernel %}
{%-   if kernel == 'latest' %}
{%-     set name = salt.ubuntu_linux.latest_lts() %}
{%-   else %}
{%-     set name = 'linux-' ~ kernel %}
{%-   endif %}
kernel-release:
  pkg.installed:
  - name: {{ name }}
{%- endif %}

kernel-cleanup-script:
  file.managed:
  - name: /usr/local/sbin/kernel-cleanup
  - source: salt://{{ slspath }}/kernel-cleanup.py
  - user: root
  - group: root
  - mode: 755

kernel-cleanup-hook:
  file.managed:
  - name: /etc/apt/apt.conf.d/98-kernel-cleanup
  - template: jinja
  - source: salt://{{ slspath }}/kernel-cleanup_dpkg-hook.jinja
  - user: root
  - group: root
  - mode: 644
