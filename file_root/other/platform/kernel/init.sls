{%- from 'shared.jinja' import list_if, list_if_pillar %}
{%- set rhel = salt.tmpltools.rhel() %}
include:
{{ list_if(grains.kernel == 'Linux', '.linux') }}
{{ list_if(grains.os_family == 'Debian', '.debian') }}
{{ list_if(rhel, '.rhel') }}
{{ list_if_pillar('kernel:crash', '.crashdumps') }}

# TODO: !!! https://docs.saltstack.com/en/develop/ref/modules/all/salt.modules.kernelpkg.html
