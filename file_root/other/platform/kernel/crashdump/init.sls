include:
- platform.hw.grub2

{%- if grains.os_family == 'Debian' %}
linux-crashdump-tools:
  pkg.installed: 
  - name:
  - pkgs:
    - linux-crashdump
  - watch_in:
    - cmd: update-grub2

crashdump-config:
  file.managed:
  - name: /etc/default/kdump-tools
  - source: salt://{{ slspath }}/kdump-tools.jinja
  - template: jinja
  - mode: 0644
  - user: root
  - group: root
  - watch_in:
    - cmd: update-grub2
{%- endif %}
