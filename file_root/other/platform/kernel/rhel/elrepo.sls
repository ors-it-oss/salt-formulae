{## YES YOU NEED TO CHECK STRINGS FOR ENABLED=! AND YOU NEED TO FILL KWARGS TO MOD! #}
{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel -%}
include:
  - repo.elrepo

{%- set release = salt['pillar.get']('kernel:release', 'elrepo-ml') %}
{%- if '-' in release %}
{%-   set name = release.split('-')[-1] %}
{%- else %}
{%-   set name = 'ml' %}
{% endif %}

{%-   for repo_id, repo_data in salt.pkg.list_repos().items() -%}
{%-     if repo_id.startswith('elrepo') -%}
{%-       if 'kernel' in repo_id -%}
{%          if repo_data.enabled == '0'  %}

repo-{{ repo_id }}-enable:
  module.run:
  - name: pkg.mod_repo
  - repo: {{ repo_id }}
  - kwargs:
      enabled: 1
  - onchanges_in:
    - pkg: elrepo-refreshed
  - require_in:
    - pkg: kernel-elrepo-{{ name }}
{%-         endif %}
{%        elif repo_data.enabled == '1' %}
repo-{{ repo_id }}-disable:
  module.run:
  - name: pkg.mod_repo
  - repo: {{ repo_id }}
  - kwargs:
      enabled: 0
  - onchanges_in:
    - pkg: elrepo-refreshed
{%-       endif -%}
{%-     endif -%}
{%-   endfor -%}

kernel-switch-tools:
  pkg.removed:
  - pkgs:
      - kernel-tools-libs
  - require_in:
      - pkg: kernel-elrepo-{{ name }}

kernel-elrepo-{{ name }}:
  pkg.installed:
  - pkgs:
    - kernel-{{ name }}
    - kernel-{{ name }}-tools

{#- Determine which kernel release packages to cleanup. Skip running kernel. #}
{%- set kernels = salt.pkg_resource.version('kernel*', versions_as_list=True) %}

{# Dirty as f*ck, needs to be fixed #}
{%- set running = ['dummy'] %}
{%- set kernels = salt.pkg_resource.version('kernel*', versions_as_list=True) %}
{%- for release, installed in kernels.items() %}
{%-   for kernel in installed %}
{%-     if kernel in grains.kernelrelease %}
{%-       do running.insert(0,kernel) %}
{%-     endif %}
{%-   endfor %}
{%- endfor %}
{%- set running = running[0] %}


{## Let's not purge for now
{%- set purge = []  %}
{%- for kernel in kernels %}
{%-   if kernel != running and kernel != 'kernel-' ~ name %}
{%-     do purge.append(kernel) %}
{%-   endif %}
{%- endfor %}
{%- if purge %}
kernel-purged:
  pkg.removed:
    - pkgs: {{ purge }}
    - onchanges_in:
      - cmd: update-boot
{%- endif %}
#}

update-boot:
  cmd.run:
  - name: grub2-set-default 0; grub2-mkconfig -o /boot/grub2/grub.cfg
  - output_loglevel: quiet
  - onchanges:
      - pkg: kernel-elrepo-{{ name }}

{%- endif %}
