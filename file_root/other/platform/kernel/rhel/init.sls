{%- set kernel = salt['pillar.get']('kernel:release', False) %}
{%- if kernel in ('latest', 'elrepo') %}
include:
- .elrepo
{%- endif %}

{# TODO: Cleanup kernels of older releases #}

{# This is dirty as it affects more packages than only kernel-* #}
{%- set retains = salt['pillar.get']('kernel:retain', 3) %}
kernel-keep-{{ retains }}:
  file.replace:
  - name: /etc/yum.conf
  - pattern: ^\s*installonly_limit\s*:.*
  - repl: 'installonly_limit={{ retains }}'
  - backup: False
