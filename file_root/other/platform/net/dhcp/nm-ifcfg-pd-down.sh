#!/bin/sh
#
# DHCPv6 PD dispatcher for NetworkManager on Red Hat
#
# NetworkManager is needed to do proper SLAAC combined with DNS servers
# It can also pick up on DHCPv6 client but the two are mutually exclusive
# and DHCPV6_OPTIONS aren't parsed through
# NetworkManager can therefore not provide PD configurations.
#
# Most of this is shamelessly ripped & modified from /etc/sysconfig/network-scripts/{network-functions, ifup-eth}
#

#set -x

dev=$1
op=$2

# Include the SysConfig script vars
. $CONNECTION_FILENAME

[[ "${DHCPV6C}" = [Yy1]* ]] && echo "${DHCPV6C_OPTIONS}" | grep -q "\-P" && [ -x /sbin/dhclient ] || exit 0

lease_file="/var/lib/NetworkManager/dhclient6-${CONNECTION_UUID}-${DEVICE}.lease"
pid_file="/var/run/dhclient6-${DEVICE}.pid"

if [ -s "/var/lib/NetworkManager/dhclient6-${DEVICE}.conf" ]; then
    DHCLIENTCONF="-cf /var/lib/NetworkManager/dhclient6-${DEVICE}.conf"
else
    # Include the script functions
    . /etc/sysconfig/network-scripts/network-functions
    generate_config_file_name 6
fi

#[ -f "$pid_file" ] || exit 0
#pkill $(cat "$pid_file")
#rm -f "$pid_file" "$lease_file" 2>/dev/null
/sbin/dhclient -6 -x ${DHCLIENTCONF} -lf ${lease_file} -pf ${pid_file} ${DEVICE}
