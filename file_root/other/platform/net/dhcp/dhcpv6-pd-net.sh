#!/bin/sh
#
# DHCPv6 hook for fixxing & keeping net connectivity
#
# This hook will detect delegated prefixes
# When it's detected,
# * enable IPv6 Forwarding on the interface
# * if SLAAC is enabled, force it to keep enabled
#

[ -n "$new_ip6_prefix" ] || return

# Enable forwarding
sysctl -w net.ipv6.conf.${interface}.forwarding=1

# If Routing Advertisements should be accepted, set it to 2
# (so that they keep getting accepted after forwarding is enabled)
if [ $(sysctl -n net.ipv6.conf.${interface}.accept_ra) = 1 ]; then
    sysctl -w net.ipv6.conf.${interface}.accept_ra=2
fi
