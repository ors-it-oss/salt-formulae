#!/bin/sh
#
# DHCPv6 PD dispatcher for NetworkManager on Red Hat
#
# NetworkManager is needed to do proper SLAAC combined with DNS servers
# It can also pick up on DHCPv6 client but the two are mutually exclusive
# and DHCPV6_OPTIONS aren't parsed through
# NetworkManager can therefore not provide PD configurations.
#
# Most of this is shamelessly ripped & modified from /etc/sysconfig/network-scripts/{network-functions, ifup-eth}
#

#set -x

dev=$1
op=$2

# Include the SysConfig script vars
. $CONNECTION_FILENAME

[[ "$op" = "up" && "${DHCPV6C}" = [Yy1]* ]] && echo "${DHCPV6C_OPTIONS}" | grep -q "\-P" && [ -x /sbin/dhclient ] || exit 0

lease_file="/var/lib/NetworkManager/dhclient6-${CONNECTION_UUID}-${DEVICE}.lease"
pid_file="/var/run/dhclient6-${DEVICE}.pid"

if [ -s "/var/lib/NetworkManager/dhclient6-${DEVICE}.conf" ]; then
    DHCLIENTCONF="-cf /var/lib/NetworkManager/dhclient6-${DEVICE}.conf"
else
    # Include the script functions
    . /etc/sysconfig/network-scripts/network-functions
    generate_config_file_name 6
fi

# Wait for SLAAC to complete
# TODO: DHCLIENT_DELAY from config?
sleep 5

# NetworkManager usually runs nm-dhcp-helper
# Which does nothing more than send a D-BUS alert AFAICT
# There's an extremely good chance you'll need the hooks scripts
# which NM on RH chooses to ignore (which sux ballz)
#-sf /usr/libexec/nm-dhcp-helper
/sbin/dhclient -6 -1 ${DHCPV6C_OPTIONS} ${DHCLIENTCONF} -lf ${lease_file} -pf ${pid_file} -H ${DHCP_HOSTNAME:-${HOSTNAME%%.*}} ${DEVICE}
