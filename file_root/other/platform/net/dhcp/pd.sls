{%- if grains.os_family == 'RedHat' %}
dhcp6-pd-nm-hook-up:
  file.managed:
  - name: /etc/NetworkManager/dispatcher.d/30-dhcpv6-pd
  - source: salt://{{ slspath }}/nm-ifcfg-pd-up.sh
  - mode: 755
  - user: root
  - group: root

dhcp6-pd-nm-hook-down:
  file.managed:
  - name: /etc/NetworkManager/dispatcher.d/pre-down.d/30-dhcpv6-pd
  - source: salt://{{ slspath }}/nm-ifcfg-pd-down.sh
  - mode: 755
  - user: root
  - group: root

{%-   set nethook = 'dhcpv6-pd-net.sh' %}
{%- elif grains.os_family == 'Debian' %}
{%-   set nethook = 'dhcpv6-pd-net' %}
{%- endif %}

dhcp6-pd-hook-enter:
  file.managed:
  - name: /etc/dhcp/dhclient-enter-hooks.d/{{ nethook }}
  - source: salt://{{ slspath }}/dhcpv6-pd-net.sh
  - mode: 755
  - user: root
  - group: root
