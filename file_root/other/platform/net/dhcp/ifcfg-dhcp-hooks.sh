#
# Make the DHCP hooks directories
# Why RH doesn't do this by default is way beyond me
#
stage={{ stage }}

stage="${ETCDIR}/dhclient-${stage}-hooks.d"

[ -d "$stage" ] || return

IFS='
'
for f in `find $stage -name '*.sh'`; do
    . ${f}
done
