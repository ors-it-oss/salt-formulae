{%- if grains.os_family == 'RedHat' %}
{%-    for stage in ('enter', 'exit', 'up', 'down') %}
dhcp-{{ stage }}-hooks.d:
  file.directory:
  - name: /etc/dhcp/dhclient-{{ stage }}-hooks.d
  - user: root
  - group: root
  - mode: 755

dhcp-{{ stage }}-hooks:
  file.managed:
  - name: /etc/dhcp/dhclient-{{ stage }}-hooks
  - user: root
  - group: root
  - template: jinja
  - source: salt://{{ slspath }}/ifcfg-dhcp-hooks.sh
  - context:
      stage: {{ stage }}
  - mode: 755
{%-   endfor %}
{%- endif %}
