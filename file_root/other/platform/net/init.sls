{%- from 'shared.jinja' import list_if %}
include:
{{ list_if(grains.virtual == 'physical', 'platform.hw.baremetal.net') }}
- .host
- .ipv6
{{ list_if(grains.os_family == 'RedHat', '.dhcp') }}
- .resolv
#  - .intf
