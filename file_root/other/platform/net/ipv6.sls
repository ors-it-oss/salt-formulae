{#- Disable IPv6 Privacy Extensions #}
{%- if grains.os == 'Ubuntu' %}
{#- UBUNTU SHITS ITS PANTS IF YOU TRY TO DO THIS LIVE #}
net-ipv6-no-privacy:
  file.managed:
  - name: /etc/sysctl.d/10-ipv6-privacy.conf
  - contents: |
      # IPv6 Privacy Extensions (RFC 4941)
      # ---
      # IPv6 typically uses a device's MAC address when choosing an IPv6 address
      # to use in autoconfiguration. Privacy extensions allow using a randomly
      # generated IPv6 address, which increases privacy.
      #
      # Acceptable values:
      #    0 - don't use privacy extensions.
      #    1 - generate privacy addresses.
      #    2 - prefer privacy addresses and use them over the normal addresses.
      net.ipv6.conf.all.use_tempaddr = 0
      net.ipv6.conf.default.use_tempaddr = 0
{%- else %}
include:
- platform.kernel.linux

net-ipv6-no-privacy:
  sysctl.present:
  - names:
    - net.ipv6.conf.all.use_tempaddr
    - net.ipv6.conf.default.use_tempaddr
  - value: 0
  - config: /etc/sysctl.d/10-ipv6-privacy.conf
{%- endif %}

{%- if grains.os == 'Fedora' %}
net-ipv6-NetworkManager-force-eui64:
  cmd.run:
  - name: nmcli con modify 'Wired connection 1' ipv6.addr-gen-mode eui64
  - unless: nmcli -t -f ipv6.addr-gen-mode con show 'Wired connection 1' | grep -q eui64
{%- endif %}

{% if salt.file.directory_exists('/etc/NetworkManager/dispatcher.d/pre-up.d') %}
net-ipv6-NetworkManager-ignore-disables:
  file.managed:
  - name: /etc/NetworkManager/dispatcher.d/pre-up.d/00-ignore-disables-ipv6.sh
  - mode: 755
  - contents: |
      #!/bin/sh
      #if connection ipv6.method == ignore, we actually mean *DISABLE*
      if nmcli -t -f ipv6.method connection show $CONNECTION_ID | grep -q ignore; then
          echo 1 >/proc/sys/net/ipv6/conf/${DEVICE_IP_IFACE}/disable_ipv6 DEVICE_IP_IFACE
      fi
{%- endif %}

{%- if grains.kernel == 'Linux' %}
net-ipv6-veth-disable-linklocal:
  file.managed:
  - name: /etc/udev/rules.d/80-veth-no-linklocal.rules
  - user: root
  - group: root
  - source: salt://{{ slspath }}/udev_veth-no-linklocal.rules
  - mode: 644
{%- endif %}
