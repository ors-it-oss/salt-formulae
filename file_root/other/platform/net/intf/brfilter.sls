{%- set sysfile = '/etc/sysctl.d/99-brfilter.conf' %}
  {#- Enable netfiltering on inter-bridge frames #}
{%- if salt.sysctl.show().keys()|intersect((
      'net.bridge.bridge-nf-call-iptables',
      'net.bridge.bridge-nf-call-ip6tables'
))|length == 2 %}
net-bridge-netfilter:
  sysctl.present:
  - config: {{ sysfile }}
  - names:
    - net.bridge.bridge-nf-call-iptables:
      - value: 1
    - net.bridge.bridge-nf-call-ip6tables:
      - value: 1
{%- endif %}
