{%- set net = pillar.net %}

{%- if 'tmpl' in net %}
net-intf:
  file.managed:
  - name: /etc/network/interfaces
  - template: jinja
  - source: salt://{{ net.tmpl }}
  - context: {{ net }}
  - defaults:
      mtu: 1500
{%- endif %}
