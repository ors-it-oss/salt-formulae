{##-
  Mind:
    - https://github.com/saltstack/salt/issues/6922
-#}
{%- set pillar_name = salt['pillar.get']('dns:fqdn', '') %}
{%- if '.' in pillar_name %}
{%-   set host_name = pillar_name %}
{%- else %}
{%-   set host_name = salt.grains.get(pillar_name, opts.id) %}
{%- endif %}

{#- ---------------- Set hostname ---------------- -#}
{%- if salt.network.get_hostname() != host_name or salt.cp.get_file_str('/etc/hostname') != host_name ~ '\n' %}
system-hostname:
  module.run:
  - name: network.mod_hostname {#- yugh; mod_??? #}
  - hostname: {{ host_name }}
{%- endif %}

{%- set host_ips = [] %}
{%- set all_ips = salt.tmpltools.deepjoin((
      grains.get('fqdn_ip4', 'empty'),
      grains.get('fqdn_ip6', 'empty'),
      salt.dnsutil.A(host_name),
      salt.dnsutil.AAAA(host_name)
    ))|reject('string')|unique|list %}
{%- for ip in all_ips %}
{%-   if not salt.network.is_loopback(ip) %}
{%-     do host_ips.append(ip) %}
{%-   endif %}
{%- endfor %}
{%- if host_ips %}
{#- ---------------- 'ENABLE' networking ---------------- -#}
{%-   if grains.os_family == 'Debian' %}
sysnet-workaround-6922:
  file.append:
  - name: /etc/default/networking
  - makedirs: True
  - text: CONFIGURE_INTERFACES=yes
{%-   elif grains.os_family == 'RedHat' %}
sysnet-workaround-6922:
  file.append:
  - name: /etc/sysconfig/network
  - makedirs: True
  - text: NETWORKING=yes
{%-   endif %}
{%- endif %}

{#- ---------------- Fix hosts file ---------------- -#}
{%- if '.' in host_name %}
{%-   set host_name = [host_name, host_name.split('.')[0]] %}
{%- else %}
{%-   set host_name = [host_name] %}
{%- endif %}
{%- for host in host_name %}
{%-   if host_ips %}
{{ host }}-hosts-present:
  host.present:
  - name: {{ host }}
  - ip: {{ host_ips }}
{%-   endif %}

{{ host }}-hosts-absent:
  host.absent:
  - name: {{ host }}
  - ip:
    - 127.0.0.1
    - 127.0.1.1
    - ::1
{%- endfor %}
