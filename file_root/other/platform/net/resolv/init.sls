{%- from 'shared.jinja' import list_if_pillar %}
{%- set resolvconf = salt.file.file_exists('/sbin/resolvconf') and not salt.file.file_exists('/usr/bin/resolvectl') %}
{%- set nameservers = salt['pillar.get']('dns:nameservers', []) %}
{%- if 'local' in nameservers %}
resolv-noslaac:
  pkg.removed:
  - pkgs:
    - rdnssd
    - resolvconf

include:
- unbound.forwarder
{{ list_if_pillar('dns:hosts', '.hosts') }}

{%- else %}
{%-   if salt['pillar.get']('dns:hosts', False) %}
include:
- .hosts
{%-   endif %}

resolv-slaac:
  pkg.installed:
{%-   if grains.os_family == 'RedHat' %}
  - name: ndisc6
{%-   elif grains.os_family == 'Debian' %}
  - pkgs:
    - rdnssd
    - resolvconf
{%-   endif %}

{%-   if resolvconf %}
{#- prefer IPv6 advertised DNS servers #}
  file.prepend:
  - name: /etc/resolvconf/interface-order
  - text:
    - 000.*
{%-   endif %}

{%- endif %}

{%- set options = salt['pillar.get']('dns:options', False)  %}
{%- if options %}
{%-   if resolvconf %}
{%-     set option_target = '/etc/resolvconf/resolv.conf.d/base' %}
{%-   else %}
{%-     set option_target = '/etc/resolv.conf' %}
{%-   endif %}
resolv-options:
  file.replace:
  - name: {{ option_target }}
  - append_if_not_found: True
  - pattern: ^[\t ]*options.*
  - repl: options {{ options }}
{%-   endif %}
