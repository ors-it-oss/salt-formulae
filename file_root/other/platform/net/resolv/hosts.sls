{%- set hosts = salt['pillar.get']('dns:hosts', False) %}
{%- if hosts %}
{%-   set entries = {} %}
{%-   for k, v in hosts.items() %}
{%-     if v is string %}
{%-       set v = [v] %}
{%-     endif %}
{%-     if k|is_ip %}
{%-       set records = entries.get(k, []) %}
{%-       do records.extend(v) %}
{%-       do entries.update({k: records}) %}
{%-     else %}
{%-       for ip in v %}
{%-          set records = entries.get(ip, []) %}
{%-         do records.append(k) %}
{%-         do entries.update({ip: records}) %}
{%-       endfor %}
{%-     endif %}
{%-   endfor %}
resolv-hosts:
  file.blockreplace:
  - name: /etc/hosts
  - marker_start: '# BEGIN SALTSTACK MANAGED'
  - marker_end: '# END SALTSTACK MANAGED'
  - content: |
{%-   for ip, hosts in entries.items() %}
      {{ ip }}    {{ ' '.join(hosts) }}
{%-   endfor %}
  - show_changes: True
  - append_if_not_found: True
{%- endif %}
