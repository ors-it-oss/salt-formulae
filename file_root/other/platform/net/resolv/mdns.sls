mdns-pkgs:
  pkg.installed:
  - names:
    - avahi
    - avahi-tools
    - nss-mdns
