{#- Some mount this automagically. #}
{%- if '/sys/fs/bpf' not in salt.mount.active() and 'bpf' not in salt.mount.fstab() %}
mount-bpf:
  mount.mounted:
  - name: /sys/fs/bpf
  - device: bpffs
  - fstype: bpf
  - opts:
    - rw
    - nosuid
    - nodev
    - noexec
    - relatime
    - mode=700
  - device_name_regex: bpf
  - persist: True
{%- endif %}

{##- This worked on Xenial, but shouldnt https://github.com/systemd/systemd/issues/8394
mount-bpf:
  file.managed:
  - name: /etc/systemd/system/sys-fs-bpf.mount
  - contents: |
      [Unit]
      Description=BPF FS mount
      Documentation=https://en.wikipedia.org/wiki/Berkeley_Packet_Filter
      DefaultDependencies=no
      Before=local-fs.target umount.target
      After=swap.target

      [Mount]
      What=bpffs
      Where=/sys/fs/bpf
      Type=bpf

      [Install]
      WantedBy=multi-user.target
  service.running:
  - name: sys-fs-bpf.mount
  - enable: True
  - watch:
    - file: mount-bpf
#}