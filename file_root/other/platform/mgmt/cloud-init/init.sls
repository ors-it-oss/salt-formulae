cloud-init-overrides:
  file.managed:
  - name: /etc/cloud/cloud.cfg.d/ZZ99-salt.cfg
  - source: salt://{{ slspath }}/cloud-init.yml
  - template: jinja
