{%- from 'shared.jinja' import list_if, list_if_pillar %}
{%- set rhel = salt.tmpltools.rhel() %}
include:
{{ list_if(salt.cmd.which('cloud-init'), '.cloud-init') }} {#- there's a good chance cloud-init fucks up the hostname control because we didn't give it proper intel #}
{{ list_if_pillar('salt:master', 'salt.minion') }}
- openssh.server
{{ list_if_pillar('zabbix:server', 'zabbix.agent') }}
- .tools

{# ScaleWays SSH key integration; maybe disable it? #}
{#scw-fetch-ssh-keys#}
