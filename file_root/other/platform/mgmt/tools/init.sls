{%- from 'shared.jinja' import list_if, list_if_pillar %}
{%- from 'salt/map.jinja' import pyver with context %}
{%- set rhel = salt.tmpltools.rhel() %}
{%- set ubuntu = salt.tmpltools.ubuntu() %}
{%- if rhel %}
include:
- repo.epel
{%- endif %}

administrative-packages:
  pkg.installed:
{%- if rhel %}
  - require:
    - sls: repo.epel
{%- endif %}
  - pkgs:
{#      - at#}
    - apg
    - binutils
{#    - byobu py2#}
    - curl
{#    - dstat py2#}
    - fping
    - socat
    - telnet
    - ethtool
    - htop
    - iftop
{#      - incron#}
    - iotop
    - ipset
{{ list_if(not ubuntu or ubuntu > 12.04, 'jq', indent=4) }}
    - lsof
{#    - mc py2#}
    - mtr
    - ncdu
    - tmux
    - nmap
    - pciutils
    - rsync
    - screen
    - strace
    - tcpdump
    - unzip
{%- if rhel %}
{%-   if rhel <= 6 %}
    - util-linux-ng
    - pwhois
{%-   else %}
    - net-tools
{%-   endif %}
{%- else %}
    - util-linux
    - whois
{%- endif %}
    - uuid
    - wget
{%- if grains.os_family == 'RedHat' %}
    - policycoreutils
{%-   if rhel %}
    - policycoreutils-python
{%-   else %}
    - policycoreutils-python-utils
{%-   endif %}
      {#    - setroubleshoot-server#}
    - iptraf-ng
    - bind-utils
    - redhat-lsb-core
    - conntrack-tools
    - vim-enhanced
    - xfsprogs
{%- elif grains.os_family == 'Debian' %}
    - apparmor-utils
    - apparmor-profiles
    - dnsutils
    - conntrack
    - iptraf
    - vim
    - lsb-release
    - xfsprogs
    - sysstat
    {# TODO: Xenial only???  #}
    - uuid-runtime
    - man-db
{%- endif %}
