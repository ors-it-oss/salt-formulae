pkg-updating:
  event.send:
  - name: pkg/{{ opts.id }}/updating
  - data:
      comment: Minion is commencing update operations
  - order: first

include:
- platform.pkg.update

pkg-updated:
  event.send:
  - name: pkg/{{ opts.id }}/updated
  - data:
      comment: Minion is done with update operations
  - order: last
