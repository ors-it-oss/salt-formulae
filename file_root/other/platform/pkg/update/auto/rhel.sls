{%- set method = salt['pillar.get']('pkg:update:method', 'salt') %}

{%- if method not in ('os', 'distro') %}
pkg-no-updating:
  pkg.removed:
  - name: yum-cron
{%- else %}
pkg-updating:
  pkg.installed:
  - name: yum-cron
{%- endif %}
