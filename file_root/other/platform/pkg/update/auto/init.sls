{%- from 'shared.jinja' import list_if %}
{%- set rhel = salt.tmpltools.rhel() %}
{%- set updates = salt['pillar.get']('pkg:update', {}) %}
{%- set method = updates.get('method', 'minion') %}

include:
{{ list_if(rhel, '.rhel') }}
{{ list_if(grains.os_family == 'Debian', '.debian') }}
{{ list_if(grains.os == 'Fedora', '.fedora') }}

{%- set name = 'Full upgrade state run' %}
{%- if method == 'minion' %}
{%-   set days = updates.get('days', 28) %} {#- every full moon #}
pkg-updates-salt:
  schedule.present:
  - name: {{ name }}
  - days: {{ days }}
  - function: state.sls
  - job_args:
    - platform.pkg.update.auto.run
  - job_kwargs:
      queue: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - splay: {{ salt.tmpltools.xround(days / 5 * 86400) }}

{%- else %}
pkg-updates-no-salt:
  schedule.absent:
  -  name: {{ name }}
{%- endif %}
