{%- set method = salt['pillar.get']('pkg:update:method', 'salt') %}

{%- if method not in ('os', 'distro') %}
pkg-no-updating:
  pkg.removed:
  - pkgs:
    - unattended-upgrades
{%- else %}
pkg-updating:
  pkg.installed:
  - pkgs:
    - unattended-upgrades
{%- endif %}
