{%- from 'shared.jinja' import param_if, param_if_pillar %}
{%- set updates = salt['pillar.get']('pkg:updates', {}) %}
{%- set rhel = salt.tmpltools.rhel() %}
include:
- .auto
- platform.pkg

pkg-updates:
  pkg.uptodate:
  - refresh: True
{{ param_if(grains.os_family == 'Debian', 'dist_upgrade', True, indent=2) }}
{{ param_if_pillar('pkg:updates:exclude', 'exclude', updates.get('exclude', False), indent=2) }}

{%- if grains.os_family == 'Debian' %}
pkg-cleanup:
  module.run:
  - pkg.autoremove: []
  - requires:
    - pkg: pkg-updates
{%- elif rhel %}
{#  TODO installonly_limit=kernel:retain, but that's not fully true either #}
pkg-cleanup:
  cmd.run:
  - name: package-cleanup -vCy --oldkernels --count={{ salt['pillar.get']('kernel:retain', 3) }}
  - unless: package-cleanup -q --assumeno -C --oldkernels --count={{ salt['pillar.get']('kernel:retain', 3) }} | grep -q 'No old kernels to remove'
{%- endif %}

#TODO 'might' break stuff (srsly)
#pkg-pip:
#  pip.uptodate: []

pkg-update-tstamp:
  grains.present:
  - name: pkg:last_update
  - value: {{ salt.tmpltools.datestamp() }}
  - onchanges:
    - pkg: pkg-updates
#    - pip: pkg-pip
