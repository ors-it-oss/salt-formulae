pkg-proxy:
{%-   if grains.os_family == 'RedHat' %}
  file.replace:
  - name: /etc/{{ 'dnf/dnf' if salt.cmd.which('dnf') else 'yum' }}.conf
  - pattern: ^proxy\s*=
  - repl: '#proxy='
  - append_if_not_found: False
{%-   elif grains.os_family == 'Debian' %}
  file.absent:
  - name: /etc/apt/apt.conf.d/10proxy
{%-   endif %}

pkg-proxy-profile:
  file.replace:
  - name: /root/.bashrc
  - pattern: ^(export)* (http|ftp|https)_proxy=.*
  - repl: ''
