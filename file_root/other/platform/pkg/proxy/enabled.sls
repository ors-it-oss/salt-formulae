{%- set proxy_pkg = salt['pillar.get']('pkg:proxy', []) %}
pkg-proxy:
{%- if grains.os_family == 'RedHat' %}
  file.replace:
  - name: /etc/{{ 'dnf/dnf' if salt.cmd.which('dnf') else 'yum' }}.conf
  - pattern: ^proxy\s*=.*
  - repl: proxy={{ proxy_pkg }}
  - append_if_not_found: True
{%- elif grains.os_family == 'Debian' %}
  file.managed:
  - name: /etc/apt/apt.conf.d/10proxy
  - user: root
  - group: root
  - mode: 644
  - contents:
      Acquire::http::proxy "{{ proxy_pkg }}";
      Acquire::ftp::proxy "{{ proxy_pkg }}";
      Acquire::https::proxy "{{ proxy_pkg }}";
{%- endif %}

pkg-proxy-profile:
  file.append:
  - name: /root/.bashrc
  - text:
    - export http_proxy="{{ proxy_pkg }}"
    - export https_proxy="{{ proxy_pkg }}"
    - export ftp_proxy="{{ proxy_pkg }}"
