{%- from 'shared.jinja' import list_if_pillar %}

include:
{{ list_if_pillar('pkg:proxy', '.enabled', else='.disabled') }}
