{%- from 'shared.jinja' import proxy %}
py-pip-not-pkg:
  pkg.removed:
  - pkgs:
    - python-pip

{%- for pyver in ('2', '3') %}
{%-   set pip = salt.cmd.which('pip' ~ pyver)  %}
{%-   set pyexec = salt.cmd.which('python' ~ pyver)  %}
{%-   if not pip and pyexec %}
{{ proxy('pip' ~ pyver ~ '_bootstrap', 'cmd') }}
py{{ pyver }}-pip_bootstrap:
  cmd.run:
  - name: {{ pyexec }} <(curl https://bootstrap.pypa.io/get-pip.py)
  - shell: /bin/bash
  - require:
    - pkg: py-pip-not-pkg
  - unless: which pip{{ pyver }}
{%-   endif %}
{%- endfor %}
