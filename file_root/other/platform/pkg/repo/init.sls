{%- set repos = salt['pillar.get']('pkg:repo', []) %}
{%- if repos %}
include:
{%-   for repo in repos %}
- repo.{{ repo }}
{%-   endfor %}
{%- endif %}

{## TODO: Revisit this when it's of use again
{%- if grains.os == 'Ubuntu' %}
{%-   import_yaml 'pcx_ubuntu.yaml' as default_sources %}
apt-sources:
  file.managed:
    - name: /etc/apt/sources.list
    - source: salt://{{ slspath }}/sources.list.jinja
    - context:
        sources: {{ salt['pillar.get']('pkg:sources') }}
    - defaults:
        sources: {{ default_sources }}
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
{%- endif %}
#}
