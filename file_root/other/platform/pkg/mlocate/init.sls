mlocate-pkg:
  pkg.installed:
  - name: mlocate

mlocate-updatedb:
  file.managed:
  - name: /etc/updatedb.conf
  - template: jinja
  - source: salt://{{ slspath }}/updatedb.conf.jinja
  - user: root
  - group: root
  - mode: 644
