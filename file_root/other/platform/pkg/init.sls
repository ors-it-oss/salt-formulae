{%- from 'shared.jinja' import list_if %}
{%- set rhel = salt.tmpltools.rhel() %}
include:
{{ list_if(rhel, 'repo.epel') }}
{{ list_if(rhel, 'repo.ius') }}
- .proxy
- .mlocate
- .repo
- .pip
- .update

pkg-reqs:
  pkg.installed:
  - pkgs:
    - git
{%- if grains.os_family == 'Debian' %}
    - apt-file
    - apt-transport-https
{# TODO: Apparently only on older ubuntus - python-software-properties#}
    - software-properties-common
{%- elif rhel %}
    - yum-utils
    - git2u
{#    - pygpgme TODO: For GPG verification in PackageCloud.io? #}
{%- endif %}
