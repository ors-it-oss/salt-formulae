include:
  - platform.boot.update
{##
Sources:
http://www.memtest.org/download/
http://www.memtest86.com/download.htm
#}
bootmenu-memtest86:
  file.recurse:
    - name: /boot/memtest86
    - source: salt://{{ slspath }}/memtest86/{{ grains.sys_interface }}
    - clean: True
    - user: root
    - group: root
    - max_depth: 0

{%- if grains.sys_interface == 'acpi' %}
bootmenu-memtest86+:
  file.recurse:
    - name: /boot/memtest86+
    - source: salt://{{ slspath }}/memtest86+/{{ grains.sys_interface }}
    - clean: True
    - user: root
    - group: root
    - max_depth: 0
{%- endif %}

bootmenu-memtest-hook:
  file.managed:
    - name: /etc/grub.d/25_memtesters
    - source: salt://{{ slspath }}/grub2.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 755
    - onchanges_in:
      - cmd: bootmenu-update
