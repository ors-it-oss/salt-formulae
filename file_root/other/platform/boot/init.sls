{%- from 'shared.jinja' import list_if %}
include:
- platform.hw.console
{{ list_if(salt.file.file_exists('/etc/default/grub'), '.grub2') }}
{{ list_if(grains.virtual == 'physical', '.memtest') }}
