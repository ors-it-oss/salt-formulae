{%- from 'platform/hw/console/serial.jinja' import conport with context -%}
{%- set grubexe = grains.kernel|upper() %}
{%- set xargs = salt['pillar.get']('kernel:args', {}).items()|map(join, '=')|join(' ') %}
include:
- .update

kernel-cmdlines:
  file.blockreplace:
  - name: /etc/default/grub
  - marker_start: '# BEGIN SALTSTACK MANAGED'
  - marker_end: '# END SALTSTACK MANAGED'
  - content: |
      # The odds of anoher OS on this system are close to 0
      GRUB_DISABLE_OS_PROBER=true

      GRUB_CMDLINE_{{ grubexe }}_DEFAULT="`echo $GRUB_CMDLINE_{{ grubexe }}_DEFAULT | sed 's/\(quiet\|rhgb\|splash\|nomodeset\|console=[^" ]\+\)//g'`"
      GRUB_CMDLINE_{{ grubexe }}="`echo $GRUB_CMDLINE_{{ grubexe }} | sed 's/\(quiet\|rhgb\|splash\|nomodeset\|console=[^" ]\+\)//g'`"

      GRUB_RECORDFAIL_TIMEOUT=10
      GRUB_SERIAL_COMMAND="serial --unit={{ conport }} --speed=115200 --word=8 --parity=no --stop=1"
      GRUB_TERMINAL="serial console"

      GRUB_CMDLINE_{{ grubexe }}="$GRUB_CMDLINE_{{ grubexe }} nomodeset console=tty0 console=ttyS{{ conport }},115200n8"
{%- if xargs %}
      GRUB_CMDLINE_{{ grubexe }}_DEFAULT=""$GRUB_CMDLINE_{{ grubexe }}_DEFAULT {{ xargs }}"
{%- endif %}
  - show_changes: True
  - append_if_not_found: True
  - onchanges_in:
    - cmd: bootmenu-update

{%- if salt.file.file_exists('/etc/grub.d/20_memtest86+') %}
memtest-conport:
  file.replace:
  - name: /etc/grub.d/20_memtest86+
  - pattern: ttyS[0-9]+(,[0-9a-z,]+)*
  - repl: ttyS{{ conport }},115200n8
  - backup: False
  - onchanges_in:
    - cmd: bootmenu-update
{%- endif %}
