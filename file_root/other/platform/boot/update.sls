{%- from 'shared.jinja' import param_if %}
bootmenu-update:
{% if grains.kernel == 'Linux' %}
  cmd.wait:
  - order: last
{{ param_if(grains.os_family == 'RedHat', 'name',
     'grub2-mkconfig > /boot/grub2/grub.cfg', else='update-grub2', indent=2) }}

{##
    - onchanges:
      - file: /etc/default/grub
#}
{%- endif %}
