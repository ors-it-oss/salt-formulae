#!py|yaml
# -*- coding: utf-8 -*-
import os
import string

'''
Do swap stuff
'''

SWAPS_DIR = '/var/swap'


def run():
    '''
    Renderer is
    #!py|yaml
    '''

    states = ''
    swaps = __salt__['mount.swaps']()
    swap_target = __salt__['pillar.get']('storage:swap', None)
    if swap_target is None:
        return states

    if swap_target is False:
        for swap, data in swaps.items():
            states += _disable(swap, data)
        return states
    elif swap_target.endswith(('*', 'x')):
        meminfo = __salt__['status.meminfo']()
        swap_target = meminfo['MemTotal']['value'] * float(swap_target.strip(string.whitespace + '*x'))
        # Bla to do more w/swap_target total size (e.g. GiB conversion etc

    swap_sized = sum((swap['size'] for swap in swaps.values())) - swap_target

    if __grains__['ostype'] == 'Linux':
        states += '''\
        swap-dir:
          file.directory:
          - name: {swap_dir}
          - user: root
          - group: root
          - dir_mode: 700
          - file_mode: 600
          - makedirs: True
        '''.format(swap_dir=SWAPS_DIR)

    if swap_sized == 0:
        return states
    elif swap_sized > 0:
        # fill up swaps

        pass
    elif swap_sized < 0:
        # delete swaps
        pass

    return states


def _disable(swap, data):
    disable = '''\
swap-disable-{swap}:
  mount.unmounted:
  - name: {swap}
'''

    # we leave partitions alone for now
    if data.get('type', 'partition') != 'partition':
        disable += '''\
  - persist: True
  file.absent:
  - name: {swap}
'''

    return disable.format(swap=swap)


def _enable(idx, size):
    '''
    TODO:
    Swap needs selinux fix:
      semanage fcontext -a -t swapfile_t '/var/swap/.*'

    swap-selinux:
      fcontext_policy_present:
      - name: '/var/swap/.*'
      - filetype: a
      - sel_type: swapfile_t
    '''
    swap_file = os.path.join(SWAPS_DIR, 'swap' + idx)
    swap_creator = 'fallocate -l {size} {swap_file}'
    # When on XFS fallocate no worky, just dd
    # swap_creator = 'dd if=/dev/null of={swap_file} bs=1M count={size}'
    swap_creator.format(size=size, swap_file=swap_file)

    enable = '''\
swap-{idx}-file:
  cmd.run:
  - name: {swap_creator}
  - creates: {swap_file}
  - requires:
    - file: swap-dir
swap-{idx}-lockdown:
  file.managed:
  - name: {swap_file}
  - replace: False
  - mode: 600
  - require:
    - cmd: swap-{idx}-file
swap-{idx}-init:
  cmd.run:
  - name: mkswap -L swap{idx} {swap_file}
  - onchanges:
    - cmd: swap-{idx}-file
swap-{idx}-mount:
  mount.swap:
  - name: {swap_file}
  - persist: True
  - require:
    - cmd: swap-{idx}-init    
'''.format(size=size, idx=idx, swap_file=swap_file)

    return enable
