{%- from 'shared.jinja' import list_if %}
{%- set rhel = salt.tmpltools.rhel() %}
{%- if rhel >= 7 %}
tmp-volatile:
  service.enabled:
  - name: tmp.mount
{# TODO: masked???, /etc/systemd/system/tmp.mount.d/mountopts.conf: Options= #}

{%- else %}
tmp-volatile:
  mount.mounted:
  - name: /tmp
  - device: tmpfs
  - fstype: tmpfs
  - opts:
    - defaults
    - nosuid
{#- Size needs to be in k or every highstate will trigger a remount ('cauz' /proc/mounts reports in k) #}
{%-   set size = salt.tmpltools.xround(grains.mem_vtotal/10, unit=128, low=1024, high=8192) %}
    - size={{ size * 1024 }}k
{{ list_if(rhel, 'context="system_u:object_r:tmp_t:s0"', indent=4) }}
  - hidden_opts:
    - mode=1777
{%- endif %}
