#!/bin/sh
# set -x
# Find bcache on a dev and make a symlink to that
# Because Bcache can't Be Bothered By device order
#
# Bcache is a bit fucked because a bcache is not the child of the
# underlying device
#
# Expects DEVPATH:
#
# [foo]# /usr/local/bin/bcache-dev '/block/virtual/bcache0'
#
max=10

diskpath=$(cd "$(readlink -f /sys/$1/bcache)/../"; echo $PWD)
disk=$(basename $diskpath)

echo "bcache/by-disk/$disk"

# on boot the parallelism might cause the locations to not exist yet
# And a bcache device is not a child of the underlying disk unfortunately
# therefore we try a couple of times
# and (re)query udev directly for the device
for i in $(seq 1 $max); do
  for link in $(udevadm info --export --query=symlink --name=/dev/$disk); do
    if [ $(echo $link | grep -c 'by-location') -gt 0 ]; then
      echo "$link" | sed 's|^disk|bcache|g'
      break 2
  fi ; done
  sleep 1
done
