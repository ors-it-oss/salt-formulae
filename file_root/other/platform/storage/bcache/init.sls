{# if salt['kmod.is_loaded']('bcache') #}
{%- from 'shared.jinja' import proxy %}
{%- set rhel = salt.tmpltools.rhel() %}

{%- if grains.oscodename == 'trusty' %}
{{ proxy('bcache-tools', 'pkg') }}
bcache-tools:
  pkgrepo.managed:
    - ppa: g2p/storage
  pkg.latest:
    - refresh: True

{%- elif rhel %}
include:
  - platform.kernel.elrepo

{## NOPE :(
bcache-tools:
  pkg.installed: []
#}
{%- endif %}

bcache-status:
  file.managed:
    - name: /usr/local/sbin/bcache-status
    - source: salt://{{ slspath }}/bcache-status
    - user: root
    - group: root
    - mode: 755

bcache-udev-links:
  file.managed:
    - name: /etc/udev/rules.d/70-bcache-dev.rules
    - source: salt://{{ slspath }}/udev-bcache-dev.rules
    - user: root
    - group: root
    - mode: 644

bcache-udev-linker:
  file.managed:
    - name: /usr/local/sbin/bcache_dev
    - source: salt://{{ slspath }}/bcache_dev.sh
    - user: root
    - group: root
    - mode: 755

{# endif #}

bcache:
  kmod.present
  {#  - persist: True #}
