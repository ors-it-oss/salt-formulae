include:
#- .swap
- .tmp_on_tmpfs

# TODO: https://docs.saltstack.com/en/develop/ref/states/all/salt.states.selinux.html#module-salt.states.selinux

{%- set root_dev = salt.mount.active()['/']['device'] %}
{%- set root = salt.mount.fstab()['/'] %}
{%- set opts = root.opts %}
{%- if 'relatime' in opts %}
{%-   do opts.remove('relatime') %}
{%- endif %}
{%- if 'noatime' in opts %}
{%-   do opts.remove('noatime') %}
{%- endif %}
{%- do opts.append('noatime') %}
root-mount-opts:
  mount.mounted:
  - name: /
  - device: {{ root.device }}
  - fstype: {{ root.fstype }}
  - opts: {{ root.opts }}
  - device_name_regex: {{ root_dev }}
  - persist: True
