{%- set groups = salt['pillar.get']('idm:groups', {}) %}
{%- for group, data in groups.items() %}
{%-   set keys = data.pop('ssh', False) %}
{%-   set system = data.pop('system', True) %}
group-{{ group }}:
  group.present:
  - name: {{ group }}
  - system: {{ system }}
{%-   for k, v in data.items() %}
  - {{ k }}: {{ v }}
{%-   endfor %}
{%- endfor %}
