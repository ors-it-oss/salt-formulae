{%- set users = salt['pillar.get']('idm:users', {}) %}
{%- set groups = salt['pillar.get']('idm:groups', {}).keys() %}

{%- set needs_groups = [] %}

{%- for user, data in users.items() %}
{%-   set data = data.copy() %}
{%-   set keys = data.pop('ssh', False) %}
{%-   set createhome = data.pop('createhome', True) %}
{%-   set system = data.pop('system', True) %}
user-{{ user }}:
  user.present:
  - name: {{ user }}
  - createhome: {{ createhome }}
  - system: {{ system }}
{%-   for k, v in data.items() %}
  - {{ k }}: {{ v }}
{%-   endfor %}
{%-   set deps = data.get('groups', [])|intersect(groups) %}
{%-   if deps %}
  - require:
{%-     for group in deps %}
    - group: group-{{ group }}
{%-     endfor %}
{%-     do needs_groups.append(deps) %}
{%-   endif %}
{%- endfor %}

{%-  if needs_groups %}
include:
- .groups
{%-  endif %}
