{%- from 'shared.jinja' import list_if %}
include:
- .groups
- .users
- .keys

{%- set refresh = pillar.get('idm:refresh', False) %}
{%- if refresh %}
idm-auto-refresh:
  schedule.present:
  - name: idm-refresh
{%- for k, v in refresh.items() %}
  - {{ k }}: {{ v }}
{%- endfor %}
  - function: state.sls
  - job_args:
    - platform.idm
  - job_kwargs:
      concurrent: True
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
{%- else %}
idm-no-refresh:
  schedule.absent:
  -  name: idm-auto-refresh
{%- endif %}
