#!py
# -*- coding: utf-8 -*-
'''
Do keys stuff
'''
import os

# Logging & Debugging
#import logging
#import pprint
#log = logging.getLogger(__name__)
#ppr = pprint.PrettyPrinter(indent=2).pprint

def run():
    idm = __salt__['pillar.get']('idm', {})
    users = idm.get('users', {})
    groups = idm.get('groups', {})

    '''
    make crew a flat dict beforehand to keep this even remotely sane
    crew = {
     'ACME admins': ['ssh-rsa etc ACME admins - Ronald', 'ssh-rsa etc ACME admins - Martijn']
     'Ronald': ['ssh-rsa etc ACME admins - Ronald', ]
    }
    '''
    crew = {}
    for group, members in idm.get('crew', {}).items():
        group_keys = set()
        for member, m_data in members.items():
            member_keys = set()
            for ssh_key in m_data.get('ssh', []):
                ssh_key = '{0} {1} - {2}'.format(ssh_key, group, member)
                member_keys.add(ssh_key)
            crew[member] = member_keys
            group_keys.update(member_keys)
        crew[group] = group_keys

    ret = {}
    for user, u_data in users.items():
        home_dir = __salt__['user.info'](user).get(
            'home',
            u_data.get(
                'home',
                os.path.join('home', user))
        )

        keys = set()
        # User specified keys
        keys.update(u_data.get('ssh', []))
        # Keys from groups
        for group in u_data.get('groups', []):
            if group not in groups:
                continue
            keys.update(
                groups[group].get('ssh', [])
            )
        # Keys from groups, the other way around
        for group, g_data in groups.items():
            if user not in g_data.get('addusers', []) + g_data.get('members', []):
                continue
            keys.update(
                g_data.get('ssh', [])
            )

        # Translate all entries to actual keys
        keys = set((key for name in keys for key in crew[name]))

        # Salt Master key
        if user == __grains__['username']:
            salt_ssh = __salt__['pillar.get']('salt:ssh:pub', False)
            if salt_ssh:
                keys.add(salt_ssh)

        # Force stable & repeatable output
        keys = sorted(keys)

        ret['user-{0}-authorized-keys'.format(user)] = {
            'file.managed': [
                {'name': '{0}/.ssh/authorized_keys2'.format(home_dir)},
                {'contents': '\n'.join(keys)},
                {'user': user},
                {'makedirs': True},
                {'mode': 600},
                {'dir_mode': 700},
                {'require': [
                    {'user': 'user-{0}'.format(user)},
                ]}
            ]}


    # Need this
    if ret:
        ret['include'] = ['.users']

    return ret
