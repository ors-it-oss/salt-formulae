# States
  base

## platform.backup

```
salt '*' state.sls platform.backup
```

* `etckeeper` is builtin.
* Respond to pillar keys for `duply`, `bacula` & `r1soft` by including their states.
  * `backup: []` pillar key overrides

## platform.hw

Hardware, driver support, sensors etc.

```
salt '*' state.sls platform.hw
salt '*' state.sls platform.hw.baremetal
salt '*' state.sls platform.hw.console
salt '*' state.sls platform.hw.qemu
salt '*' state.sls platform.hw.rng
salt '*' state.sls platform.hw.clock

```
* baremetal
    - drivers & controlling real hw
    - ipmi
    - sensors
    - disks
    - lsi hw

## platform.boot
Boot CLI/mgr entries & updates
```
salt '*' state.sls platform.boot
```

## platform.i18n
Put some locale stuff right
```
salt '*' state.sls platform.i18n
```

## platform.idm
```
salt '*' state.sls platform.idm
salt '*' state.sls platform.groups
salt '*' state.sls platform.users
salt '*' state.sls platform.keys
```
Identity Management Ghetto-style | LDAP style

## platform.kernel

Kernel management and tuning, crashdumps & proper cleanup

## platform.mgmt

Include Salt Minion, SSH
Zabbix Agent and other monitoring & logging agents,
fix Cloud-Init that kind of stuff

Pillar keys must exist for each solution

## platform.net

Network stuffs

## platform.pkg

Package & repository settings & mgmt

```
salt '*' state.sls platform.pkg
salt '*' state.sls platform.pkg.proxy
salt '*' state.sls platform.pkg.repo
salt '*' state.sls platform.pkg.tools
salt '*' state.sls platform.pkg.update
salt '*' state.sls platform.pkg.updates
```

* tools = admin stuff
* repo = repo's, mirrors etc
* proxy = enable/disable proxy
* update = manage auto-updates
* updates = perform auto-updates saltily

## platform.storage

bcache, swap, zfs etc.


## platform.jobs

most args are directly passed on to scheduler, save saltenv, pillarenv, pillar, exclude, kwargs, args, fun in the right scenarios

## Others?

* .maint = orch for upgrades etc?
* .tls ? Let's Encrypt etc?
