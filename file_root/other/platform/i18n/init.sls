i18n-locales:
  locale.present:
  - names:
    - en_US.UTF-8
    - en_GB.UTF-8
    - nl_NL.UTF-8

{#- *REALLY* #}
{%- if grains.init == 'systemd' and grains.os_family == 'Debian' %}
i18n-systemd-suxorz:
  file.managed:
  - names:
    - /etc/locale.conf
    - /etc/vconsole.conf
    - /etc/default/keyboard
  - replace: False
  - require_in:
    - locale: i18n-default
{%- elif grains.os == 'Fedora' %}
i18n-locale-pkgs:
  pkg.installed:
  - names:
    - glibc-locale-source
  - require_in:
    - locale: i18n-locales
{%- endif %}

i18n-default:
  locale.system:
  - name: en_GB.UTF-8
  - require:
    - locale: i18n-locales
