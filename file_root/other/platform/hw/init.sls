include:
- .console
- platform.boot
- .rng
{%- if grains.virtual in ('bochs', 'qemu', 'kvm') %}
- .qemu
{%- elif grains.virtual == 'physical' %}
- .baremetal
{%- endif %}
- .clock

{## TODO: Validate if this is stil necesseray
{%- if grains.cpuarch == 'x86_64' %}
acpi-support:
  pkg.installed:
    - name: acpid
{%- endif %}
#}
