{%- from 'shared.jinja' import list_if %}
{## TODO: This used to crash CentOS kernels
{% if grains.kernel == 'Linux' %}
kernel-blacklist-btrfs:
  file.managed:
    - name: /etc/modprobe.d/blacklist-btrfs.conf
    - source: salt://{{ slspath }}/blacklist-btrfs.conf
    - user: root
    - group: root
    - mode: 644
{%- endif %}
#}

hw-platform-pkgs:
  pkg.installed:
  - names:
    - virt-what
{{ list_if(salt.tmpltools.ubuntu() > 12.04, 'qemu-guest-agent', indent=4) }}
