{%- from slspath + '/serial.jinja' import conport with context %}
include:
- platform.boot

{%- if grains.init == 'upstart' %}
conport-tty:
  file.managed:
  - name: /etc/init/ttyS{{ conport }}.conf
  - source: salt://{{ slspath }}/upstart.jinja
  - template: jinja
  - mode: 644
  - user: root
  - group: root
  service.running:
  - name: ttyS{{ conport }}
  - enable: True
  - watch:
    - file: /etc/init/ttyS{{ conport }}.conf
{%- endif %}
