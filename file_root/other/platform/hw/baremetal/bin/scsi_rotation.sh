#!/bin/sh

MRR=$(sdparm --quiet --get=MRR=1 /dev/$1 | awk '{ print $2 }')
if [ -n "$MRR" ]; then
  echo "ID_SCSI_MRR=$MRR"
fi