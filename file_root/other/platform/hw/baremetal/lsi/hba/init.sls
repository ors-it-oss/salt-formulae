hw-support-sas2ircu:
  file.managed:
  - name: /usr/local/sbin/sas2ircu
  - source: salt://{{ slspath }}/sas2ircu_{{ grains.kernel }}
  - user: root
  - group: root
  - mode: 755

hw-support-lsi-locate:
  file.managed:
  - name: /usr/local/sbin/lsi_locate
  - source: salt://{{ slspath }}/lsi_locate.py
  - user: root
  - group: root
  - mode: 755

{% if grains.kernel == 'Linux' %}
udev-lsi-locate:
  file.managed:
  - name: /etc/udev/rules.d/55-lsi-locate.rules
  - source: salt://{{ slspath }}/udev-lsi-locate.rules
  - user: root
  - group: root
  - mode: 644
{% endif %}
