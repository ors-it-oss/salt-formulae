#!/usr/bin/env bash

release=$1
dir=$2

baseURL='http://www.lsi.com/downloads/Public/Host%20Bus%20Adapters/Host%20Bus%20Adapters%20Common%20Files/SAS_SATA_6G_P${release}'

tmpDir=$(mktemp -d)
cd $tmpDir

# SAS2IRCU
sas2ircu = SAS2IRCU_P${release}.zip
wget ${baseURL}/$sas2ircu
unzip $sas2ircu


http://www.lsi.com/downloads/Public/Host%20Bus%20Adapters/Host%20Bus%20Adapters%20Common%20Files/SAS_SATA_6G_P20/SAS2IRCU_P20.zip


http://www.lsi.com/downloads/Public/Host%20Bus%20Adapters/Host%20Bus%20Adapters%20Common%20Files/SAS_SATA_6G_P20/9211_8i_Package_P20_IR_IT_Firmware_BIOS_for_MSDOS_Windows.zip