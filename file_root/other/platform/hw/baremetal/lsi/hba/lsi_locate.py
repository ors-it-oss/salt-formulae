#!/usr/bin/python
#
# Return ID_ENCLOSURE and ID_SLOT to udev
# based on sas2(3?)ircu data
# derived from SAS address
#
# Should get from udev %p, e.g.: /devices/pci0000:00/0000:00:03.0/0000:01:00.0/host2/port-2:7/end_device-2:7/target2:0:7/2:0:7:0/block/sdh
#
#
import sys
import re
import subprocess

try:
    ircu = subprocess.check_output(('which', 'sas2ircu')).strip()
except (IOError, OSError, subprocess.CalledProcessError):
    sys.exit(1)


def controllers():
    """
    Returns a list of tuples with the controller info in it;
    e.g.:
    sas2ircu LIST
         Adapter      Vendor  Device                       SubSys  SubSys
    Index    Type          ID      ID    Pci Address          Ven ID  Dev ID
    -----  ------------  ------  ------  -----------------    ------  ------
    0     SAS2008     1000h    72h   00h:01h:00h:00h      1000h   3020h

    becomes
    (
    (0,'SAS2008','1000h','72h','00h:01h:00h:00h','1000h','3020h')
    )
    """
    LameStoopidIrritating_out = subprocess.check_output((ircu, 'LIST')).decode()
    contlist = []
    for line in reversed(LameStoopidIrritating_out.split('\n')):
        if re.match(r'^[\s-]*[0-9]+',line):
            cont = re.split(r'\s+',line.strip())
            cont[0] = int(cont[0])
            cont = tuple(cont)
            contlist.append(cont)
        #Header line only has ----- --- - --- - ----- - etc
        elif re.match(r'^[\s-]+$', line):
            return tuple(contlist)


def devices(controller):
    """
    Extracts the Physical Device information as a dict from the LSI controller
    Key = SAS address, e.g.:

    Device is a Hard disk
    Enclosure #                             : 1
    Slot #                                  : 7
    SAS Address                             : 4433221-1-0400-0000
    State                                   : Ready (RDY)
    Size (in MB)/(in sectors)               : 2861588/5860533167
    Manufacturer                            : ATA
    Model Number                            : ST3000DM001-1CH1
    Firmware Revision                       : CC27
    Serial No                               : W1F4FE2C
    GUID                                    : 5000c5007281c150
    Protocol                                : SATA
    Drive Type                              : SATA_HDD

    becomes

    0x4433221103000000 : {
     'Enclosure #' : '1',
     'SAS Address' : '4433221-1-0400-0000',
     etc.
    }
    """
    LameStoopidIrritating_out = subprocess.check_output((ircu, str(controller), 'DISPLAY')).decode()

    #SPLIT out the section
    #Physical device information
    #--------------------
    #(...)
    #--------------------
    devices = re.split(r'Physical device information\n[\s-]+\n', LameStoopidIrritating_out)
    devices = re.split(r'\n[\s-]+\n', devices[1])

    #SPLIT out each Device
    #Device is a Hard disk
    #  Enclosure #                             : 1
    #  Slot #                                  : 5
    #  SAS Address                             : 4433221-1-0600-0000
    #  (...)
    devices = re.split(r'Device is[^\n]+', devices[0])

    locations = {}
    for dev in devices:
        #split & strip all fields
        dev = map(lambda prop: prop.split(':'), dev.splitlines())
        dev = map(lambda o: map(lambda f: f.strip(), o), dev)

        #Destroy all bogus/empty lines
        dev = [prop for prop in dev if len(prop) == 2 and len(prop[0]) and len(prop[1])]
        if not len(dev):
            #print("SKIPPING")
            continue

        #FINALLY a useful dict!
        dev = {prop[0] : prop[1] for prop in dev}
        sas_addr = '0x' + dev['SAS Address'].replace('-', '')

        locations[sas_addr] = dev
    return locations


if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit(1)

    sas_address = '/sys' + sys.argv[1] + '/device/sas_address'

    try:
        sas_address = open(sas_address).read().strip()
    except (IOError, OSError):
        sys.exit(1)

    for controller in controllers():
        devices = devices(controller[0])
        if sas_address in devices:
            dev = devices[sas_address]
            encl = int(dev['Enclosure #'])
            slot = int(dev['Slot #'])
            print('ID_LOC_ENCLOSURE=' + format(encl, '02d'))
            print('ID_LOC_SLOT=' + format(slot, '02d'))
            sys.exit(0)
