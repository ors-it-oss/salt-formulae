megaraid-dir:
  file.recurse:
  - name: /opt/MegaRAID
  - source: salt://{{ slspath }}/{{ grains.kernel }}/
  - user: root
  - group: root

{# https://github.com/saltstack/salt/issues/2707 #}
megacli-acls:
  file.managed:
  - name: /opt/MegaRAID/MegaCli/MegaCli64
  - replace: False
  - mode: 755

storcli-acls:
  file.managed:
  - name: /opt/MegaRAID/storcli/storcli64
  - replace: False
  - mode: 755

megacli-symlink:
  file.symlink:
  - name: /usr/local/sbin/MegaCli
  - target: /opt/MegaRAID/MegaCli/MegaCli64

storcli-symlink:
  file.symlink:
  - name: /usr/local/sbin/storcli
  - target: /opt/MegaRAID/storcli/storcli64
