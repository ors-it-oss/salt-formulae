net-support-pkgs:
  pkg.installed:
  - pkgs:
    - ethtool
{%- if grains.os_family == 'Debian' %}
    - vlan
    - ifenslave
    - bridge-utils
{%- endif %}

{%- set max_speed = ((salt.i7y.eth() or {'nodata': {'speed': 0}}).values()|selectattr('speed', 'defined')|map(attribute='speed')|sort)[-1] %}
{%- if max_speed > 2000 %}
include:
- .10G
{%- endif %}
