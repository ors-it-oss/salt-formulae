{##
NET tunes;
  https://www.suse.com/communities/blog/sles-1112-os-tuning-optimisation-guide-part-1/
  https://www.suse.com/communities/blog/sles-1112-network-cpu-tuning-optimization-part-2/
  https://blog.cloudflare.com/how-to-receive-a-million-packets/
  https://blog.cloudflare.com/how-to-achieve-low-latency/
#}
net-10G-tunes:
  sysctl.present:
  - config: /etc/sysctl.d/20-10000mbit.conf
  - names:
    - net.core.netdev_max_backlog:
      - value: 30000
    - net.core.rmem_max:
      - value: 1048576
    - net.core.wmem_max:
      - value: 1048576
    - net.ipv4.tcp_fastopen:
      - value: 1
    - net.ipv4.tcp_moderate_rcvbuf:
      - value: 1
    - net.ipv4.tcp_rmem:
      - value: 4096 65536 1048576
    - net.ipv4.tcp_sack:
      - value: 1
    - net.ipv4.tcp_timestamps:
      - value: 0
    - net.ipv4.tcp_wmem:
      - value: 4096 65536 1048576
