{##
Don't want to run sensors-detect on each highstate
theoretically this should run it when the pkg states change
where updating == changing (QED)
alternative idea: salt-call file.contains /etc/modules 'sensors-detect'
but it's not /etc/modules on each distro
#}

hw-sensor-support:
  pkg.installed:
  - names:
    - i2c-tools
{%- if grains.os_family == 'RedHat' %}
    - lm_sensors
{%- elif grains.os_family == 'Debian' %}
    - lm-sensors
{%- endif %}
  cmd.wait:
  - name: yes | sensors-detect
  - onchanges:
    - pkg: hw-sensor-support
