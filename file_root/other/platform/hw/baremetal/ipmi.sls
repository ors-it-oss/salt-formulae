{%- from 'shared.jinja' import list_if %}
ipmi-support:
  pkg.installed:
  - names:
{#      #- freeipmi-bmc-watchdog?#}
{%- if grains.os_family == 'RedHat' %}
    - freeipmi
    - OpenIPMI
    - OpenIPMI-python
{%- elif grains.os_family == 'Debian' %}
    - freeipmi-tools
    - openipmi
{%- endif %}
  - watch_in:
      - sensors-support
  kmod.present:
  - names:
    - ipmi_devintf
    {# It's there. It's not there. It's kmod crazyness! #}
    {{ list_if(salt.kmod.check_available('ipmi_msghandler'), 'ipmi_msghandler', indent=4) }}
    - ipmi_si
  - persist: True
