{%- from 'shared.jinja' import list_if, list_if_pillar %}
{%- set rhel = salt.tmpltools.rhel() %}
{#- http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/arch/x86/include/asm/cpufeature.h #}
hw-support-pkgs:
  pkg.installed:
  - pkgs:
    {#- hw info #}
    - lshw
    - pciutils
    - cpuid
    {#- mem stuff #}
{%- if grains.cpu_manufacturer == 'AMD' %}
    - edac-utils
{%- else %}
    - mcelog
{%- endif %}

    {#- disk stuff #}
    - gdisk
    - hdparm
    - sdparm
    - smartmontools

{%- if grains.os_family == 'Debian' %}
{%-   if grains.cpu_manufacturer == 'AMD' %}
    - amd64-microcode
{%-   elif grains.cpu_manufacturer == 'Intel' %}
    - intel-microcode
{%-   endif %}
{%- elif rhel %}
    - microcode_ctl
{%- endif %}

include:
{{ list_if(grains.num_sockets > 1, '.numa') }}
{{ list_if('ipmi' in grains, '.ipmi') }}
{{ list_if(salt.kmod.is_loaded('mptbase') or salt.kmod.is_loaded('mpt2sas'), '.lsi.hba') }}
{{ list_if(salt.kmod.is_loaded('megaraid_sas'), '.lsi.megaraid') }}
- .sensors
- .disk
- .net
