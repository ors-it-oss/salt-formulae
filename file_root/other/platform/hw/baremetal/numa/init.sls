hw-mproc-support:
  pkg.installed:
  - pkgs:
    - irqbalance
{%- if grains.get('numa_nodes', 0) > 1 %}
    - numactl
{%-   if grains.os_family == 'RedHat' or salt.tmpltools.ubuntu() > 14.04 %}
    - numad
{%-   endif %}
{%- endif %}

hw-irqbalance:
  file.managed:
{%- if grains.os_family == 'Debian' %}
  - name: /etc/default/irqbalance
{%- elif grains.os_family == 'RedHat' %}
  - name: /etc/sysconfig/irqbalance
{%- endif %}
  - user: root
  - group: root
  - mode: 644
  - template: jinja
  - source: salt://{{ slspath }}/config.jinja
  service.running:
  - watch:
    - pkg: hw-mproc-support
    - file: irqbalance
