{%- if grains.kernel == 'Linux' %}
{%-   if grains.oscodename == 'trusty' %}
{#- exctracted from utopic's util-linux #}
hw-support-blkdiscard:
  file.managed:
  - name:  /usr/local/sbin/blkdiscard
  - source: salt://{{ slspath }}/bin/blkdiscard
  - mode: 755
  - user: root
  - group: root
{%-   endif %}

{#- I think this makes little sense for now on virtualized machines 'cauz' they ain't got no friggin' clue about the disks #}
udev-disk-schedulers:
  file.managed:
  - name: /etc/udev/rules.d/65-schedulers.rules
  - user: root
  - group: root
  - source: salt://{{ slspath }}/udev_disk-schedulers.rules
  - mode: 644

udev-scsi-rotation-script:
  file.managed:
  - name: /usr/local/sbin/scsi_rotation
  - source: salt://{{ slspath }}/bin/scsi_rotation.sh
  - user: root
  - group: root
  - mode: 755

udev-scsi-rotation:
  file.managed:
  - name: /etc/udev/rules.d/65-scsi-rotation.rules
  - user: root
  - group: root
  - source: salt://{{ slspath }}/udev_scsi-rotation.rules
  - mode: 644

udev-blk-locate:
  file.managed:
  - name: /etc/udev/rules.d/65-blk-locate.rules
  - user: root
  - group: root
  - source: salt://{{ slspath }}/udev_blk-locate.rules
  - mode: 644
{%- endif %}
