l10n-tz:
  timezone.system:
  - name: {{ salt['pillar.get']('timezone', 'Europe/Amsterdam') }}
  - utc: True

{%- set ntp = 'chrony' %}
{%- if grains.kernel != 'Linux' %}
{%-   set ntp = 'ntp' %}
{%- elif grains.os == 'Debian' and grains.osmajorrelease < 9 %}
{%-   set ntp = 'ntp' %}
{%- elif grains.os == 'Ubuntu' and grains.osmajorrelease < 18 %}
{%-   set ntp = 'ntp' %}
{%- elif grains.os_family == 'RedHat' and grains.osmajorrelease < 8 %}
{%-   set ntp = 'ntp' %}
{%- endif %}
include:
- {{ ntp }}
