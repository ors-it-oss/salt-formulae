{%- set hwrng = ('exrand', 'rdrand', 'rng_en')|intersect(grains.cpu_flags)|length %}
{%- if hwrng > 0 %}
rng-hw:
  pkg.installed:
  - pkgs:
    - rng-tools
  service.running:
  - name: {{ 'rng-tools' if grains.os_family == 'Debian' else 'rngd' }}
  - enable: True
{%- else %}
rng-no-hw:
  pkg.removed:
  - pkgs:
    - rng-tools
{%- endif %}
