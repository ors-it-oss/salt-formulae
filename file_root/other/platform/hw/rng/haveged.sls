rng-haveged:
  pkg.installed:
  - name: haveged
  service.running:
  - name: haveged
  - enable: True
  - watch:
    - pkg: haveged
