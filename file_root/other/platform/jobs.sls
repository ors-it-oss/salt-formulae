{%- set all_jobs = [] %}
{%- for j_type, jobs in pillar.get('jobs', {}).items() %}
{%-   if jtype == 'fun' %}
{%-     for jobid, jobd in jobs.items() %}
{%-       set job_kwargs = jobd.pop('kwargs', {}) %}
{%-       set job_args = jobd.pop('args', []) %}
{%-       set job = {
            'name': jobid,
            'sched': jobd,
            'fun': jobid,
            'args': job_args,
            'kwargs': job_kwargs,
} %}
{%-       do all_jobs.append(job) %}

{%-   elif jtype == 'mine' %}
{%-     for jobid, jobd in jobs.items() %}
{%-       set job_kwargs = {
            'mine_function': jobd.pop('fun'),
            'args': jobd.pop('args', []),
            'kwargs': jobd.pop('kwargs', {})} %}
{%-       set job = {
            'name': jobid,
            'sched': jobd,
            'fun': 'mine.send',
            'args': [jobid],
            'kwargs': job_kwargs,
} %}
{%-       do all_jobs.append(job) %}

{%-   elif jtype == 'state' %}
{%-     for jobid, jobd in jobs.items() %}
{%-       set job_kwargs = {'queue': True} %}
{%-       for arg in ('pillar', 'pillarenv', 'saltenv', 'exclude') %}
{%-         set state_kwarg = jobd.pop(arg, None) %}
{%-         if state_kwarg %}
{%-           do job_kwargs[arg] = state_kwarg %}
{%-         endif %}
{%-       endfor %}
{%-       set job = {
            'name': jobid,
            'sched': jobd,
            'fun': 'state.sls',
            'args': [],
            'kwargs': job_kwargs,
} %}
{%-       do all_jobs.append(job) %}
{%-     endfor %}

{%-   elif jtype == 'top' %}
{%-     for jobid, jobd in jobs.items() %}
{%-       set job_kwargs = {'queue': True} %}
{%-       for arg in ('pillar', 'pillarenv', 'saltenv') %}
{%-         set state_kwarg = jobd.pop(arg, None) %}
{%-         if state_kwarg %}
{%-           do job_kwargs[arg] = state_kwarg %}
{%-         endif %}
{%-       endfor %}
{%-       set job = {
            'name': jobid,
            'sched': jobd,
            'fun': 'state.top',
            'args': [],
            'kwargs': job_kwargs,
} %}
{%-       do all_jobs.append(job) %}
{%-     endfor %}

{%-   endif %}
{%- endfor %}


{%-   for j_id, j_data in jobs.items() %}
{%-     do all_jobs.append(j_id) %}

{%-     if j_type == 'fun' %}
{%-       set j_fun = j_id %}
{%-       set j_args = j_data.pop('args', []) %}
{%-       set j_kwargs = j_data.pop('kwargs', {}) %}

{%-     elif j_type == 'mine' %}
{%-       set j_fun = 'mine.send' %}
{%-       set j_args = [j_id] %}
{%-       set j_kwargs = {
            'mine_function': j_data.pop('fun'),
            'args': j_data.pop('args', []),
            'kwargs': j_data.pop('kwargs', {})} %}

{%-     elif j_type == 'orch' %}
{%-       set j_fun = 'saltutil.runner' %}
{%-       set j_args = ['state.orchestrate'] %}
{%-       set j_kwargs = {'orchestration_jid': 'scheduled-' ~ j_id, 'kwarg': {}} %}
{%-       for arg in ('pillar', 'pillarenv', 'saltenv') %}
{%-         set state_kwarg = j_data.pop(arg, None) %}
{%-         if state_kwarg %}
{%-           do j_kwargs['kwarg'][arg] = state_kwarg %}
{%-         endif %}
{%-       endfor %}

{%-     elif j_type == 'state' %}
{%-       if j_id == 'high' %}
{%-         set j_fun = 'state.highstate' %}
{%-         set j_args = [] %}
{%-       else %}
{%-         set j_fun = 'state.sls' %}
{%-         set j_args = [j_id] %}
{%-       endif %}
{%-       set j_kwargs = {'queue': True} %}
{%-       for arg in ('pillar', 'pillarenv', 'saltenv', 'exclude') %}
{%-         set state_kwarg = j_data.pop(arg, None) %}
{%-         if state_kwarg %}
{%-           do j_kwargs[arg] = state_kwarg %}
{%-         endif %}
{%-       endfor %}

{%-     elif j_type == 'top' %}
{%-       set j_fun = 'state.top' %}
{%-       set j_args = [j_id] %}
{%-       set j_kwargs = {'queue': True} %}
{%-       for arg in ('pillar', 'pillarenv', 'saltenv') %}
{%-         set state_kwarg = j_data.pop(arg, None) %}
{%-         if state_kwarg %}
{%-           do j_kwargs[arg] = state_kwarg %}
{%-         endif %}
{%-       endfor %}

{%-     endif %}
job-schedule-{{ j_id }}:
  schedule.present:
  - name: {{ j_id }}
{%-     for k, v in j_data.items() %}
  - {{ k }}: {{ v }}
{%-     endfor %}
  - function: {{ j_fun }}
  - job_args: {{ j_args }}
  - job_kwargs: {{ j_kwargs }}
  - maxrunning: 1
  - persist: True
  - return_job: True
  - run_on_start: False
  - metadata:
      source: platform.jobs
{%-   endfor %}
{%- endfor %}

{%- for j_id, j_data in salt.schedule.list(show_all=True).items() %}
{%-   if j_data.get('metadata', {}).get('source', False) == 'platform.jobs' and j_id not in all_jobs %}
job-deschedule-{{ j_id }}:
  schedule.absent:
  - name: {{ j_data.name }}
  - persist: True
{%-   endif %}
{%- endfor %}
