{%- from 'shared.jinja' import list_if_pillar %}
{%- set backups = salt['pillar.get']('backup', []) %}
include:
- etckeeper
{{ list_if_pillar('bacula:server', 'bacula.agent', default='bacula' in backups) }}
{{ list_if_pillar('duply', 'duply', default='duply' in backups) }}

{# TODO: OTHER BACKUP METHODS!  #}
